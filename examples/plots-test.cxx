#include <iostream>
#include "NPALegend.h"
#include "TCanvas.h"
#include "TH1.h"

using namespace std;

int main(int argc, char const *argv[])
{
	cout<<"Runing a test code for NPAPlots"<<endl;
	cout<<"Creating a legend: "<<endl;
	TCanvas *c = new TCanvas("c","c",800,600);
	NPALegend leg("leg1",0.2,0.4,0.4,0.6);
	cout<<"Legend id:"<<leg.getID()<<endl;
	cout<<"Legend length: "<<leg.getLength()<<endl;
	cout<<"Legend hight: "<<leg.getHight()<<endl;
	cout<<"Create a dummy hisgtogram to add to the leged: "<<endl;
	TH1D *h = new TH1D("h1","h1",100,0,1000);
	for (float i = 0; i < 1000; i+=0.2)
	{
		h->Fill(i,1);
	}
	leg.AddEntry("h1","legend 1","p");

	cout<<"Create a new legend that will be place to the right of the first one."<<endl;
	NPALegend leg2("leg2", 0.1,0.2,0.3,0.1);
	leg2.AddEntry("h1","legend 2","p");
	leg2.alignTop(&leg, 0.1);
	h->Draw("pe");
	leg.Draw("same");
	leg2.Draw("same");
		leg2.alignRight(&leg, 0.1);
			leg2.Draw("same");
					leg2.alignLeft(&leg, 0.1);
			leg2.Draw("same");

	c->Print("test.pdf");
	delete c;
	
	return 0;
}