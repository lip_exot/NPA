#!/usr/bin/env sh
srcdir=`dirname $0`
(cd $srcdir; autoreconf -if)
$srcdir/configure "$@"
