#!/bin/bash

## Simple setup for NPA
export NPADIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )
export RUBYLIB=$NPADIR/scripts:$RUBYLIB
export PATH=$NPADIR/bin:$PATH
export LD_LIBRARY_PATH=$NPADIR/lib:$LD_LIBRARY_PATH
echo "---> NPA set up in: "$NPADIR
