#include "@ANALYSIS@.h"

using namespace std;

int main(int argc, const char *argv[])
{
    @ANALYSIS@ object(argc, argv);
    object.Start(argc, argv);

    return 0;
}
