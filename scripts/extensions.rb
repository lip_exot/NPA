module NPA
    Thread.abort_on_exception = true
end
def do_while_wait(opt={})
    opts = {:fps => 10,:msg => ""}.merge(opt)
    print opts[:msg]
    chars=%w(| / - \\ | / - \\)
    delay = 1.0/opts[:fps]
    iter = 0
    spinner = Thread.new do
        while iter do  
            print chars[(iter+=1) % chars.length]
            sleep delay
            print "\b"
        end
    end
    yield.tap{      
        iter = false
        spinner.join
        print "Done!\n"
    }                
end
class Numeric
    def duration
        secs  = self.to_int
        mins  = secs / 60
        hours = mins / 60
        days  = hours / 24

        if days > 0
            "#{days}d:#{hours % 24}h"
        elsif hours > 0
            "#{hours}h:#{mins % 60}m"
        elsif mins > 0
            "#{mins}m:#{secs % 60}s"
        elsif secs >= 0
            "#{secs}s"
        end
    end
end
