=begin
JobClass.rb is a library with all the classes used to work with jobs sumission.
The idea is to have a center system that manages the jobs that will be needed with
the options and configuration needed for any analysis. 
Command class is used to create a given script to be launched when needed 
Rules is a classes used to define when a job needs to run and what to do with it.
Job class inherits from Rules and it is the main object used to define a Job that needs to be 
launched or run. It needs to satisfy all the rules before running
=end

class Command
    attr_accessor :executable
    attr_accessor :format

    def initialize(exe)
        @executable = "./#{exe}"
        @options = Hash.new
        @format="KEY VALUE"
        @line = ""
    end

    # Define the sample of the code
    def add_option(key,value)
        line = @format.sub "KEY",key.to_s
        line.sub! "VALUE",value.to_s
        @line += " #{line} "
    end

    # Get the the line
    def to_s
        "time #{@executable} #{@line}"
    end

end
