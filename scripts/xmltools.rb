=begin
xmltool.rb is intended to make it easy to work with xml inside NPA pacakge.
Some xmls are defined to work with NPA and it should easy to handle them
from the user side instead of have to write code or scripts to change
them.
=end
include REXML

class UpdateXML
    def initialize(file)
        @initfile = file
        @doc = Document.new(File.new(file))
        @newdoc = Document.new
        @newdoc << XMLDecl.new
        @root = @doc.root
        @newroot = @newdoc.add_element(@root)
    end

    def update_fields
        fields = Hash.new
        yield(fields)
        fields.each do |key,value|
                @newroot.each_element(key) do |element|
                    update(:ele => element,:val => value)
                end
        end
    end

    ## Duplicate an element changing some values
    #def duplicate_nodes(nodes=[])
    #    nodes.each do |obj|
    #        @newroot.each_element(obj.node) do |ele|
    #            todup = false      
    #            ele.each_recursive do |ee|
    #                ee.attributes.each do |atr|
    #                end
    #        end
    #end

    def update(pp={})
        case pp[:val]
        when Hash
            pp[:val].each do |k,v|
                v.each do |cc|
                    pp[:ele].attributes.each do |id,val|
                        #p "Attribute: #{id}=#{val}"
                        pp[:ele].attributes[id] = val.gsub! k,cc if pp[:ele].to_s.include? k
                        #p "After -> Attribute: #{id}=#{val}"
                    end
                    if pp[:ele].text != nil
                        pp[:ele].text.gsub! k,cc
                    end
                end
            end
        when String
            ## TODO: Needs to be done properly.. or dropped.
            # Check if attribute is passed
            attribute = (pp[:val].include? ":") ? pp[:val].split(":")[0] : ""
            if attribute.empty?
                ele.text = pp[:val].split(":")[1]
            else
                ele.attributes[attribute] = pp[:val].split(":")[1]
            end
        else 
            raise ArgumentError, "The type of value given to duplicate a field is invalid. Hash or String are supported"
        end
    end

    def parse_key(key)
        nodes = []
        attribute = ""
        # Find nodes and attributes to change
        if key.include? ":"    
            fff = key.split(":")
            attribute = fff[-1]
            nodes = fff[0].split(">")
        else 
            nodes = fff[0].split(">")
        end
        return nodes,attribute
    end

    def write(file = "")
        file = @initfile if file.empty?
        tw = File.new(file,"w")
        @newdoc.write(tw,2)            
        tw.close
    end

end

class GenerateInputs
    
    def initialize(jm)
        bundles = bundleize jm.structure
        bundles.each do |k,v|
            #puts b
            inputs = Document.new
            inputs << XMLDecl.new
            inputs.add_element("Inputs")
            # Check same output file
            root = inputs.root
            root.add_element "Configuration", {"InputDir" => k+"/"}
            # Add different input to the inputs file
            create_inputs v,root
            #Write file
            filename = File.open("#{k}/inputs.xml","w")
            inputs.write(filename,2)
        end
    end

    def create_inputs(bundle, root)
        bundle.each do |j|
            input  = root.add_element "Input", {"ID" => j["ID"], "File" => j["File"]+".root"}
            if j.include? "syst"
                attrs = Hash.new
                j["syst"].each do |ss|
                    input.add_element "ShapeSyst", ss
                end
            end
        end

    end

    def bundleize strc
        bundles = Hash.new
        strc.each do |j|
            if bundles[j["Path"]] == nil
                bundles[j["Path"]] = []
            end
            bundles[j["Path"]] << j
        end
        bundles
    end

end
