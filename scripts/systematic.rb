module NPA
    class Systematic
        VALID_TYPES = %w(UPDOWN SCALE VAR)
        attr_reader :info
        def initialize(args)
            @info = OpenStruct.new(args)
            validate
        end

        def affect?(job)
            return false if job[:data] == 1
            affected = false
            if @info.samples.nil?
                affected = true
            else
                affected = @info.samples.any?{|s| /#{s}/ =~ job[:ID]}
            end
            if !@info.paths.nil?
                affected &&= @info.paths.any?{|p| /#{p}/ =~ job[:output]}
            end
            affected
        end

        def validate
            if @info.ID.nil?
                raise ArgumentError, "All systematics must have an ID"
            end
            if !VALID_TYPES.include? @info.type
                raise ArgumentError, "The systematic #{@info.ID} has an invalid type (valid types are #{VALID_TYPES.join(",")})"
            end
            if @info.type == "UPDOWN" && (@info.up_opt.nil? || @info.down_opt.nil?)
                raise ArgumentError, "The systematic #{@info.ID} has been specified as #{@info.type} but no up_opt and/or down_opt properties have been specified"
            end
            if @info.type == "VAR" && @info.var_opt.nil?
                raise ArgumentError, "The systematic #{@info.ID} has been specified as #{@info.type} but no var_opt property has been specified"
            end
        end
    end
end

