require 'net/smtp'

module NPA
    module Notification
        class Email < Base
            def check_validity
                super
                if @params[:to].nil?
                    raise ArgumentError, "The :to parameter for your email notification has not been supplied."
                end
            end

            def send
`mail -s "#{@title}" #{@params[:to]}<<EOM
#{@body}
EOM`
true
            end
        end
    end
end
