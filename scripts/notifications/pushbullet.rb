require "washbullet"
module NPA
    module Notification
        class Pushbullet < Base
            def check_validity
                super
                if @params[:api_key].nil?
                    raise ArgumentError, "The :api_key for your PushBullet notification has not been supplied."
                end
            end

            def send
                client = Washbullet::Client.new(@params[:api_key])
                if @params[:devices].nil?
                    client.push_note(
                        receiver:   :device, # :email, :channel, :client
                        #identifier: '<IDENTIFIER>',
                        params: {
                            title: @title,
                            body:  @body
                        }
                    )
                else
                    client.devices.each do |dev|
                        next if !@params[:devices].include? dev.identifier
                        client.push_note(
                            receiver:   :device, # :email, :channel, :client
                            identifier: dev.identifier,
                            params: {
                                title: @title,
                                body:  @body
                            }
                        )
                    end
                end
                true
            end
        end
    end
end
