module NPA
    module Notification
        class Base
            attr_accessor    :title
            attr_accessor    :body
            attr_accessor    :params
            attr_accessor    :logger
            def initialize(params ={})
                @params = params
            end
            def check_validity
                if !@title.is_a? String
                    raise ArgumentError, "The title of the notification must be a string." 
                end
                if !@body.is_a?(String) && !@body.is_a?(File)
                    raise ArgumentError, "The body of the notification must be a string or a File." 
                end
                if !@params.is_a? Hash
                    raise ArgumentError, "The params of a notification is a Hash." 
                end
            end

            def notify
                begin
                    check_validity
                rescue ArgumentError => msg
                    puts "There was an error with the #{self.class} notification:\n\t#{msg}"
                    @logger.error "#{self.class}:\n\t#{msg}"
                else
                    sent = send
                    if sent
                    if !@logger.nil?
                        @logger.info "Notification sent with #{self.class} to #{@params[:to]}"
                    end
                    else
                    if !@logger.nil?
                        @logger.info "No notification has been sent using #{self.class}"
                    end
                    end
                end
            end
        end
    end
end
