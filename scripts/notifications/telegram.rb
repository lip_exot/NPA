require "telegram/bot"
require "rumoji"

module NPA
    module Notification
        class Telegram < Base
            def check_validity
                super
                if @params[:to].nil?
                     raise ArgumentError, "The :user_id parameter for the Telegram notification is not supplied. See @myidbot in Telegram to check your ID."
                end
            end
            def send
                ret = true
                begin
                ::Telegram::Bot::Client.run("155822968:AAFEddKJ-nlpmJYEOOg3WklXgXP7eVNuibA") do |bot|
                    newbody = @body.gsub("Jobs run:","#{Rumoji.decode(":page_facing_up:")} - Jobs run:")
                    newbody = newbody.gsub("Successful:","#{Rumoji.decode(":white_check_mark:")} - Successful:")
                    newbody = newbody.gsub("Failed:","#{Rumoji.decode(":bangbang:")} - Failed:")
                    msg = "#{@title}\n#{newbody}"
                    bot.api.send_message(chat_id: @params[:to], text: msg)
                end
                rescue ::Telegram::Bot::Exceptions::ResponseError => e
                        puts "ERROR: #{e.to_s}"
                        @logger.error "#{e.to_s}"
                        ret = false
                end
                ret
            end
        end
    end
end
