=begin 
classes to manage different type of clusters.
Can be expanded by the user if they want just making a derived class of
ClusterManager
=end
require "open3"
require "rexml/document"

class ClusterManager
    def initialize(opts={})
        @params = {
            :queue => "",
            :config => [],
            :flags => {},
            :scom => ""
        }.merge(opts)
    end

    def submit(input,test=false)
        if !input.respond_to? "get_exec"
            raise ArgumentError, "The job object used in ClusterManager needs to have the function get_exec defined."
        end
        if !input.respond_to? "ID"
            raise ArgumentError, "The job object used in ClusterManager needs to have a callable ID method."
        end
        puts input.get_exec if test
        jid = launch_job(input,test)
        return jid
    end

end

class PBSCluster < ClusterManager
    def initialize(opts={})
        o = {
            :queue => "default",
            :scom => "qsub"
        }.merge(opts)
        super(o)
    end

    def launch_job(input,test)
        flags = " -N #{input.ID}"
        @params[:flags].each {|k,v| flags += " #{k} #{v}"}
        file = File.open("tmp_npajob.sh","w")
        file.write("#!/bin/env bash\n")
        @params[:config].each {|x| file.write("#{x}\n")}
        file.write("#{input.get_exec}")
        file.close
        output = 1
        error = 1
        if !test
            comm = "#{@params[:scom]} #{flags} tmp_npajob.sh"
            input, output, error = Open3.popen3(comm)
            return output.read#return job id
        else
            return -1
        end
    end

    # Function that returns the exit status of a given job
    def exit_status(job_id)
        i,o,e,t = Open3.popen3("qstat -fx #{job_id}")
        error = e.read
        exit_status = 999
        if !error.include? "Unknown Job Id"
            root = REXML::Document.new(o.read).root
            job = root.elements["Job"]
            status = job.elements["job_state"].text
            exit_status = job.elements["exit_status"].text if status == "C"
        else
            exit_status = -99
        end
        #puts "status: #{exit_status} -> state: #{job.elements["job_state"].text}"
        return exit_status,exit_message(exit_status)
    end

    def exit_message(code)
        msg = ""
        case code 
        when 0   
            msg = "Sucessfully run"
        when -1  
            msg = "Job exec failed, before files, no retry"
        when -2  
            msg = "Job exec failed, after files, no retry"
        when -3  
            msg = "Job execution failed, do retry"
        when -4  
            msg = "Job aborted on MOM initialization"
        when -5  
            msg = "Job aborted on MOM init, chkpt, no migrate"
        when -6  
            msg = "Job aborted on MOM init, chkpt, ok migrate"
        when -7  
            msg = "Job restart failed"
        when -8  
            msg = "exec() of user command failed"
        when -99
            msg = "The job has finished before NPA could check if it eneded properly. Chec it manually to be sure."
        else 
            msg = "The job has been sucessfully laucnhed but the execution ended. Check the error log files."
        end
    end
end
