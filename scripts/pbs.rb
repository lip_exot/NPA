=begin
Cluster class for pbs cluster
=end

module NPA
    class PBSCluster < ClusterManager
        def initialize(opts={})
            o = {
                :queue => "default",
                :scom => "qsub"
            }.merge(opts)
            @status = {}
            @status[:run] = ["R"]
            @status[:iddle] = ["Q"]
            @status[:finished] = %w(C E)
            super(o)
        end

        def launch_job(input,test)
            flags = " -N #{input.ID}"
            @params[:flags].each {|k,v| flags += " #{k} #{v}"}
            file = File.open("tmp_job.sh","w")
            file.write("#!/bin/env bash\n")
            @params[:config].each {|x| file.write("#{x}\n")}
            file.write("#{input.get_exec}")
            file.close
            output = 1
            error = 1
            if !test
                comm = "#{@params[:scom]} #{flags} tmp_job.sh"
                input, output, error = Open3.popen3(comm)
                return output.read.split(".")[0]#return job id
            else
                return -1
            end
        end

        # Function that returns the exit status of a given job
        def exit_status(job_id)
            return 0,exit_message(0)
            if !@jobs[job_id].nil?
                if @jobs[job_id][:estatus] != 999
                    return @jobs[job_id][:estatus],@jobs[job_id][:emessage]
                end
            end
            i,o,e,t = Open3.popen3("qstat -fx #{job_id}")
            error = e.read
            exit_status = 999
            if !error.include? "Unknown Job Id"
                root = REXML::Document.new(o.read).root
                job = root.elements["Job"]
                status = job.elements["job_state"].text
                exit_status = job.elements["exit_status"].text if status == "C"
            else
                exit_status = -99
            end
            #puts "status: #{exit_status} -> state: #{job.elements["job_state"].text}"
            return exit_status,exit_message(exit_status.to_i)
        end

        def exit_message(code)
            msg = ""
            case code 
            when 0   
                msg = "Sucessfully run"
            when -1  
                msg = "Job exec failed, before files, no retry"
            when -2  
                msg = "Job exec failed, after files, no retry"
            when -3  
                msg = "Job execution failed, do retry"
            when -4  
                msg = "Job aborted on MOM initialization"
            when -5  
                msg = "Job aborted on MOM init, chkpt, no migrate"
            when -6  
                msg = "Job aborted on MOM init, chkpt, ok migrate"
            when -7  
                msg = "Job restart failed"
            when -8  
                msg = "exec() of user command failed"
            when -99
                msg = "The job has finished before NPA could check if it eneded properly. Chec it manually to be sure."
            else 
                msg = "The job has been sucessfully laucnhed but the execution ended. Check the log files."
            end
        end

        def finished?(id)
            @jobs[id][:status] == "C"                
        end

        def job(id)
            @jobs[id]
        end

        def update_jobs_information
            jj = Hash.new
            i,o,e,t = Open3.popen3("qstat | grep #{ENV["USER"]}")
            lines = o.readlines
            return if lines.nil?
            lines.each do |line|
                vals = line.split
                id = vals[0].split(".")[0]
                status = vals[4]
                #puts "ID: #{id}, Status: #{status}"
                jj[id] = {:status => status}
                if status == "C"
                    estatus,emessage = exit_status(id)
                    jj[id][:estatus] = estatus
                    jj[id][:emessage] = emessage
                else
                    jj[id][:estatus] = 999 # Deftaul not exit
                    jj[id][:emessage] = nil
                end
            end
            #@jobs.merge!(jj)
            @mutex.synchronize do
                @jobs.each do |k,v|
                    #puts "#{jj[k]}"
                    if !jj[k].nil?
                       # puts "NOT FOUND #{k}"
                       # v[:status] = "C" 
                       # v[:estatus] = -99
                       # v[:emessage] = exit_message(-99)
                    #else
                        #puts "ID: #{k}" 
                        #puts "Status: #{jj[k][:status]}" 
                        #puts "EStatus: #{jj[k][:estatus]}" 
                        #puts "EMesg: #{jj[k][:emessage]}" 
                        v[:status] = jj[k][:status]
                        v[:estatus] = jj[k][:estatus]
                        v[:emessage] = jj[k][:emssage]
                    end
                end
            end
        end
    end
end
