#!/bin/env ruby

# Automatically create a NPA analysis with the following structure
# - include
# - src
# - utils
# - cmt
# - run
# - samples
# Create Makefile and be able to compile a standalone analysis
# from scratch
BD = "#{ENV["NPADIR"]}/bin"
def createMakefile(analysis)
    lines = File.new("#{BD}/Makefile.template","r").read.gsub("@ANALYSIS@",analysis)
    newmake = File.new("#{analysis}/cmt/Makefile","w")
    newmake.write lines
    newmake.close
end

def createClass(analysis)
    # Create hader file
    header = File.new("#{analysis}/include/#{analysis}.h","w")
    header.write "#ifndef #{analysis}_h\n"
    header.write "#define #{analysis}_h\n"

    header.write "#include \"NPA.h\"\n"

    header.write "class #{analysis} : public NPA {\n"
    header.write "\t// Functions\n"
    header.write "\tpublic:\n"
    header.write "\tvirtual void DefineSamples(); //!< Defines input files for each sample\n"
    header.write "\t#{analysis} (int, const char**);\n"
    header.write "\t~#{analysis} ();\n"
    #header.write "\tvirtual void UserCommandLineOptions(char*, Int_t); //!< Load command line options\n"
    header.write "\tvirtual void DoCuts(); //!< Event selection\n"
    header.write "\tvirtual void BookHistograms(); //!< Define histograms\n"
    header.write "\tvirtual void FillHistograms(); //!< Fill histograms\n"
    header.write "};\n"
    header.write "#endif\n"
    header.close

    # Create source file
    source = File.new("#{analysis}/src/#{analysis}.cxx","w")
    source.write "// Automatically generated skeleton for #{analysis}\n"
    source.write "#include \"#{analysis}.h\"\n\n"
    # Constructor and destructor
    source.write "#{analysis}::#{analysis}(int argc, const char *argv[]): NPA(){\n"
    source.write "// Class constructor\n"
    source.write "\tMaxCuts = 0; // Set MaxCuts to maximum number of cuts defined in DoCuts\n"
    source.write "}\n\n"
    source.write "#{analysis}::~#{analysis}(){\n"
    source.write "// Class destructor\n"
    source.write "}\n\n"
    # Define samples
    source.write "void #{analysis}::DefineSamples(){\n"
    source.write "\t// Define TMonteCarlo objects\n"
    source.write "}\n\n"
    ## Book histograms
    source.write "void #{analysis}::BookHistograms(){\n"
    source.write "// Create here all the histograms you want to be saved with something like\n"
    source.write "// addHisto(\"n_jet\",\";Jet multiplicity;Events/bin\",13,-0.5,12.5);\n"
    source.write "}\n\n"
    ## Fill histograms
    source.write "void #{analysis}::FillHistograms(){\n"
    source.write "// In this function s whre the histograms are filled\n"
    source.write "// fillHisto(pt.str(),LeptonVec[i].Pt()/GeV,Weight);\n"
    source.write "}\n\n"
    ## DoCuts
    source.write "void #{analysis}::DoCuts(){\n"
    source.write "\t// DoCuts is used to implement cuts in the analysis. The structure is very simple:\n"
    source.write "\t// if (condition to reject the event) return;\n"
    source.write "\t// LastCut++;\n"
    source.write "\t// All event which is not rejected needs to increase the LastCut variable.\n"
    source.write "}\n"
end

def createStructure(analysis)
    `mkdir -p #{analysis}/include`
    `mkdir -p #{analysis}/src`
    `mkdir -p #{analysis}/utils`
    `mkdir -p #{analysis}/run`
    `mkdir -p #{analysis}/cmt`
    `mkdir -p #{analysis}/samples`
    `mkdir -p #{analysis}/lib`
    lines = File.new("#{BD}/Ana.template.cxx","r").read.gsub("@ANALYSIS@",analysis)
    newmain = File.new("#{analysis}/utils/RunNPA.cxx","w")
    newmain.write lines
    newmain.close
end



## Execute the script
if ARGV.length == 0
    puts "To create a template for an NPA anlaysis use:"
    puts "\t npa-crator [analysis name]"
    exit
end
ANA = ARGV[0]
puts "Crating analysis #{ANA}..."
puts "\t - Creating folder structure ..."
createStructure(ANA)
puts "\t - Creating template class ..."
createClass(ANA)
puts "\t - Creating default Makefile ..."
createMakefile(ANA)
