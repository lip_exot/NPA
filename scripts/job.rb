module NPA
    class NPAJob

        attr_accessor :exit_status
        attr_accessor :exit_msg
        attr_accessor :pid
        attr_reader :output_path
        def initialize(opts = {})
            @par = {
                :command => "NONE",
                :ID => "NONE",
                :origID => "NONE",
                :sample => -1,
                :data => 0,
                :output => "NONE",
                :nosyst => false,
                :options => {}
            }.merge(opts) # Read parameters from input
            begin
                checkvalidity
            rescue ArgumentError => msg
                abort "The initilization of NPAJob has failed due to bad formatted inputs:\n\t#{msg.message}"
            end
            @exit_status = nil
        end

        def ID
            @par[:ID]
        end

        def origID
            @par[:origID]
        end

        def isdata?
            @par[:data] == 1
        end

        def sample
            @par[:sample]
        end

        def options
            @par[:options]
        end

        def checkvalidity
            # Define parameters based on defaults
            if @par[:ID] == "NONE"
                raise ArgumentError, "NPAJob created without ID"
            end
            if @par[:sample] == -1 && !@par[:data]
                raise ArgumentError, "NPAJob created for MC without no sample. A sample must be specified"
            end
            if @par[:output] == "NONE"
                @par[:output] = "output/NPAOutput_#{@par[:ID]}"
            end
            @output_path = File.dirname @par[:output]
            #if !File.directory? dirname
            #    FileUtils.mkdir_p dirname
            #end
            if !@par[:options].is_a?Hash
                raise ArgumentError, "Options supplied to a NPAJob need to be a set of \"option\" => \"value\"\n Example: {\"ch\" => \"ee\", \"syst\" => \"nominal\"}"
            end
        end

        def output_path
            File.dirname(@par[:output])
        end

        def output_file
            File.basename(@par[:output])
        end

        def get_exec
            com = Command.new(@par[:command])
            com.format = "-KEY VALUE"
            com.add_option "output",@par[:output]
            com.add_option "sample",@par[:sample]
            com.add_option "data","" if @par[:data] == 1
            @par[:options].each {|k,v| com.add_option k.to_s,v.to_s}
            return com.to_s
        end

        def check_finish
            est = 1
            try = 0
            begin
                file = File.read(@par[:output]+".out") 
            rescue Errno::ENOENT
                try += 1
                while try < 5 && !File.exist?(@par[:output]+".out")
                    puts "ERROR: reading file #{@par[:output]+".out"}, attempt #{try}/5"
                    file = File.read(@par[:output]+".out") if File.exist?(@par[:output]+".out")
                    try += 1
                    sleep 1
                end
            end
            if try == 5
                puts "ERROR: The file #{@par[:output]+".out"} could not be read. Check for any issue with the code to see why it does not exist."
                @est = -2
            else
                file.each_line do |x|
                    if x.include? "FINISHED SUCCESSFULLY"
                        est = 0 
                        break
                    elsif x.include?("WARNING") && x.include?("has 0 files to run")
                        est = -1
                        break
                    end
                end
                @exit_status = est
                case est
                when 0
                    @exit_msg = "Success" 
                when 1 
                    @exit_msg = "The job has not finished successfully. Please take a look at the log files\n -> #{@par[:output]}.out/.err" 
                when -1
                    @exit_msg = "The job has finished but it has run over 0 files! Check that everything is properly configured"
                when -2
                    @exit_msg = "The output file could not be read/does not exist. Check to run it manually\n   Command: #{get_exec}"
                end
            end
        end

        def to_filter
            "#{@par[:ID]}_#{@par[:options].values.join("$$")}" 
        end
    end
end
