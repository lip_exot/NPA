module NPA
    class JobManager
        attr_accessor :joblist
        def initialize(par = {})
            @par = {
                :command => "NONE",
                :log => "jobmanager_log.txt"
            }.merge(par)
            @joblist = []
            @jobids = {}
            if @par[:command] == "NONE"
                raise ArgumentError, "NPAJobManager created with no command. Initialize it with the :command => \"command\" option supplied."
            end
            @to_filter = ""
            @jstruct = []
            @running_procs = []
            @submitted = []
            @notifier = []
            @not_filtered = []
            @log = Logger.new(File.new(@par[:log],"w"))
            @log.formatter = proc do |severity, datetime, progname, msg|
                "#{severity} [#{datetime.strftime('%Y-%m-%d %H:%M:%S.%6N')}]: #{msg}\n"
            end
            @logfile = File.absolute_path(@par[:log])
        end

        def add_notification(notifier)
            if notifier.is_a? Notification::Base
                notifier.logger = @log
                @notifier << notifier
            else
                msg =  "Object added for notification is of type #{notificer.class} which is not NPA::Notification::Base. It will be skipped."
                @log.warning msg
                puts "Warning: #{msg}"
            end
        end

        def load(*args)
            input,syst = args
            error = "Not supported type of input (and/or syst) to load. Supported types are: Array"
            # Load systematics if there are any
            if !syst.nil? && syst.class != Array
                case syst
                when String
                    if !File.exists? syst
                        raise ArgumentError, "You have entered a file to load systematics which does not exist"
                    else
                        syst = JSON.parse(File.read(syst))
                    end 
                else 
                    raise ArgumentError, error
                end
            end
            case input
            when Array
                load_from_array(input,syst)
            when String
                if !File.exists? input
                    raise ArgumentError, "You have entered a file to load inputs which does not exist"
                else
                    input = JSON.parse(File.read(input))
                end
            else 
                raise ArgumentError, error
            end
            sort_by_batches # Optimize order in jobs
            
            @joblist.map{|x| x.output_path}.uniq.each {|x| FileUtils.mkdir_p x}
        end

        def load_from_array(inputs,syst)
            syst &&= syst.map{|x| Systematic.new x}
            bar = ProgressBar.create(title: "Loading inputs ", total: inputs.length, length: 120, format: "%t (%j%% done) |%B|")
            inputs.each do |input|
                input.merge!({:command => @par[:command]})
                # Create nominal job
                nom = input.clone
                nom[:origID] = nom[:ID]
                nom[:options] = input[:options].merge({"syst" => "nominal"})
                nomj = NPAJob.new(nom)
                @joblist << nomj
                systematics = []
                if !syst.nil? && !input[:nosyst]
                    # Select systematics that apply to this input
                    systematics = syst.select{|s| s.affect? input}
                    systematics.each do |ss|
                        if ss.info.type == "VAR"
                            tmp = input.clone
                            tmp[:options] = input[:options].merge({"syst" => ss.info.var_opt})
                            tmp[:origID] = tmp[:ID]
                            tmp[:ID] += "_#{ss.info.var_opt}"
                            tmp[:output] += "_#{ss.info.var_opt}"
                            @joblist << NPAJob.new(tmp)
                        elsif ss.info.type == "UPDOWN"
                            tmp1 = input.clone
                            tmp2 = input.clone
                            tmp1[:origID] = tmp1[:ID]
                            tmp2[:origID] = tmp2[:ID]
                            tmp1[:ID] += "_#{ss.info.up_opt}"
                            tmp2[:ID] += "_#{ss.info.down_opt}"
                            tmp1[:options] = input[:options].merge({"syst" => ss.info.up_opt})
                            tmp2[:options] = input[:options].merge({"syst" => ss.info.down_opt})
                            tmp1[:output] += "_#{ss.info.up_opt}"
                            tmp2[:output] += "_#{ss.info.down_opt}"
                            @joblist << NPAJob.new(tmp1)
                            @joblist << NPAJob.new(tmp2)
                        end
                    end
                end
                ## Create structure of the jobs
                strc = Hash.new
                strc["ID"] = input[:ID]
                ofile = nomj.output_file
                strc["File"] = ofile
                strc["Path"] = nomj.output_path
                if !syst.nil?
                    systx = []
                    systematics.each do |ss|
                        tmp = {"ID" => ss.info.ID}
                        long = {"FileDown" => "#{ofile}_#{ss.info.down_opt}.root","FileUp" => "#{ofile}_#{ss.info.up_opt}.root"}
                        tmp.merge!(long) if ss.info.type == "UPDOWN"
                        tmp.merge!({"File" => "#{ofile}_#{ss.info.var_opt}.root"}) if ss.info.type == "VAR"
                        systx << tmp
                    end
                    strc["syst"] = systx
                end
                @jstruct << strc
                bar.increment
            end
            bar.finish
        end

        def sort_by_batches
            @joblist = @joblist.group_by{|i| "#{i.sample}-#{i.options["syst"]}" }.values                  # Chunks of equal values,
            .sort_by{|v| -v.size }.flatten                    # sorted by decreasing length,
            #.reduce(&:zip)                            # transposed,
            #.map{|x| x.flatten.compact.sort_by{|x| x.sample}}.flatten
        end

        def filter(tof)
            @to_filter = tof.clone
            @filtered  = @joblist.select{|x|  pass_filter(x.to_filter)}
            @not_filtered = @joblist.select{|x| !@filtered.include? x}
            @filter_count = @filtered.length
        end

        def structure
            @jstruct
        end

        def submit(par={})
            @start_time = Time.now
            sopt = {
                :cluster => "NONE",
                :sleep => 30,
                :test => false
            }.merge(par)
            cluster = ""
            case sopt[:cluster]
            when "PBS"
                puts "Using PBS cluster to submit jobs"
                cluster = PBSCluster.new(sopt)
            when "NONE"
                puts "WARNING: No cluster type has been set on NPAJobManager::submit. PBS wil be used as default"
                cluster = PBSCluster.new(sopt)
            when JobManager
                puts "Using custom cluster to submit jobs"
                cluster = sopt[:cluster]
                if !cluster.respond_to?"launch_job"
                    raise ArgumentError, "User defined ClsuterManager does not have launch_job method."
                end
                if !cluster.respond_to?"exit_status"
                    raise ArgumentError, "User defined ClsuterManager does not have exit_status method."
                end
                cluster.initialize(sopt)
            else
                raise ArgumentError, "Cluster type #{sopt[:cluster]} is not supported"
            end
            maxlength = 0
            @filtered = @joblist if @to_filter.empty?
            qbar = ProgressBar.create(title: "Submiting jobs ", total: @filtered.length, length: 120, format: "%t (%j%% done) |%B|")
            @filtered.each do |j|
                #if sopt[:test] 
                    #puts "Submit job: #{j.ID}"
                    #puts "Options: #{JSON.pretty_generate j.options}"
                #end
                maxlength = j.ID.length if j.ID.length > maxlength
                id = cluster.submit(j,sopt[:test])
                j.pid = id
                @submitted << j
                qbar.increment
            end
            qbar.finish
            #puts "Submitted jobs with PIDS: "
            #@submitted.each do |j|
            #    puts "* #{j.ID.ljust(maxlength)} -> #{j.pid}"
            #end
            do_while_wait(msg: "Waiting for jobs to finish...   "){
                loop do
                    ended = 0
                    @submitted.each do |j|
                        status = cluster.job(j.pid)[:estatus]
                        #puts "Read status: #{status}"
                        ended += 1 if status != 999
                    end
                    break if @submitted.length <= ended || sopt[:test]
                end
            }
            cluster.finish
            qbar = ProgressBar.create(title: "Checking status ", total: @filtered.length, length: 120, format: "%t (%j%% done) |%B|")
            @filtered.each{|x| 
              x.check_finish if !sopt[:test]
              qbar.increment
            }
            qbar.finish
            printout_log
        end

        def run(par={})
            @finished = 0
            @startime = Time.now
            sopt = {
                :max => "NONE",
                :wait => 5,
                :test => false,
                :verbose => false,
            }.merge(par)
            if sopt[:max] == "NONE"
                ncores = number_of_processors
                puts "INFO: No maximum number of jobs have been defined to run. The maximum number of cores (#{ncores}) will be used."
                sopt[:max] = number_of_processors 
            end
            @filtered.each do |j|
                if sopt[:verbose] 
                    puts "Running: => #{j.get_exec}"
                end
                if checkrunning(sopt[:max],sopt[:wait])
                    i,o,e,t = Open3.popen3(j.get_exec)
                    @running_procs << t
                    @submitted << j
                end
            end
            while @running_procs.length != 0
                print_wait
                @running_procs.each do |p|
                    begin
                        Process.getpgid(p.pid)
                    rescue Errno::ESRCH
                        @running_procs.delete p
                        @finished += 1
                    end
                end
                sleep sopt[:wait]
            end
            puts "\n --- Done! --- "
            @filtered.each{|x| x.check_finish if !sopt[:test]}
            printout_log
        end

        def pass_filter(id)
            # This function will get an string with the logic operation to filter by and 
            # use eval to filter as desired by matching each part with the ID and all options
            # of the given job
            return true if @to_filter.empty?
            evalstring = @to_filter.split.map do |x|
                if !%w(&& ! || \( \)).include? x
                    "!(/#{x}/ =~ id).nil?"
                else
                    x
                end
            end.join(" ")
            ret = eval(evalstring)
            #if ret
            #puts id
            #puts evalstring
            #end
            ret
        end

        def checkrunning(max,wait)
            loop do
                if @running_procs.length < max
                    return true
                else
                    @running_procs.each do |p|
                        begin
                            Process.getpgid(p.pid)
                        rescue Errno::ESRCH
                            @running_procs.delete p
                            @finished += 1
                        end
                    end
                end
                print_wait
                sleep wait
            end
        end

        def print_wait
            print "\rWaiting: idle[#{@filter_count - @running_procs.length - @finished}], running[#{@running_procs.length}], finished[#{@finished}]\s\t Elapsed time: #{(Time.now - @startime).duration}"
            STDOUT.flush
        end

        def generate_xml
            GenerateInputs.new(self)
        end

        def number_of_processors
            if RUBY_PLATFORM =~ /linux/
                return `cat /proc/cpuinfo | grep processor | wc -l`.to_i
            elsif RUBY_PLATFORM =~ /darwin/
                return `sysctl -n hw.logicalcpu`.to_i
            elsif RUBY_PLATFORM =~ /win32/
                # this works for windows 2000 or greater
                require 'win32ole'
                wmi = WIN32OLE.connect("winmgmts://")
                wmi.ExecQuery("select * from Win32_ComputerSystem").each do |system| 
                    begin
                        processors = system.NumberOfLogicalProcessors
                    rescue
                        processors = 0
                    end
                    return [system.NumberOfProcessors, processors].max
                end
            end
            raise "can't determine 'number_of_processors' for '#{RUBY_PLATFORM}'"
        end

        def printout_log
            @end_time = Time.now
            @log.info "#######################################"
            @log.info "######      NPA Run finished     ######"
            @log.info "#######################################"
            @log.info " Start time: #{@start_time.strftime("%d/%m/%y - %H:%M")}"
            @log.info " End time: #{@end_time.strftime("%d/%m/%y - %H:%M")}"
            @log.info " Elapsed time: #{(@end_time-@start_time).duration}"
            @success = []
            @failed = []
            @submitted.each do |j|
                if j.exit_status.to_i == 0
                    @success << j
                else
                    @failed << j
                end
            end
            @log.info " Job successfully ended: #{@success.length}/#{@submitted.length}"
            @log.info " Job failed: #{@failed.length}/#{@submitted.length}"
            @log.info " ----- Information about failed jobs -----" if !@failed.empty?
            @failed.each do |j|
                @log.error "---> Job ID: #{j.ID}"
                @log.error " \tExit status: Exit #{j.exit_status} -> #{j.exit_msg}"
                #@log.info " \tCommand: #{j.get_exec}"
            end
            @log.info " ----- Submitted jobs -----"
            @log.info " List of jobs grouped by ID and sets of options "
            list = {}
            @filtered.each do |j|
                list[j.origID] = {} if list[j.origID].nil? 
                j.options.each do |k,v|
                    list[j.origID][k] = [] if list[j.origID][k].nil?
                    list[j.origID][k] << v
                end
            end
            list.each do |id,o|
                opts = []
                o.each do |op,vals|
                    if vals.uniq.length> 1
                        opts << "#{op} (#{vals.uniq.length})"
                    end
                end
                @log.info " * #{id}: #{opts.join(", ")}"    
            end
            if !@not_filtered.empty?
                @log.info " ----- Not submitted jobs -----"
                @log.info " The following jobs were defined but didn't passed any filter "
                list = {}
                @not_filtered.each do |j|
                    list[j.origID] = {} if list[j.origID].nil? 
                    j.options.each do |k,v|
                        list[j.origID][k] = [] if list[j.origID][k].nil?
                        list[j.origID][k] << v
                    end
                end
                list.each do |id,o|
                    opts = []
                    o.each do |op,vals|
                        if vals.uniq.length> 1
                            opts << "#{op} (#{vals.uniq.length})"
                        end
                    end
                    @log.info " * #{id}: #{opts.join(", ")}"    
                end
            end
            @notifier.each{|x| notify x}
        end

        def notify(notifier)
            message = "
NPA has finished running in #{(@end_time-@start_time).duration}.
     Jobs run: #{@submitted.length}
     Successful: #{@success.length}
     Failed: #{@failed.length}

For detailed information take a look at the logfile
     #{@logfile}"
     notifier.title = "NPA notification (#{@end_time.strftime("%d/%m/%y - %H:%M")})"
     notifier.body = message
     notifier.notify
        end
    end
end
