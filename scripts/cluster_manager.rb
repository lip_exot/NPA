=begin 
Classes to manage different type of clusters.
Can be expanded by the user if they want just making a derived class of
ClusterManager, this class must contain a launch_job function that provides
the propper commands to launch jobs in each cluster
=end

module NPA
    class ClusterManager
        def initialize(opts={})
            @params = {
                :queue => "",
                :config => [],
                :flags => {},
                :sleep => 30,
                :scom => ""
            }.merge(opts)
            @jobs = {}
            @mutex = Mutex.new
            @check_jobs = true
            @thread = Thread.new{
                while @check_jobs do
                    update_jobs_information
                    sleep @params[:sleep]
                end
            }
        end

        def submit(input,test=false)
            if !input.respond_to? "get_exec"
                raise ArgumentError, "The job object used in ClusterManager needs to have the function get_exec defined."
            end
            if !input.respond_to? "ID"
                raise ArgumentError, "The job object used in ClusterManager needs to have a callable ID method."
            end
            jid = launch_job(input,test)
            @mutex.synchronize{@jobs[jid]={:estatus => 999}}
            return jid
        end

        def restart
            @mutex.synchronize{
                @jobs.clear
            }
        end
        
        def finished?(id)
            
        end

        def n_running
            @jobs.select{|k,v| @status[:run].include? v[:status]}.length
        end

        def n_iddle
            @jobs.select{|k,v| @status[:iddle].include? v[:status]}.length
        end

        def n_finished
            @jobs.select{|k,v| @status[:finished].include? v[:status]}.length
        end

        def n_total
            @jobs.length
        end

        def finish
            @check_jobs = false
            @thread.join
        end
    end
end
