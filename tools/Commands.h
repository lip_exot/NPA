#ifndef COMMANDS_H
#define COMMANDS_H
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <algorithm>
#include <iostream>

// Auxiliary functions to allow boolean specialization
namespace commands_tools{
    inline std::vector<std::string> splitString(std::string ss, const char *sep = ","){
        std::vector<std::string> result;
        std::string buf = "";
        for (int i = 0; i < ss.length(); i++) {
            if (ss[i] == *sep){
                result.push_back(buf);
                buf="";
            } else {
                buf += ss[i];
            }
        }
        result.push_back(buf);
        return result;
    }

    template<typename T>
        inline T read(const std::string &key, T def, std::map<std::string,std::string> &vals){
            std::stringstream ss(vals[key]);
            T result;
            return (ss >> result) ? result : def;
        }

    template<>
        inline std::string read<std::string>(const std::string &key, std::string def, std::map<std::string,std::string> &vals){
            return vals[key];
        }

    template<>
        inline bool read<bool>(const std::string &key,bool def, std::map<std::string,std::string> &vals)
        {
            std::string val = vals[key];
            transform(val.begin(), val.end(),
                    val.begin(), ::toupper);
            if(val != "TRUE" && val != "FALSE") return def;
            return (val == "TRUE");
        }

    template<typename T>
        inline std::vector<T> readArray(const std::string &key, std::vector<T> def, std::map<std::string,std::string> &vals){
            bool valid = true;
            std::vector<T> result;
            for(auto &&x : splitString(vals[key])){
                std::stringstream ss(x);
                T rr;
                if(!(ss >> rr)) valid = false;
                if(valid) result.push_back(rr);
            }
            return (valid) ? result : def;
        }

    template<>
        inline std::vector<bool> readArray<bool>(const std::string &key,std::vector<bool> def, std::map<std::string,std::string> &vals)
        {
            bool valid = true;
            std::vector<bool> result;
            for(auto &&x : splitString(vals[key])){
                transform(x.begin(), x.end(),
                        x.begin(), ::toupper);
                if(x != "TRUE" && x != "FALSE") return def;
                result.push_back(x == "TRUE");
            }
            return result;
        }
}

//namespace NPA{

class Commands {
    public:
        Commands (){};
        virtual ~Commands (){};

        static bool hasKey(std::string val){return s_opt_vals.find(val) != s_opt_vals.end();};
        template<typename T> 
            static T read(const std::string &key,T def){
                if(!hasKey(key)) return def;
                else return commands_tools::read(key,def,s_opt_vals);
            }
        template<typename T> 
            static T must(const std::string &key, T def){
                if(!hasKey(key)) {
                    throw std::invalid_argument("ERROR: The option "+key+" has been defined as mandatory and is not present");
                }
                else return commands_tools::read(key,def,s_opt_vals);
            }
        template<typename T> 
            static std::vector<T> readArray(const std::string &key,std::vector<T> def){
                if(!hasKey(key)) return def;
                else return commands_tools::readArray(key,def,s_opt_vals);
            }
        static void initialize(int argc, const char* argv[]);
        static int loadFile();
        static void printout();

    private:
        static std::map<std::string,std::string> s_opt_vals;
        static bool s_initialized;
};

//}
#endif
