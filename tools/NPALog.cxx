#include "NPALog.h"

NPALog::NPALog(){
}

NPALog::~NPALog(){
		m_logOutputFile.close();
		m_errorOutputFile.close();
}

void NPALog::setOutput(const char* fileName){
	std::string out=fileName; out+=".log";
	std::string err=fileName; err+=".err";
	m_logOutputFile.open(out.c_str());
	m_errorOutputFile.open(err.c_str());
}

void NPALog::log(const char* message){
	if (!m_logOutputFile.is_open())
	{
		setOutput("LogMessages");
	}
	m_logOutputFile<<message<<std::endl;
}

void NPALog::error(const char* message){
	if (!m_errorOutputFile.is_open())
	{
		setOutput("LogMessages");
	}
	m_errorOutputFile<<"ERROR: "<<message<<std::endl;
	std::cerr<<"ERROR: "<<message<<std::endl;
}

void NPALog::warning(const char* message){
	if (!m_logOutputFile.is_open())
	{
		setOutput("LogMessages");
	}
	m_errorOutputFile<<"WARNING: "<<message<<std::endl;
}
