// NPAColor is a class designed to read the color properties from the xml file in a readible 
// string format and translate it to the ROOT TColor language.
#include <iostream>
#include <string>
#include <map>
#include "TColor.h"

class NPAColor {
    public:
        NPAColor ();
        virtual ~NPAColor ();

        static int parseColor(std::string color);
        static std::map<std::string,int> s_colors;
};
