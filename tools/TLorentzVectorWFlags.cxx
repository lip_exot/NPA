// #################################################################################
//
//                                         
//          888b      88  88888888ba      db         
//          8888b     88  88      "8b    d88b        
//          88 `8b    88  88      ,8P   d8'`8b       
//          88  `8b   88  88aaaaaa8P'  d8'  `8b      
//          88   `8b  88  88""""""'   d8YaaaaY8b     
//          88    `8b 88  88         d8""""""""8b    
//          88     `8888  88        d8'        `8b   
//          88      `888  88       d8'          `8b  
//                                         
//                                         
//                                                                              
// #################################################################################
//                                                                              
//   NPA                                         \      | | / |  /    |   |  ||  
//                                                '-___-+-+'  |_/     |   |  |   
//   by J.P. Araque, N. Castro, V. Oliveira      `--___-++--^'|       |   | /|   
//                                                      ||    |       |   |' |   
//   date: 19-12-2013                            --___  ||    |       | _/|  |   
//                                               ==---:-++___ |  ___--+'  |  |   
//                                                 '--_'|'---'+--___  |   |  |   
//                                                     '+-_   |     '-+__ |  |   
//                                              ._.          ._.       ._____   
//                                              | |          | |       | ___ \  
//                                              | |_.        | |       | .___/  
//                                              |___|        |_|       |_|
//      
//   Note: 	this code was built from LipCbrAnalysis developed by 
//     	   	Nuno Castro   (nfcastro@lipc.fis.uc.pt)  and 
//         	Filipe Veloso (fveloso@lipc.fis.uc.pt)             
//   Purpose: NPA is intended to be a complete API to develop new physics analyses
//            using NPA as an interface class.
//   Version: 1.0 (This is a quick adaptation of LipCbrAnalysis and several changes
//                are foreseen).
//
//
// #################################################################################

#define TLorentzVectorWFlags_cxx

#include "TLorentzVectorWFlags.h"

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(): TLorentzVector(), idx(-1), isb(-1), IsoDeltaR(999), itruthMatch(-1), itrigMatch(-1){
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(Double_t px, Double_t py, Double_t pz, Double_t E, Int_t index, Int_t index2, double p_IsoDeltaR, Int_t index3, Int_t index4) :
  TLorentzVector(px,py,pz,E), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4),truth_flavor(-1) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(TLorentzVector v, int index, int index2, double p_IsoDeltaR, int index3, int index4) :
  TLorentzVector(v), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4),truth_flavor(-1) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(const TLorentzVectorWFlags& other) :
  TLorentzVector(other), idx(other.idx), isb(other.isb), IsoDeltaR(other.IsoDeltaR), itruthMatch(other.itruthMatch), itrigMatch(other.itrigMatch),truth_flavor(other.truth_flavor) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags& TLorentzVectorWFlags::operator=(const TLorentzVectorWFlags& other) {
// #############################################################################

  if (&other==this) {
    return *this ;
  }
  TLorentzVector::operator=(other) ;
  idx = other.idx ;
  isb = other.isb ;
  IsoDeltaR = other.IsoDeltaR;
  itruthMatch = other.itruthMatch ;
  itrigMatch  = other.itrigMatch ;
  return *this ;
}

// #############################################################################
TLorentzVectorWFlags::~TLorentzVectorWFlags() {
// #############################################################################

}
