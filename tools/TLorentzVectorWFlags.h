// #################################################################################
//
//                                         
//          888b      88  88888888ba      db         
//          8888b     88  88      "8b    d88b        
//          88 `8b    88  88      ,8P   d8'`8b       
//          88  `8b   88  88aaaaaa8P'  d8'  `8b      
//          88   `8b  88  88""""""'   d8YaaaaY8b     
//          88    `8b 88  88         d8""""""""8b    
//          88     `8888  88        d8'        `8b   
//          88      `888  88       d8'          `8b  
//                                         
//                                         
//                                                                              
// #################################################################################
//                                                                              
//   NPA                                         \      | | / |  /    |   |  ||  
//                                                '-___-+-+'  |_/     |   |  |   
//   by J.P. Araque, N. Castro, V. Oliveira      `--___-++--^'|       |   | /|   
//                                                      ||    |       |   |' |   
//   date: 19-12-2013                            --___  ||    |       | _/|  |   
//                                               ==---:-++___ |  ___--+'  |  |   
//                                                 '--_'|'---'+--___  |   |  |   
//                                                     '+-_   |     '-+__ |  |   
//                                              ._.          ._.       ._____   
//                                              | |          | |       | ___ \  
//                                              | |_.        | |       | .___/  
//                                              |___|        |_|       |_|
//      
//   Note: 	this code was built from LipCbrAnalysis developed by 
//     	   	Nuno Castro   (nfcastro@lipc.fis.uc.pt)  and 
//         	Filipe Veloso (fveloso@lipc.fis.uc.pt)             
//   Purpose: NPA is intended to be a complete API to develop new physics analyses
//            using NPA as an interface class.
//   Version: 1.0 (This is a quick adaptation of LipCbrAnalysis and several changes
//                are foreseen).
//
//
// #################################################################################

#ifndef TLorentzVectorWFlags_h
#define TLorentzVectorWFlags_h

#include <TLorentzVector.h>
#include <vector>

// #############################################################################
class TLorentzVectorWFlags : public TLorentzVector {
// #############################################################################
// 
//  purpose: TlorentzVector with additional relevant info
//
//  authors: A.Onofre
//  first version: 25.Nov.2012
//
//  last change: 25.Nov.2012
//  by: A.Onofre
//
// #############################################################################

 public:
  // 
  TLorentzVectorWFlags() ;
  TLorentzVectorWFlags(Double_t px, Double_t py, Double_t pz, Double_t E, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(TLorentzVector v, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(const TLorentzVectorWFlags& other);
  //
  ~TLorentzVectorWFlags();
  TLorentzVectorWFlags& operator=(const TLorentzVectorWFlags& other) ;

  inline void SetIsoDeltaR(double p_IsoDeltaR) {IsoDeltaR = p_IsoDeltaR;};
  int truthFlavor(){return truth_flavor;};
  void setTruthFlavor(int tf){truth_flavor = tf;};

  int idx ;
  int isb ;
  double IsoDeltaR;
  int itruthMatch; 
  int itrigMatch;
  int  truth_flavor;

 private:

  //  ClassDef(TLorentzVectorWFlags,0)
} ;

#endif
