/**
 * Lightweight class to manage log messages thtough different tools and make them all 
 * homogeneus.
 */
 #ifndef NPALOG_H
 #define NPALOG_H
#include <iostream>
#include <fstream>

 class NPALog{
public:
	NPALog();
	~NPALog();

	void error(const char* message);
	void warning(const char* message);
	void log(const char* message);

	void setOutput(const char* fileName);
	std::ofstream* getLogStream(){return &m_logOutputFile;};
	std::ofstream* getErrorStream(){return &m_errorOutputFile;};


private:
	char* m_fileName;
struct{
	short outputset:1;
} m_info;
	std::ofstream m_logOutputFile;
	std::ofstream m_errorOutputFile;

 };
 #endif

