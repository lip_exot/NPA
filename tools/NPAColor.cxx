#include "NPAColor.h"
#include <vector>
#include <regex>

using namespace std;

vector<string> splitString(string s, const char *sep = ";"){
    vector<string> result;
    if (s.find(sep) == string::npos) return result;
    string buf = "";
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == *sep){
            size_t fw = buf.find_first_not_of(" ");
            size_t lw = buf.find_last_not_of(" ");
            if (fw == std::string::npos) fw = 0;
            string trimmed = buf.substr(fw,lw-fw+1);
            result.push_back(trimmed);
            buf="";
        } else {
            buf += s[i];
        }
    }
    size_t fw = buf.find_first_not_of(" ");
    size_t lw = buf.find_last_not_of(" ");
    if (fw == std::string::npos) fw = 0;
    string trimmed = buf.substr(fw,lw-fw+1);
    result.push_back(trimmed);
    return result;
}

map<string,int> NPAColor::s_colors  {
    {"white" , kWhite},
        {"black", kBlack},
        {"red", kRed},
        {"blue", kBlue},
        {"green", kGreen},
        {"orange", kOrange},
        {"yellow", kYellow},
        {"magenta", kMagenta},
        {"cyan", kCyan},
        {"spring", kSpring},
        {"teal", kTeal},
        {"azure", kAzure},
        {"violet", kViolet},
        {"pink", kPink},
        {"gray", kGray}
};

NPAColor::NPAColor(){
}

NPAColor::~NPAColor(){
}

int NPAColor::parseColor(string color){
    // TODO add more methods to pass color
    int ret = -1;
    vector<string> sp = splitString(color,"+");
    vector<string> sm = splitString(color,"-");
    if (sp.size() == 2) ret = s_colors[sp[0]]+atoi(sp[1].c_str()); 
    else if (sm.size() == 2) ret = s_colors[sm[0]]-atoi(sm[1].c_str()); 
    else if (s_colors.find(color) != s_colors.end()) ret = s_colors[color];
    else    cout<<"ERROR: The color " << color << "is not defined in NPAColor."<<endl;
    // Save this method until we have a most recent compiler (4.9+)
    /*smatch sm;
    if (regex_match(color,sm,regex("(.*)([\\+\\-])(.*)")))
      ret = s_colors[sm[1].str()]+s_colors[sm[2].str()]*atoi(sm[3].str().c_str());*/
    return ret;
}
