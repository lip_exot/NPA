#include "Commands.h"
#include "json11.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;
using namespace json11;

// Static vars init
bool Commands::s_initialized = false;
map<string,string> Commands::s_opt_vals = map<string,string>();

int Commands::loadFile(){
    // Reader json configurfile
    string jsfile = read("conf",string("settings.json"));
    ifstream ff(jsfile);
    if (!ff.good()) return 1;
    std::string content( (std::istreambuf_iterator<char>(ff) ),
            (std::istreambuf_iterator<char>()    ) );
    string err;
    auto json_config = Json::parse(content,err,JsonParse::COMMENTS);
    if (!err.empty()){
        cerr<<"Error reading the settings file: "<<err<<endl;
        return 1;
    }
    for(auto &v : json_config.object_items()){
        string key = v.first;
        auto val = v.second;
        if (s_opt_vals.find(key) == s_opt_vals.end()){// Only to this if no value has been specified via command line
            // Treat types separately
            if (val.is_number()){
                stringstream ss;
                ss << val.number_value();
                s_opt_vals.insert(make_pair(key,ss.str()));
            } else if(val.is_string()){
                s_opt_vals.insert(make_pair(key,val.string_value()));
            } else if(val.is_bool()){
                string ss = (val.bool_value()) ? "TRUE" : "FALSE";
                s_opt_vals.insert(make_pair(key,ss));
            } else if(val.is_array()){ // TODO: Implement a better way for arrays, for now string comma separated
                string result = "";
                for(auto &v : val.array_items()){
                    if(v.is_string()) result += v.string_value()+",";
                    else result += v.dump()+",";
                }
                result.erase(result.length()-1,1);
                s_opt_vals.insert(make_pair(key,result));
            }
        }   
    }
    return 0;
}

void Commands::initialize(int argc, const char* argv[]){
    //if (s_initialized) return;
    // Loop over all options
    if(argc > 1){
        for(int ii = 1; ii < argc; ii++){
            // Check trailing chars
            string current(argv[ii]);
            if (current[0] == '-'){
                current.erase(0,1);
                if(current[0] == '-') current.erase(0,1);
                // Check that the option has not been used before
                if(s_opt_vals.find(current) != s_opt_vals.end()){
                    cerr << "WARNING: The option " << current << "has been entered twice. The second time will be ignored." << endl;
                } else {
                    if (ii < argc-1){
                        string next = argv[ii+1];
                        if (next[0] != '-'){
                            s_opt_vals.insert(make_pair(current,next));
                            ii++;
                        } else {
                            s_opt_vals.insert(make_pair(current,"TRUE"));
                        }
                    } else {
                        s_opt_vals.insert(make_pair(current,"TRUE"));
                    }
                }
            }
        }
    }
    loadFile();
    s_initialized = true;
}

void Commands::printout(){
    cout << " ------ Opions specified ------ "<<endl;
    int leng = 0;
    for(auto &v : s_opt_vals){
        if (v.first.length() > leng) leng = v.first.length();
    }

    for(auto &opt : s_opt_vals){
        cout << " | " << left << setw(leng + 2) << opt.first << " : " << opt.second <<endl;
    }
}
