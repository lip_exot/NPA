#include "NPAYields.h"
#include <numeric>
#include <iostream>
#include <cmath>

using namespace std;

NPAYields::NPAYields():m_defined(false){
    m_stat_errors.resize(0);
    m_up_syst_errors.resize(0);
    m_down_syst_errors.resize(0);
}

NPAYields::NPAYields(const NPAYields& other){
    cout<<"Size: "<<other.m_stat_errors.size()<<endl;
    m_stat_errors.insert(m_stat_errors.end(),
                         other.m_stat_errors.begin(),
                         other.m_stat_errors.end());
    m_up_syst_errors.insert(m_up_syst_errors.end(),
                         other.m_up_syst_errors.begin(),
                         other.m_up_syst_errors.end());
    m_down_syst_errors.insert(m_down_syst_errors.end(),
                         other.m_down_syst_errors.begin(),
                         other.m_down_syst_errors.end());
    m_yields = other.m_yields;
    m_yields_source = other.m_yields_source;
    m_defined - other.m_defined;
}

NPAYields::~NPAYields(){
}

double NPAYields::computeError(vector<double> v){
    if(!m_defined){
        cerr<<"ERROR: NPAYields cannot derive the yields errors since no"<<endl;
        cerr<<"source (histogram) has been defined"<<endl;
        exit(1);
    }
    double result=0;
    return sqrt(accumulate(v.begin(),
                v.end(),
                result,
                quadraticSum));
}

double NPAYields::getUpError(){
    if(!m_defined){
        cerr<<"ERROR: NPAYields cannot derive the yields errors since no"<<endl;
        cerr<<"source (histogram) has been defined"<<endl;
        exit(1);
    }
    double result = 0;
    accumulate(m_stat_errors.begin(),
                m_stat_errors.end(),
                result,
                quadraticSum);
    accumulate(m_up_syst_errors.begin(),
                m_up_syst_errors.end(),
                result,
                quadraticSum);
    return sqrt(result);
}

void NPAYields::reset(){
    m_stat_errors.clear();
    m_up_syst_errors.clear();
    m_down_syst_errors.clear();
    m_yields = 0;
}

double NPAYields::getDownError(){
    if(!m_defined){
        cerr<<"ERROR: NPAYields cannot derive the yields errors since no"<<endl;
        cerr<<"source (histogram) has been defined"<<endl;
        exit(1);
    }
    double result = 0;
    accumulate(m_stat_errors.begin(),
                m_stat_errors.end(),
                result,
                quadraticSum);
    accumulate(m_down_syst_errors.begin(),
                m_down_syst_errors.end(),
                result,
                quadraticSum);
    return sqrt(result);
}
