/**
NPAText is a NPADrawable that writes text
in a given position.
It inheris from TLatex which means that everything
you can do with TLatex can be done with NPAText.
*/
#include "TLatex.h"
#include "NPADrawable.h"

class TMarker;

class NPAText:public NPADrawable, public TLatex
{
public:
	NPAText(const char* id);
	NPAText(NPAText &t);
	NPAText(const char* id, double x, double y, const char* text);
	virtual ~NPAText();

	virtual double getLength();
	virtual double getHight();
	virtual void draw(const char* opt);

	void addMarker(const char* pos, double d, int marker);
	TMarker* getMarker(){return m_marker;}

protected:
	TMarker* m_marker;
	int m_marker_type;
	const char* m_text;

	
};
