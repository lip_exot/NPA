#ifndef NPAYIELDS_H
#define NPAYIELDS_H
#include <vector>
#include <string>
#include <cstdlib>

class NPAYields {
public:
    NPAYields ();
    NPAYields(const NPAYields& other);
    virtual ~NPAYields ();

    /// Methods to get yields and errors
    double getYields(){if(!m_defined){reset();defineYields();}return m_yields;};
    double getStatError(){return computeError(m_stat_errors);};
    double getUpSystError(){return computeError(m_up_syst_errors);};
    double getDownSystError(){return computeError(m_down_syst_errors);};
    double getUpError();
    double getDownError();
    void reset();
    
    /// Set histogram used to derive the yields
    void setYieldsSource(std::string source){reset();
                                    m_yields_source=source;
                                    defineYields();m_defined=true;};
protected:
    // This function needs to be defined for each derived class
    // and is where all variables needed for the getters will be 
    // defined.
    virtual void defineYields()=0;
    double computeError(std::vector<double> v);
    
    // Variables
    std::vector<double> m_stat_errors;
    std::vector<double> m_up_syst_errors;
    std::vector<double> m_down_syst_errors;
    double m_yields;
    std::string m_yields_source;

    static double quadraticSum(double a, double b){return a+b*b;};
    bool m_defined;
    
};
#endif
