#include "NPATable.h"
#include <iostream>

using namespace std;

NPATable::NPATable(int rows, int columns){
    // Create array of cells to fill
    for (int i = 0; i < rows; i++) {
        vector<NPATableCell> tmpc;
        tmpc.resize(columns);
        m_cells.push_back(tmpc);
    }
    m_columns_width.resize(columns);
    m_rows = rows;
    m_columns = columns;
}

NPATable::~NPATable(){
}

void NPATable::addCell(int x, int y, string content){
    if(x > m_rows || y > m_columns){
        cerr<<"ERROR: The table has been created with "<<m_rows<<" rows and "<<m_columns<<" columns."<<endl;
        cerr<<"A cell in coordinates ("<<x<<","<<y<<") cannot be added"<<endl;
        exit(1);
    }
    if(content.length() + 4 > m_columns_width[y])
        m_columns_width[y] = content.length() + 4;
   m_cells[x][y] = NPATableCell(x,y,content); 
}

void NPATable::write(ofstream &file){
    updateWidths();
    //Start table
    file<<"{\\renewcommand{\\arraystretch}{"<<m_stretch<<"}"<<endl;
    file<<"\\begin{tabular}{l";
    for(int i = 1; i <m_columns; i++) file<<"|c";
    file<<"|}"<<endl;
    for (int x = 0; x < m_rows; x++) {
        file<<"\\hline"<<endl;
        /*cout<<"+"<<m_cells[x][0].write()<<"+"<<endl;*/
        file<<m_cells[x][0].write();
        for (int y = 1; y < m_columns; y++) {
            file<<"&"<<m_cells[x][y].write();
        }
        file<<"\\\\";
    }
    file<<endl<<"\\hline"<<endl;
    file<<"\\end{tabular}"<<endl;
    file<<"}"<<endl;

}

void NPATable::updateWidths(){
    for (int x = 0; x < m_rows; x++) {
        for (int y = 0; y < m_columns; y++) {
            m_cells[x][y].updateWidth(m_columns_width[y]);
        }
    }
}
