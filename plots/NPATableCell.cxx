#include "NPATableCell.h"
#include <string>
#include <iostream>

using namespace std;

NPATableCell::NPATableCell(int x, int y, string content){
    m_X = x;
    m_Y = y;
    m_content = content;
    m_width = content.length();
}

NPATableCell::~NPATableCell(){
}

string NPATableCell::write(){

    // Find the spaces around the content to write
    // and format the content to fill the width.
    /*cout<<"Goint to write the content "<<m_content<<endl;
    cout<<"Width: "<<m_width<<endl;*/
    int left = m_width - m_content.length();
    int right = (left%2 == 0) ? left : left+2;
    string sl(left/2,' ');
    string sr(right/2,' ');
    /*cout<<"+"<<sl+m_content+sr<<"+"<<endl;*/
    return sl+m_content+sr;
}
