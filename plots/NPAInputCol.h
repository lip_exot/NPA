#ifndef NPAINPUTCOL_H
#define NPAINPUTCOL_H

#include "NPAXML.h"
#include "NPAPlotInput.h"
#include <vector>
#include <string>


class NPAInputCol:public NPAXML
{
public:
	NPAInputCol(const char* xml, std::string root="Inputs");
	~NPAInputCol();

	std::vector<NPAPlotInput*> getInputs(){return m_input_col;};
	NPAPlotInput* getInput(std::string ID);
    NPAPlotInput* nextInput();
    void parse();
    void resetInputCounter(){m_actual_input = m_input_col.begin();};
    void filterInputs(std::vector<std::string> tofilter){m_to_filter=tofilter;};

    // Use systeamtics or not
    void doSyst(bool s){m_do_syst = s;};

    static bool sameInputID(NPAPlotInput *i, std::string ii){return i->getID()==ii;}
    static bool toFilter(PlotProperties::input_t i, std::vector<std::string> tofilter){
        return std::find(tofilter.begin(),
                         tofilter.end(),
                         i.id) == tofilter.end();
    };
private:

	std::vector<NPAPlotInput*> m_input_col;
    std::vector<NPAPlotInput*>::iterator m_actual_input;
    std::vector<std::string> m_to_filter;	
    bool m_do_syst;
};


#endif
