#include "NPATableWriter.h"
#include "NPAPlotSample.h"
#include <sstream>
#include <iostream>
#include <math.h>

using namespace std;

NPATableWriter::NPATableWriter(const char *xml) : NPAXML(xml,"Tables"){
    m_bits = bits();
    cout<<"------ "<<m_bits.merge_errors<<endl;
}

NPATableWriter::~NPATableWriter(){

    for (int i = 0; i < m_samples.size(); i++) {
        delete m_samples[i];
    }
}

void NPATableWriter::configureYields(pugi::xml_node yields){
    if(readOptionStr("OutputDir",m_output_dir) != 0) m_output_dir=".";
    // Read output file
    if(!yields){
        cerr<<"ERROR: The yields table cannot be written since no YieldsTable node has been defined."<<endl;
        cerr<<"Please define the YieldsTable node with the information neeeded."<<endl;
        exit(1);
    }
    string output;
    if(yields.attribute("Output"))
        output = m_output_dir+"/"+yields.attribute("Output").value();
    else output = m_output_dir+"/yields_table.tex";
    if(!m_bits.yfile_created){
        m_yields_file.open(output.c_str());
        m_bits.yfile_created=1;
    }
    if(yields.attribute("Precision"))
        m_format.precision = yields.attribute("Precision").as_int();
    else m_format.precision = 3;
    if(yields.attribute("Stretch"))
        m_format.stretch = yields.attribute("Stretch").value();
    else m_format.stretch = "1.4";
    cout<<"******************"<<string(output.length(),'*')<<endl;
    cout<<" Writing table to "<<output<<endl;
    cout<<"******************"<<string(output.length(),'*')<<endl;
}

void NPATableWriter::configureSyst(pugi::xml_node syst){
    if(readOptionStr("OutputDir",m_output_dir) != 0) m_output_dir=".";
    // Read output file
    if(!syst){
        cerr<<"ERROR: The yields table cannot be written since no YieldsTable node has been defined."<<endl;
        cerr<<"Please define the YieldsTable node with the information neeeded."<<endl;
        exit(1);
    }
    string output;
    output = m_output_dir+"/"+syst.attribute("Output").as_string("syst_table.tex");
    if(!m_bits.sfile_created){
        m_syst_file.open(output.c_str());
        m_bits.sfile_created=1;
    }
    m_format.precision = syst.attribute("Precision").as_int(3);
    m_format.stretch = syst.attribute("Stretch").as_string("1.4");
    cout<<"******************"<<string(output.length(),'*')<<endl;
    cout<<" Writing table to "<<output<<endl;
    cout<<"******************"<<string(output.length(),'*')<<endl;
}

void NPATableWriter::writeYieldsTable(string hname, string id){
    // Go to the yield table node
    pugi::xml_node yields = m_root.find_child_by_attribute("YieldsTable","ID",id.c_str());
    if(!m_bits.yfile_created) configureYields(yields);
    m_yields_file<<endl<<"---- Yields table for "<<hname<<" ----"<<endl;

    // Check different options
    if(yields.attribute("ShowSignal").as_bool("true")) m_bits.signal = 1;
    if(yields.attribute("ShowData").as_bool("true")) m_bits.data = 1;
    if(yields.attribute("ShowTotalBackground").as_bool("true")) m_bits.total_bck = 1;
    if(yields.attribute("ShowTotalSignal").as_bool()) m_bits.total_signal = 1;
    if(yields.attribute("ShowSignificance").as_bool()) m_bits.significance = 1;
    if(yields.attribute("ShowAgreement").as_bool()) m_bits.agreement = 1;
    if(yields.attribute("ShowSyst").as_bool("true")) m_bits.show_syst = 1;
    if(yields.attribute("SymSyst").as_bool()) m_bits.sym_syst = 1;
    if(yields.attribute("MergeErrors").as_bool()) m_bits.merge_errors = 1;


    // Create all sample collections just one time
    if(!m_bits.samples_defined){
        pugi::xml_node samples_node = yields.child("Samples");
        for (pugi::xml_node node = samples_node.child("Collection");node;node = node.next_sibling("Collection")) {
            NPASampCol* col = new NPASampCol(m_xml_file_name.c_str(),"Tables",node.child_value());
            cout<<"Parsing collection: "<<node.child_value()<<endl;
            col->parse();
            m_samples.push_back(col);
        }
        m_bits.samples_defined=1;
    }
    int columns = m_samples.size()+1;
    int rows = m_samples[0]->getNBackground()+1;
    if(m_bits.signal) rows += m_samples[0]->getNSignal();
    if(m_bits.data) rows += m_samples[0]->getNData();
    if(m_bits.total_bck) rows += 1;
    if(m_bits.total_signal) rows += 1;
    if(m_bits.significance) rows += m_samples[0]->getNSignal();
    if(m_bits.agreement) rows += m_samples[0]->getNData();

    // Fill the table 
    NPATable table(rows, columns);
    table.setStretch(m_format.stretch);
    int bckf=0;
    int totbckf=0;
    int dataf=0;
    int sensf=0;
    int agreef=0;
    for(int r = 0; r < rows; r++){
        for(int c = 0; c < columns; c++){
            if(r==0 && c == 0)
                table.addCell(0,0," ");
            //Fill the first line with the collections names
            else if(r==0 && c > 0)
                table.addCell(r,c,m_samples[c-1]->getName());

            //Fill the first column with the m_samples names
            else{
                int cindex = (c==0) ? c : c-1;
                vector<NPAPlotSample*> signal = m_samples[cindex]->getSignalSamples();
                vector<NPAPlotSample*> background = m_samples[cindex]->getBckSamples();
                vector<NPAPlotSample*> data = m_samples[cindex]->getDataSamples();
                //Signal
                string content;
                if(m_bits.signal && r<=signal.size()){
                    if (c == 0){
                        content = signal[r-1]->getName();
                        bckf++;
                        totbckf++;
                        dataf++;
                        sensf++;
                        agreef++;
                    } else content=getYields(signal[r-1],hname);
                    table.addCell(r,c,content);
                }
                // Background
                if(r>bckf && r-bckf <= background.size()){
                    if(c==0){
                        content = background[r-1-bckf]->getName();
                        dataf++;
                        totbckf++;
                        sensf++;
                        agreef++;
                    } else  content = getYields(background[r-1-bckf],hname);
                    table.addCell(r,c,content);
                }
                // Total bakcground
                if(m_bits.total_bck && r == 1+totbckf){
                    if(c==0){
                        content = "Total bkg.";
                        dataf++;
                        sensf++;
                        agreef++;
                    } else content=getYields(m_samples[cindex]->getTotalBackground(),hname);
                    table.addCell(r,c,content);
                }
                // Data
                if(m_bits.data && r>dataf && r-dataf <= data.size()){
                    if(c == 0){
                        content = data[r-1-dataf]->getName();
                        sensf++;
                        agreef++;
                    } else {
                        bits bit = m_bits;
                        m_bits.show_syst = 0;//Disable systeamtic for data
                        content = getYields(data[r-1-dataf],hname,true);
                        m_bits = bit;
                    }
                    table.addCell(r,c,content);
                }
                // Sensitiviy (sqrt(S)/b)
                if(m_bits.agreement && m_bits.data && r>agreef && r-agreef <= data.size()){
                    if(c==0){
                        content = data[r-agreef-1]->getName()+"/Bkg.";
                        sensf++;
                    }
                    else content = getAgreement(data[r-agreef-1],
                            m_samples[cindex]->getTotalBackground(),
                            hname);
                    table.addCell(r,c,content);
                }
                // Sensitiviy (sqrt(S)/b)
                if(m_bits.significance && r>sensf && r-sensf <= signal.size()){
                    if(c==0) content = signal[r-sensf-1]->getName()+"/$\\sqrt{\\rm{Bkg.}}$";
                    else content = getSensitivity(signal[r-sensf-1],
                            m_samples[cindex]->getTotalBackground(),
                            hname);
                    table.addCell(r,c,content);
                }
            }
        }
    }
    table.write(m_yields_file);
}

void NPATableWriter::writeSystTable(string hname,string id){
    pugi::xml_node syst = m_root.find_child_by_attribute("SystTable","ID",id.c_str());
    if(!m_bits.sfile_created) configureSyst(syst);

    // Check different options
    m_bits.signal = int(syst.attribute("ShowSignal").as_bool("true"));
    m_bits.total_bck = int(syst.attribute("ShowTotalBackground").as_bool("true"));
    m_bits.sym_syst = int(syst.attribute("SymSyst").as_bool());
    m_bits.show_stat = int(syst.attribute("ShowStat").as_bool());

    // Create all sample collections just one time
    if(!m_bits.samples_defined){
        pugi::xml_node samples_node = syst.child("Samples");
        for (pugi::xml_node node = samples_node.child("Collection");node;node = node.next_sibling("Collection")) {
            NPASampCol* col = new NPASampCol(m_xml_file_name.c_str(),"Tables",node.child_value());
            cout<<"Parsing collection: "<<node.child_value()<<endl;
            col->parse();
            m_samples.push_back(col);
        }
        m_bits.samples_defined=1;
    }
    // Read order desired for the systematics
    vector<string> order;
    for (pugi::xml_node node = syst.child("SystOrder").child("Element");node; node = node.next_sibling("Element"))
        order.push_back(node.child_value());
       
    for(int cc = 0; cc < m_samples.size(); cc++){
        cout<<"Doing table for collection "<<m_samples[cc]->getName()<<endl;
        vector<NPAPlotSample*> bck = m_samples[cc]->getBckSamples();
        vector<NPAPlotSample*> signal = m_samples[cc]->getSignalSamples();
        NPAPlotSample *totalbck = m_samples[cc]->getTotalBackground();
        m_syst_file<<endl<<"---- Systematic breakdown ----"<<endl;
        m_syst_file<<"Colection: "<<m_samples[cc]->getName()<<endl;
        m_syst_file<<"Histogram: "<<hname<<endl;

        int columns = bck.size()+1;
        vector<string> snames = m_samples[cc]->getSystNames(order);
        int rows = snames.size()+1;
        if(m_bits.show_stat) rows+=1;
        if(m_bits.signal) columns += signal.size();
        if(m_bits.total_bck) columns += 1;

        // Create and fill table
        NPATable table(rows,columns);
        table.setStretch(m_format.stretch);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (column == 0){ 
                    if (row == 0) table.addCell(0,0,"");
                    else if (row == rows-1 && m_bits.show_stat){
                        table.addCell(row,column,"Statistical");
                    }
                    else table.addCell(row,column,snames[row-1]);
                } else if(column <= bck.size()){
                    if (row == 0) table.addCell(row,column,bck[column-1]->getName());
                    else if (row == rows-1 && m_bits.show_stat){
                        double val = bck[column-1]->getStatError()/bck[column-1]->getYields()*100;
                        stringstream result;
                        result.precision(m_format.precision);
                        result<<fixed;
                        if(m_bits.sym_syst)
                            result<<"$"<<val<<"$";
                        else 
                            result<<"$+"<<val<<"/"<<"-"<<val<<"$";
                        table.addCell(row,column,result.str());
                    }
                    else table.addCell(row,column,getSystVar(bck[column-1],hname,snames[row-1]));
                } else if(column > bck.size() && column <= bck.size()+signal.size()){
                    if (row == 0) table.addCell(row,column,signal[column-bck.size()-1]->getName());
                    else if (row == rows-1 && m_bits.show_stat){
                        double val = signal[column-bck.size()-1]->getStatError()/signal[column-bck.size()-1]->getYields()*100;
                        stringstream result;
                        result.precision(m_format.precision);
                        result<<fixed;
                        if(m_bits.sym_syst)
                            result<<"$"<<val<<"$";
                        else 
                            result<<"$+"<<val<<"/"<<"-"<<val<<"$";
                        table.addCell(row,column,result.str());
                    }
                    else table.addCell(row,column,getSystVar(signal[column-bck.size()-1],hname,snames[row-1]));
                } else {
                    if(row == 0) table.addCell(row,column,totalbck->getName());
                    else if (row == rows-1 && m_bits.show_stat){
                        double val = totalbck->getStatError()/totalbck->getYields()*100;
                        stringstream result;
                        result.precision(m_format.precision);
                        result<<fixed;
                        if(m_bits.sym_syst)
                            result<<"$"<<val<<"$";
                        else 
                            result<<"$+"<<val<<"/"<<"-"<<val<<"$";
                        table.addCell(row,column,result.str());
                    }
                    else table.addCell(row,column,getSystVar(totalbck,hname,snames[row-1]));
                }
            }
        }
        table.write(m_syst_file);
    }



}

string NPATableWriter::getYields(NPAPlotSample* sample, string hname,bool isdata){
    sample->setYieldsSource(hname);
    double yields = sample->getYields();
    double stat = sample->getStatError();
    stringstream result;
    if(isdata) result.precision(0);
    else result.precision(m_format.precision);
    result<<fixed;
    if(!isdata) result<<showpoint;
    result<<"$"<<yields;
    if(isdata && m_bits.dataerror) result<<"\\pm"<<stat;
    if(!isdata){
        if(m_bits.show_syst){
            double up = sample->getUpSystError();
            double down = sample->getDownSystError();
            if(m_bits.sym_syst){
                up = (up > down) ? up : down;
                if(m_bits.merge_errors){
                    double total = sqrt(stat*stat+up*up);
                    result<<"\\pm"<<total;
                } else result<<"\\pm"<<stat<<"\\pm"<<up;
            } else
                if(m_bits.merge_errors){
                    double totalup = sqrt(stat*stat+up*up);
                    double totaldown = sqrt(stat*stat+down*down);
                    result<<"^{+"<<totalup<<"}_{-"<<totaldown<<"}";
                } else result<<"\\pm"<<stat<<"^{+"<<up<<"}_{-"<<down<<"}";
        } else result<<"\\pm"<<stat;
    }
    result<<"$";
    return result.str();
}

string NPATableWriter::getSystVar(NPAPlotSample *s,string hname, string syst){
    s->setYieldsSource(hname);
    double yields = s->getYields();
    double up = s->getSystUpYields(hname,syst,true);
    double down = s->getSystDownYields(hname,syst,true);
    stringstream result;
    result.precision(m_format.precision);
    result<<fixed;

    double edown = 0;
    double eup = 0;
    double udif = up-yields;
    double ddif = down-yields;
    /*cout << "Sample: " << s->getID() << endl;
    cout << "Syst: " << syst << endl;
    cout << "Yields: " << yields << endl;
    cout << "Up:     " << up << endl;
    cout << "Down:   " << down << endl;
    cout << "Udif:   " << up-yields<<endl;
    cout << "Ddif:   " << down-yields<<endl;*/
    if (udif > 0) eup += udif;
    else edown += udif;
    if (ddif > 0) eup += ddif;
    else edown += ddif;

    /*cout << "Eup   : " << eup<<endl;
    cout << "Edown : " << edown<<endl;*/
    double pup = 0 ;
    if (yields != 0) pup = fabs(eup/yields*100);
    double pdown = 0; 
    if (yields != 0) pdown = fabs(edown/yields*100);
    /*cout << "Up perc:   " << pup << endl;
    cout << "Down perc: " << pdown << endl;*/
    if(pup == 0 && pdown == 0) result<<"--";
    else if (pup < 1.0/pow(10,m_format.precision) && pdown < 1.0/pow(10,m_format.precision)) result << "$\\sim 0$";
    else{
        if(m_bits.sym_syst)
            result<<"$"<<((pup > pdown) ? pup : pdown)<<"$";
        else {
            if (pup == 0) result << "--"; 
            else if (pup < 1.0/pow(10,m_format.precision)) result << "$\\sim 0$";
            else result << "+"<<pup;
            result << "/";
            if (pdown == 0) result << "--"; 
            else if (pdown < 1.0/pow(10,m_format.precision)) result << "$\\sim 0$";
            else result << "-" << pdown;
        }
    } 
    return result.str();
}

string NPATableWriter::getSensitivity(NPAPlotSample* signal, NPAPlotSample* bck, string hname){
    signal->setYieldsSource(hname);
    bck->setYieldsSource(hname);
    stringstream result;
    result.precision(m_format.precision);
    result<<fixed;
    double syields = signal->getYields();
    double byields = bck->getYields();
    result<<syields/sqrt(byields);
    return result.str();
}

string NPATableWriter::getAgreement(NPAPlotSample* data, NPAPlotSample* bck, string hname){
    data->setYieldsSource(hname);
    bck->setYieldsSource(hname);
    stringstream result;
    result.precision(m_format.precision);
    result<<fixed;
    double dyields = data->getYields();
    double byields = bck->getYields();
    double sup = bck->getUpSystError();
    double sdo = bck->getDownSystError();
    sup = (sup > sdo) ? sup : sdo;
    double stat = bck->getStatError();
    double toterr = sqrt(stat*stat+sup*sup);
    double err = dyields/byields*sqrt((toterr*toterr)/(byields*byields)+1.0/dyields);
    result<<"$"<<dyields/byields << "\\pm" << err<<"$";
    return result.str();
}
