#include "NPASampCol.h"
#include "NPAXML.h"
#include "NPAPlotSample.h"
#include "NPAInputCol.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include <vector>
#include <iostream>
using namespace std;

NPASampCol::NPASampCol(const char* xml,string root, string samples_name):NPAXML(xml,root){
    m_sample_col_name=samples_name;
    m_total_background = new NPAPlotSample("TOTALBKG","Total Bkg.");
    m_total_background->setType("bck");
}

void NPASampCol::parse(){
    vector<PlotProperties::sample_t> samples;
    samples = getXmlSamples(m_sample_col_name.c_str());
    bool avflws = false;
    bool dosyst = true;
    readOptionBool("AvoidFlows",avflws);
    readOptionBool("DoSyst",dosyst);

    // Check if the input file has been set
    // if not try to find a InputFile node in the
    // xml supplied. If it does not exists end the execution
    // and ask for one.
    if(m_inputs_file.length() < 3){
        pugi::xml_node node = m_root.find_child_by_attribute("Samples","Name",m_sample_col_name.c_str());
        if(node.attribute("InputsFile")) setInputsFile(node.attribute("InputsFile").value());
        else {
            cerr<<"ERROR: You have not defined an xml file to load the inputs for the selected samples."<<endl;
            cerr<<"Please, do it adding the attribute InputsFile to the Samples node or by calling the"<<endl;
            cerr<<"function setInputsFile(std::string inputs) of the NPASampCol object."<<endl;
            exit(1);
        }
    }
    m_input_col = new NPAInputCol(m_inputs_file.c_str());
    m_input_col->doSyst(dosyst);
    // Vector of tootal inputs for the sample collection to save
    // time reading the inputs in the xml.
    m_input_col->filterInputs(getListOfInputs(samples));
    m_input_col->parse();

    // Create all samples objects in the collection
    for (int s = 0; s < samples.size(); ++s)
    {

        PlotProperties::sample_t sample = samples[s];
        NPAPlotSample *sa = new NPAPlotSample(sample.id,sample.name);
        sa->setCollection(this);
        sa->setType(sample.type);
        if(sample.type=="data") m_data_index.push_back(m_sample_col.size());
        if(sample.type=="bck") m_bck_index.push_back(m_sample_col.size());
        if(sample.type=="signal") m_signal_index.push_back(m_sample_col.size());
                cout<<"----------------------------------"<<endl;
                cout<<"Sample ID: "<<sample.id<<" Name: "<<sample.name<<endl;
                cout<<"----------------------------------"<<endl;

        //Loop over all inputs for the sample
        for (int i = 0; i < sample.inputs.size(); ++i)
        {
            NPAPlotInput *in = m_input_col->getInput(sample.inputs[i]);
            if (avflws) in->noFlows();
            // Print info here to be printed after the input parsing info
            if (in == NULL){
                cerr<<"ERROR: The input "<<sample.inputs[i]<<" was added in the sample "<<sample.id<<" but was not defined in "<<m_inputs_file<<"."<<endl;
                cerr<<"Please, define the input before adding it to a sample"<<endl;
                exit(1);
            }
            cout<<"\t*Input: "<<in->getID()<<" ("<<in->getNSyst()<<" syst.)"<<endl;
            cout<<"\t        "<<in->getSource()<<endl;
            sa->addInput(in);
        }
        sa->setHistoProperties(sample.histoprops);
        m_sample_col.push_back(sa);
        vector<string> sss = sa->getSystNames();
        m_syst_names.insert(m_syst_names.end(),sss.begin(),sss.end());
        if(sample.type=="bck") *m_total_background+=*sa;
    }

    pugi::xml_node hfilter = m_root.child("HistoFilter");
    if(hfilter){
        vector<string> avoid;
        vector<string> accept;
        for (pugi::xml_node node = hfilter.child("Accept");node; node = node.next_sibling("Accept")) 
            accept.push_back(node.child_value());   
        for (pugi::xml_node node = hfilter.child("Avoid");node; node = node.next_sibling("Accept")) 
            avoid.push_back(node.child_value());   
        setHistosToAccept(accept);
        setHistosToAvoid(avoid);
    }

    m_actual_sample = m_sample_col.begin();
    //Clean duplicated systematic names
    sort(m_syst_names.begin(),m_syst_names.end());
    m_syst_names.erase(unique(m_syst_names.begin(),
                m_syst_names.end()),
            m_syst_names.end());
    m_checks.parse=1;
}

NPASampCol::~NPASampCol(){
    delete m_input_col;
    for ( auto &x: m_sample_col) delete x;
}

void NPASampCol::setInputsFile(string s){
    m_inputs_file = s;
}

NPAPlotSample* NPASampCol::getSample(string id){
    if(!m_checks.parse) parse();
    vector<NPAPlotSample*>::iterator it = find_if(m_sample_col.begin(),m_sample_col.end(),bind(NPASampCol::sameSampleID,_1,id));
    if (it == m_sample_col.end()){
        cerr<<"ERROR: Sample "<<id<<" does not exists. Request a existing one, nullputr is being returned, be sure to take care of it."<<endl;
        return nullptr;
    }
    return *it;
}

NPAPlotSample* NPASampCol::nextSample(){
    if(!m_checks.parse)parse();

    if(m_actual_sample == m_sample_col.end()) return NULL;
    return *m_actual_sample++;
}

vector<string> NPASampCol::getListOfHistos(){
    if(!m_checks.parse)parse();
    vector<string> orig = m_sample_col[0]->getListOfHistos();
    if(m_h_to_accept.size() != 0  || m_h_to_avoid.size() != 0)
        orig.erase(remove_if(orig.begin(),orig.end(),bind(NPASampCol::cleanHistoVector,_1,m_h_to_avoid,m_h_to_accept)),orig.end());
    return orig;

}

void NPASampCol::plotSampleSystVariations(const char* hname, std::string ID){
    if(!m_checks.parse)parse();
    string outputfile;
    readOptionStr("OutputDir",outputfile);
    getSample(ID)->plotSystVariations(hname,outputfile,m_sample_col_name,m_root.child("ShapeSyst"));
}

void NPASampCol::plotSampleSystVariations(const char* hname, NPAPlotSample* s){
    if(!m_checks.parse)parse();
    string outputfile;
    readOptionStr("OutputDir",outputfile);
    s->plotSystVariations(hname,outputfile,m_sample_col_name,m_root.child("ShapeSyst"));
}

void NPASampCol::plotSystVariations(const char* hname){
    if(!m_checks.parse)parse();
    plotSampleSystVariations(hname,m_total_background);
    resetSampleCounter();
    while(NPAPlotSample* s = nextSample())
        plotSampleSystVariations(hname,s);
}

TH1D* NPASampCol::getHistogram(string hname, TGraphAsymmErrors &graph){
    if(!m_checks.parse)parse();
    pugi::xml_node band = m_root.child("ErrorBand");
    TH1D* h = m_total_background->getHistogram(hname.c_str(),graph);
    if (h == NULL)
        return getDummyHistogram(hname);
    if(band.attribute("Symmetrize").as_bool()){
        // Symetrize error band of systematic
        int np = graph.GetN();
        for (int i = 0; i < np; i++) {
            double max = (graph.GetErrorYhigh(i) > graph.GetErrorYlow(i)) ? graph.GetErrorYhigh(i) : graph.GetErrorYlow(i);
            graph.SetPointEYhigh(i,max);
            graph.SetPointEYlow(i,max);
        }
    }
    return h;

    /*resetSampleCounter();
      TH1D* h = NULL;
      while(NPAPlotSample* s = nextSample()){
      if(h == NULL)
      h = s->getHistogram(hname.c_str(),graph);
      else{
      TGraphAsymmErrors err;
      h->Add(s->getHistogram(hname.c_str(),err));
      NPAPlotSample::addErrors(graph,err);
      }
      }
      return h;*/
}

TH1D* NPASampCol::getHistogram(string hname){
    if(!m_checks.parse)parse();
    TH1D* h = m_total_background->getHistogram(hname.c_str());
    if (h == NULL)
        return getDummyHistogram(hname);
    else return h;
    /*resetSampleCounter();
      TH1D* h = NULL;
      while(NPAPlotSample* s = nextSample()){
      if(h == NULL)
      h = s->getHistogram(hname.c_str());
      else{
      h->Add(s->getHistogram(hname.c_str()));
      }
      }
      return h;*/
}

TH1D* NPASampCol::getDummyHistogram(string hname){
    if(!m_checks.parse)parse();
    // Loop over all samples in the collection and see if an histogram like the one requested
    // is present and return a copy of it but empty.
    for(unsigned int ii = 0; ii < m_sample_col.size(); ii++){
        TH1D* hh = m_sample_col[ii]->getHistogram(hname);
        if (hh != NULL){
            TH1D* rh = (TH1D*)hh->Clone((hname+"_empty").c_str());
            rh->Reset("ICES");
            return rh;
        }
    }
    // If no histogram has been returned return NULL
    cerr<<"ERROR: The histogram requested is also not present in any sample of the collection. Make sure the histogram exists."<<endl;
    return NULL;
}

vector<string> NPASampCol::getListOfInputs(vector<PlotProperties::sample_t> samples){
    vector<string> inputs;
    for(int s = 0; s < samples.size();s++){
        vector<string> tmp = samples[s].inputs;
        inputs.insert(inputs.end(),tmp.begin(), tmp.end());
    }
    // Remove duplicated
    sort(inputs.begin(),inputs.end());
    inputs.erase(unique(inputs.begin(),inputs.end()),inputs.end());
    return inputs;
}

vector<NPAPlotSample*> NPASampCol::getDataSamples(){
    if(!m_checks.parse)parse();
    vector<NPAPlotSample*> tmp;
    for (int i = 0; i < m_data_index.size(); i++)
        tmp.push_back(m_sample_col[m_data_index[i]]);
    return tmp;
}

vector<NPAPlotSample*> NPASampCol::getBckSamples(){
    if(!m_checks.parse)parse();
    vector<NPAPlotSample*> tmp;
    for (int i = 0; i < m_bck_index.size(); i++)
        tmp.push_back(m_sample_col[m_bck_index[i]]);
    return tmp;
}

vector<NPAPlotSample*> NPASampCol::getSignalSamples(){
    if(!m_checks.parse)parse();
    vector<NPAPlotSample*> tmp;
    for (int i = 0; i < m_signal_index.size(); i++)
        tmp.push_back(m_sample_col[m_signal_index[i]]);
    return tmp;
}

string NPASampCol::getPlotOptions(string node){
    if(!m_checks.parse)parse();
    pugi::xml_node cnode = m_root.child(node.c_str());
    if(!cnode){
        cerr<<"ERROR: It seems that there is no ShapeSyst node in "<<m_xml_file_name<<endl;
        cerr<<"Before trying to get the pltos for systematics variations please define this node";
        cerr<<" with the options which match your needs"<<endl;
        exit(1);
    }
    bool norm = cnode.attribute("Normalise").as_bool(true);
    bool bbbin = cnode.attribute("BinByBin").as_bool();;
    bool log = cnode.attribute("Logy").as_bool();
    bool staterror = cnode.attribute("ShowStatNom").as_bool(true);
    string opt = (norm) ? "normalise" : "";
    opt += (bbbin) ? "bbbin" : "";
    opt += (log) ? "log" : "";
    opt += (staterror) ? "nstat" : "";
    cout<<"---> Syst variations options: "<<opt<<endl;
    return opt;
}

vector<string> NPASampCol::getSystNames(vector<string> base){
    if(!m_checks.parse)parse();
    if(base.size() > 0)
        sort(m_syst_names.begin(),m_syst_names.end(),bind(NPASampCol::sortSystNames,_1,_2,base));     
    return m_syst_names;
}
