/*/
The NPADrawable class is designed
to be an interface for all different
objetcts that can be used and generated
when drawing plots after the analysis
has run.
*/
#ifndef NPA_DRAWABLE_H
#define NPA_DRAWABLE_H
struct Coord
{
	double x1;
	double y1;
	double x2;
	double y2;
};

enum Mods {CHANGE_POS};
class NPADrawable
{
public:
	NPADrawable(const char* id);
	NPADrawable(const char* id,double x, double y);
	NPADrawable(const char* id,double x1,double x2, double y1,double y2);
	virtual ~NPADrawable();
	
	// General functions
	virtual void draw(const char* opt)=0;//!< Main function to draw the objetc. Usually it suppor options.
	const char* getID(){return m_id;};//!< Returns the ID of the Drawable

	// Different functions to position the drawable 
	// based on the position of a given drawable
	void alignLeft(const NPADrawable *draw, double dist=0);
	void alignRight(const NPADrawable *draw, double dist=0);
	void alignTop(const NPADrawable *draw, double dist=0);
	void alignBottom(const NPADrawable *draw, double dist=0);
	virtual void update(Mods m)=0;//!< Update anything needed in the Drawable after any modification is done.

	// Set position
	void put(double x, double y){m_coord.x1 = x; m_coord.y1 = y; m_coord.x2=x, m_coord.y2=y;};
	void put(double x1, double y1, double x2, double y2 ){m_coord.x1 = x1; m_coord.y1 = y1;
		m_coord.x2 = x2; m_coord.y2  =y2;};

	// Get spatial information about Drawable
	double getXPos() const {return m_coord.x1;};//!< Returns the x position of the drawable.
	double getYPos() const {return m_coord.y1;};//!< Returns the y position of the drawable.
	virtual double getLength() const ;//!< Get the lenght of the drawable.
	virtual double getHight() const;//!< Get the hight of the drawable.

protected:
	Coord getCoord(){return m_coord;};
	double getX2(){return m_coord.x2;};
	double getX1(){return m_coord.x1;};
	double getY2(){return m_coord.y2;};
	double getY1(){return m_coord.y1;};
	const char* m_id;
	Coord m_coord;

};
#endif
