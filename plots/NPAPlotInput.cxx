#include <sys/stat.h>
#include "NPAPlotInput.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TKey.h"
#include "TROOT.h"
#include "TClass.h"
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>

using namespace std;

NPAPlotInput::NPAPlotInput(std::string ID, std::string rootfile){

    m_histos_names_filled = false;
    m_syst_ids_cleaned = false;
	m_file = TFile::Open(rootfile.c_str());
	m_ID = ID;
	p_include_flows = true;
	m_type = NOINPUT;
    m_base_histo_counter = 0;

	if(m_file->IsZombie()){
		cerr<<"ERROR: The file "<<rootfile<<" does not exists or it is broken."<<endl;
		exit(1);
	}
	strToType();
}

/*NPAPlotInput::NPAPlotInput(const NPAPlotInput& other) : NPAYields(other),
              m_strtotype(other.m_strtotype),
              m_ID(other.m_ID),
              m_syst_up_type(other.m_syst_up_type),
              m_syst_down_type(other.m_syst_down_type),
              m_syst_ids(other.m_syst_ids),
              m_histos_names(other.m_histos_names),
              m_scale_vars(other.m_scale_vars),
              p_include_flows(other.p_include_flows),
              m_histos_names_filled(other.m_histos_names_filled),
              m_syst_ids_cleaned(other.m_syst_ids_cleaned),
              m_type(other.m_type){
    // Copy pointers content
    map<string,const void*>::iterator it = other.m_syst_up_list.begin();
    for(;it!=other.m_syst_up_list.end();++it)
        *m_syst_up_list[(*it).first] = *(*it).second.
    map<string,const void*>::iterator it = other.m_syst_down_list.begin();
    for(;it!=other.m_syst_down_list.end();++it)
        *m_syst_down_list[(*it).first] = *(*it).second.
    m_file = new TFile();
    *m_file = *other.m_file;
}*/

NPAPlotInput::~NPAPlotInput(){
    cout << "Cleaning input " << m_ID << endl; 
    map<string,sys_type>::iterator itu = m_syst_up_type.begin();
    map<string,sys_type>::iterator itd = m_syst_down_type.begin();
    for(;itu != m_syst_up_type.end(); itu++){
        if((*itu).second == FILE){
            TFile *f = (TFile*) m_syst_up_list[(*itu).first];
            if (!f->IsZombie())
            f->Close();
            delete f;
        }
        if((*itu).second == SCALE){
            double *a = (double*) m_syst_up_list[(*itu).first];
            delete a;
        }
        if((*itu).second == HISTO){
            TH1D *h = (TH1D*) m_syst_up_list[(*itu).first];
            if(h) delete h;
        }
    }
    for(;itd != m_syst_down_type.end(); itd++){
        if((*itd).second == FILE){
            TFile *f = (TFile*) m_syst_down_list[(*itd).first];
            if (!f->IsZombie())
            f->Close();
            delete f;
        }
        if((*itd).second == SCALE){
            double *a = (double*) m_syst_down_list[(*itd).first];
            delete a;
        }
        if((*itd).second == HISTO){
            TH1D *h = (TH1D*) m_syst_down_list[(*itd).first];
            if(h) delete h;
        }
    }
    if (m_file) delete m_file;
}

/**
  @brief Fill the variables m_strtotype with all possibles types that the users might
  use to define a systematic variation so he can acces it by a string.
  */
void NPAPlotInput::strToType(){
    m_strtotype["file"]=FILE;
    m_strtotype["scale"]=SCALE;
    m_strtotype["histo"]=HISTO;
}

bool NPAPlotInput::checkSysType(string type){
    return m_strtotype.find(type) == m_strtotype.end();
}

/**
  @brief Add a systematic variation to this input.
  @param input Input for the systematic. What it is depends
  on the parameter type.
  @param type -file: The systematic is a new root file with same structure
  than the nominal one
  -scale: The systematic is a scale variation in which the
  noimnale histogram is scaled.
  -histo:	Teh systematic is a given histogram inside the same 
  root file than the noimnal case.*/
void NPAPlotInput::addUpSyst(const std::string ID, const std::string name, const std::string input, const std::string stype,const std::string ud){
    // Add the systematic name
    m_syst_ids.push_back(ID);
    m_syst_names.push_back(name);

    if(ud == "up"){
        if(m_syst_up_list.find(ID) != m_syst_up_list.end()){
            cerr<<"ERROR: Sorry, the ID "<<ID<<" given to a systematic has already been used and cannot be reasigned."<<endl;
            exit(1);
        }
    } else {
        if(m_syst_down_list.find(ID) != m_syst_down_list.end()){
            cerr<<"ERROR: Sorry, the ID "<<ID<<" given to a systematic has already been used and cannot be reasigned."<<endl;
            exit(1);
        }
    }

    //Check validity of the systematic type
    if(checkSysType(stype)){
        cerr<<"ERROR: Sorry, the systematic type "<<stype<<" is not valid."<<endl;
        exit(1);
    }

    sys_type type = m_strtotype[stype];
    struct stat buffer;   
    switch (type){
        case FILE:
            {
                string fname = input;
                if (stat(input.c_str(), &buffer) != 0){
                  cerr << "ERROR: The systematic file " << input << " for input " <<m_ID<<" does not exists. Using nominal instead (PLEASE CHECK!)" << endl;
                  fname = m_file->GetName();
                } 

                if(ud == "up"){
                    m_syst_up_type[ID] = FILE;
                    m_syst_up_list[ID] = TFile::Open(fname.c_str());
                } else if(ud == "down"){
                    m_syst_down_type[ID] = FILE;
                    m_syst_down_list[ID] = TFile::Open(fname.c_str());
                } else if (ud == "none"){
                    m_syst_down_type[ID] = FILE;
                    m_syst_up_type[ID] = FILE;
                    m_syst_down_list[ID] = TFile::Open(m_file->GetName());
                    m_syst_up_list[ID] = TFile::Open(fname.c_str());
                }
                break;
            }
        case HISTO:
            {
                if(m_file->FindKey(input.c_str()) == 0){
                    cerr<<"ERROR: The histogram "<<input
                        <<" does not exists in the file "<<m_file->GetName()<<"."<<endl;
                    exit(1);
                }
                if(ud == "up"){
                    m_syst_up_type[ID] = HISTO;
                    m_syst_up_list[ID] = (TH1D*)m_file->Get(input.c_str());		
                } else if(ud == "down"){
                    m_syst_down_type[ID] = HISTO;
                    m_syst_down_list[ID] = (TH1D*)m_file->Get(input.c_str());		
                } else if (ud == "none"){
                    //TODO: This need to be improved, right now it is not working
                    m_syst_down_type[ID] = HISTO;
                    m_syst_up_type[ID] = HISTO;
                    m_syst_down_list[ID] = (TH1D*)m_file->Get(input.c_str());//!!!! This cannot be like that!
                    m_syst_up_list[ID] = (TH1D*)m_file->Get(input.c_str());
                }
                break;
            }
        case SCALE:
            {
                if(ud == "up"){
                    m_syst_up_type[ID] = SCALE;
                    m_scale_vars.push_back(atof(input.c_str()));
                    m_syst_up_list[ID] = &(m_scale_vars[m_scale_vars.size()-1]);
                } else {
                    m_syst_down_type[ID] = SCALE;
                    m_scale_vars.push_back(atof(input.c_str()));
                    m_syst_down_list[ID] = &(m_scale_vars[m_scale_vars.size()-1]);
                }
                break;
            }
    }
}

/**
  @brief Returns the histogram specified in hname and store in graph
  a TGraphAsymmErrors which has the error bars associted
  with all systematic varition of the input.
  @param hname Histogram name to be retrieved
  @param graph Histogram to be used to store the errors.
  */
TH1D* NPAPlotInput::getHistogram(string hname, TGraphAsymmErrors &graph){
    TH1D* histo = getHistogram(hname.c_str());
    if (histo == NULL)
        return histo;
    string newname = hname;
    newname += "_new";
    int nBins = histo->GetNbinsX();

    double errup[nBins];		
    double errdo[nBins];
    double xval[nBins];
    double yval[nBins];
    double xwidth[nBins];

    // Put the statistic error in the error badns
    for (int b = 0; b < nBins; ++b)
    {
        errup[b] = histo->GetBinError(b+1)*histo->GetBinError(b+1);
        errdo[b] = histo->GetBinError(b+1)*histo->GetBinError(b+1);
        xval[b] = histo->GetBinCenter(b+1);
        yval[b] = histo->GetBinContent(b+1);
        xwidth[b] = (histo->GetBinLowEdge(b+2)-histo->GetBinLowEdge(b+1))/2.;
    }

    //Loop over systematics to get the error bands
    map<string,const void*>::iterator it = m_syst_up_list.begin();
    for (; it != m_syst_up_list.end(); ++it)
    {
        string id = it->first;
        TH1D* hup(nullptr);
        TH1D* hdo(nullptr);
        //Identify type of systematic and load up and down variations
        // of the histogram.
        if(m_syst_up_type[id]==FILE) {
            ((TFile*)m_syst_up_list[id])->GetObject(hname.c_str(),hup);
            ((TFile*)m_syst_down_list[id])->GetObject(hname.c_str(),hdo);
        }
        if(m_syst_up_type[id] == HISTO){
            hup = (TH1D*)((TH1D*)m_syst_up_list[id])->Clone(newname.c_str());
            hdo = (TH1D*)((TH1D*)m_syst_down_list[id])->Clone(newname.c_str());
        }
        if(m_syst_up_type[id] == SCALE){
            double* sup = (double*)m_syst_up_list[id];
            double* sdo = (double*)m_syst_down_list[id];
            stringstream nup, ndo;
            hup = (TH1D*) histo->Clone(nup.str().c_str());
            hup->Scale(1+*sup/100.);
            hdo = (TH1D*) histo->Clone(ndo.str().c_str());			
            hdo->Scale(1-*sdo/100.);
        }
        if(hup == nullptr || hdo == nullptr){
            cerr << "WARNING: The histogram " << hname << " has not been found in: "<<endl;
            cerr << "         Input:      " << m_ID <<endl;
            cerr << "         Systematic: " << id <<endl;
            cerr << "This systematic won't be taken into acount for this input " << endl;
            if (hup) delete hup;
            if (hdo) delete hdo;
            continue;
        }
        hup->SetDirectory(0);
        hdo->SetDirectory(0);
        includeFlows(*hup);
        includeFlows(*hdo);

        // Derive variations
        double u,d,n,diff;
        for (int b = 0; b < nBins; ++b)
        {
            u = hup->GetBinContent(b+1);
            d = hdo->GetBinContent(b+1);
            n = histo->GetBinContent(b+1);
            diff = u-n;
            if (diff>=0) errup[b]+=diff*diff;
            else errdo[b]+=diff*diff;
            diff = d-n;
            if (diff<0) errdo[b]+=diff*diff;
            else errup[b]+=diff*diff;
        }
        if(hup) delete hup;
        if(hdo) delete hdo;
    }

    //Create the graph with the error bands
    for (int b = 0; b < nBins; ++b)
    {
        graph.SetPoint(b,xval[b],yval[b]);
        graph.SetPointError(b,xwidth[b],xwidth[b],sqrt(errdo[b]),sqrt(errup[b]));
    }

    return histo;
}

/**
  @brief Returns the histogram specified in hname
  @param hname Histogram name to be retrieved
  */
TH1D* NPAPlotInput::getHistogram(string hname){
    // Check if it exists
    if(!m_file->GetListOfKeys()->Contains(hname.c_str())){
        cerr<<"Warning <NPAPLotInput::getHistogram>: Histogram "<<hname<<" not found in input "<<m_ID<<endl;
        return NULL;
    }
    TH1D* histo;
    m_file->GetObject(hname.c_str(),histo);
    if (histo) histo->SetDirectory(0);
    //TH1D* histo = (TH1D*)m_file->Get(hname);
    /*cout<<"Before flow bins: "<<histo->GetNbinsX()<<endl;
      cout<<"Before low: "<<histo->GetBinLowEdge(1)<<endl;
      cout<<"Before high: "<<histo->GetBinLowEdge(histo->GetNbinsX()+1)<<endl;*/
    includeFlows(*histo);
    /*cout<<"After flow bins: "<<histo->GetNbinsX()<<endl;
      cout<<"After low: "<<histo->GetBinLowEdge(1)<<endl;
      cout<<"After high: "<<histo->GetBinLowEdge(histo->GetNbinsX()+1)<<endl;*/
    return histo;
}

/**
brief: Add systematic variations (difference with nominal) to the
histograms up and down. These histogram are suposed to contain at the 
end the total variation from a given systematic for all the inputs
in a sample.
param: syst Systematic to be added.
param: hname Name of the histogram to use
param: up Histogram containing the up variations.
param: down Histogram containing the down variations.
*/
int NPAPlotInput::addSystVariations(string syst, const char* hname, TH1D *up, TH1D *down){
    int vars = (isOneSided(syst)) ? 1 : 2;// Tells the number of variations, in the case of only

    // Check if the systematic exist for this sample and if not don't add anything
    if (m_syst_up_list.find(syst) == m_syst_up_list.end()) return vars;

    TH1D *sysup;
    TH1D *sysdo;
    // one (no up and dow) it is 2.
    if(m_syst_up_type[syst] == FILE){
        ((TFile*) m_syst_up_list[syst])->GetObject(hname,sysup);
        ((TFile*) m_syst_down_list[syst])->GetObject(hname,sysdo);
        includeFlows(*sysup);
        includeFlows(*sysdo);

        // Substract nominal
        TH1D *nom;
        m_file->GetObject(hname,nom);
        includeFlows(*nom);
        sysup->Add(nom,-1);
        sysdo->Add(nom,-1);
    }

    // Add variations
    up->Add(sysup);
    down->Add(sysdo);
    delete sysup;
    delete sysdo;
    return vars;
}

/**
brief: Add the distributions of a given systematic of this input. 
param: syst Systematic to be added.
param: hname Name of the histogram to use
param: up Histogram containing the up variations.
param: down Histogram containing the down variations.
*/
int NPAPlotInput::addSyst(string syst, const char* hname, TH1D* up, TH1D* down){
    TH1D *sysup;
    TH1D *sysdo;
    int vars = (isOneSided(syst)) ? 1 : 2;// Tells the number of variations, in the case of only

    // one (no up and dow) it is 2.
    sysup = getSystUpHistogram(hname,syst);
    sysdo = getSystDownHistogram(hname,syst);
    includeFlows(*sysup);
    includeFlows(*sysdo);

    // Add variations
    if(up == nullptr){
    up = sysup;
    down = sysdo;
    } else {
    up->Add(sysup);
    down->Add(sysdo);
    }
    delete sysup;
    delete sysdo;
    return vars;
}

/**
 * @brief: Get the list of all histograms in the sample.
 */
vector<string> NPAPlotInput::getListOfHistos(){
    if(!m_histos_names_filled){
        TIter next(m_file->GetListOfKeys());
        TKey *key;
        const char *className;
        while ((key = (TKey*)next())) {
            className = key->GetClassName();
            TClass *cl = gROOT->GetClass(className);
            if (!cl) continue;
            if (!cl->InheritsFrom("TH1") || cl->InheritsFrom("TH2")) continue;
            TH1* h0 = (TH1*)key->ReadObj();
            string histoname = h0->GetName();
            m_histos_names.push_back(histoname);
        }
    }
    m_histos_names_filled = true;
    return m_histos_names;
}

/**
 * @brief: Remove duplicated systematics names before using them
 */
void NPAPlotInput::removeDuplicatedSysNames(){
    if(!m_syst_ids_cleaned){
        sort(m_syst_ids.begin(),m_syst_ids.end());
        m_syst_ids.erase(unique(m_syst_ids.begin(),
                    m_syst_ids.end()),
                m_syst_ids.end());
        sort(m_syst_names.begin(),m_syst_names.end());
        m_syst_names.erase(unique(m_syst_names.begin(),
                    m_syst_names.end()),
                m_syst_names.end());
        m_syst_ids_cleaned=true;
    }
}

/**
 * @brief This function defines the yields needed for NPAYields to derived yields and errors
 */
void NPAPlotInput::defineYields(){
    // Define the yields itself and the error
    if(!m_file->FindKey(m_yields_source.c_str())){
        cerr<<"ERROR: The yield source "<<m_yields_source<<" doens't exist. Please, select a valid one."<<endl;
        exit(1);
    }
    TH1D* histo = (TH1D*)m_file->Get(m_yields_source.c_str());
    double err;
    m_yields = histo->IntegralAndError(0,histo->GetNbinsX(),err);
    m_stat_errors.push_back(err);
    // Define systematics errors up and down.
    map<string,const void*>::iterator it = m_syst_up_list.begin();
    double upyields, doyields;
    for (; it != m_syst_up_list.end(); ++it) {
        string id = it->first;
        if(m_syst_up_type[id]==FILE) {
            TFile* fu = (TFile*)m_syst_up_list[id];
            TFile* fd = (TFile*)m_syst_down_list[id];
            TH1D* hup = (TH1D*) fu->Get(m_yields_source.c_str());
            TH1D* hdo = (TH1D*) fd->Get(m_yields_source.c_str());
            upyields = hup->Integral();
            doyields = hdo->Integral();
        }
        if(m_syst_up_type[id] == HISTO){
            TH1D* hup = (TH1D*)m_syst_up_list[id];
            TH1D* hdo = (TH1D*)m_syst_down_list[id];
            upyields = hup->Integral();
            doyields = hdo->Integral();
        }
        if(m_syst_up_type[id] == SCALE){
            double* sup = (double*)m_syst_up_list[id];
            double* sdo = (double*)m_syst_down_list[id];
            upyields = m_yields*(*sup);
            doyields = m_yields*(*sdo);
        }
        if(upyields - m_yields > 0) m_up_syst_errors.push_back(upyields-m_yields);
        else m_down_syst_errors.push_back(upyields-m_yields);
        if(doyields - m_yields > 0) m_up_syst_errors.push_back(doyields-m_yields);
        else m_down_syst_errors.push_back(doyields-m_yields);
    }
}

/**
 * @brief Inlude the underflow and upper flow of the histogram as content of the last and first bin
 * @param histo Histogram to use
 */
void NPAPlotInput::includeFlows(TH1D& h){
    if(!p_include_flows) return;
    //Include upperflow
    double lbc = h.GetBinContent(h.GetNbinsX());
    double llbc = h.GetBinContent(h.GetNbinsX()+1);
    double fbc = h.GetBinContent(1);
    double ffbc = h.GetBinContent(0);
    h.SetBinContent(h.GetNbinsX(),lbc+llbc);
    h.SetBinContent(1,fbc+ffbc);
    lbc = h.GetBinError(h.GetNbinsX());
    llbc = h.GetBinError(h.GetNbinsX()+1);
    fbc = h.GetBinError(1);
    ffbc = h.GetBinError(0);
    h.SetBinError(h.GetNbinsX(),sqrt(lbc*lbc+llbc*llbc));
    h.SetBinError(1,sqrt(fbc*fbc+ffbc*ffbc));
}

/**
 * brief: Return the number of events in the histogram hname for the systematic variation syst.
 * If nominal is set to true, in case that syst is not present in the input the nominal yields will
 * be returned.
 * param: hname Name of the histogram used to get the yields.
 * param: syst Systematic ID of the systematic wanted.
 * param: nominal If set to true and the systematic is not found in the input the number of events
 *                  in the nominal case will be returned. Otherwise 0 will be returned.
 */
double NPAPlotInput::getSystUpYields(string hname, string syst, bool nominal){
    map<string,const void*>::iterator it = m_syst_up_list.find(syst);
    if (it == m_syst_up_list.end()){
        if(nominal){
            return ((TH1D*) m_file->Get(hname.c_str()))->Integral();
        }else return 0;
    }
    if(m_syst_up_type[syst] == FILE){
        const TH1D *h;
        ((TFile*)m_syst_up_list[syst])->GetObject(hname.c_str(),h);
        return h->Integral();
    }
    if(m_syst_up_type[syst] == SCALE){
        double *d = (double*)m_syst_up_list[syst];
        double scale = 1 + *d/100;
        const TH1D* h = (TH1D*) m_file->Get(hname.c_str());
        return h->Integral()*scale;
    }
}

/**
 * brief: Same as getSystUpYields but for the down variation of the systematic
 * param: hname Name of the histogram used to get the yields.
 * param: syst Systematic ID of the systematic wanted.
 * param: nominal If set to true and the systematic is not found in the input the number of events
 *                  in the nominal case will be returned. Otherwise 0 will be returned.
 */
double NPAPlotInput::getSystDownYields(string hname, string syst, bool nominal){
    map<string,const void*>::iterator it = m_syst_down_list.find(syst);
    if (it == m_syst_down_list.end()){
        if(nominal){
            const TH1D *h = (TH1D*) m_file->Get(hname.c_str());
            return h->Integral();
        }else return 0;
    }
    if(m_syst_down_type[syst] == FILE){
        TFile *f = (TFile*)m_syst_down_list[syst];
        const TH1D *h = (TH1D*) f->Get(hname.c_str());
        return h->Integral();
    }
    if(m_syst_down_type[syst] == SCALE){
        double *d = (double*)m_syst_down_list[syst];
        double scale = 1 + *d/100;
        const TH1D* h = (TH1D*) m_file->Get(hname.c_str());
        return h->Integral()*scale;
    }
}

/**
 * brief: Return a vector with the up and down uncertainty of a given systematic
 * param: hname Name of the histogram used to get the yields.
 * param: syst Systematic ID of the systematic wanted.
 *                  in the nominal case will be returned. Otherwise 0 will be returned.
 */
vector<double> NPAPlotInput::getSystError(string hname, string syst){
    map<string,const void*>::iterator it = m_syst_up_list.find(syst);
    if (it == m_syst_up_list.end()){
        vector<double> v = {0,0};
        return v;
    }
    // Get the nominal yields
    TH1D* nomh;
    ((TFile*)m_file)->GetObject(hname.c_str(),nomh);
    double nom_yields = nomh->Integral();
    double uerr = 0;
    double derr = 0;
    double uyi = getSystUpYields(hname,syst,false);
    double dyi = getSystDownYields(hname,syst,false);
    double tmp = uyi-nom_yields;
    /*cout << "Error for input " << m_ID << endl;
    cout << "     Yields:    "<<nom_yields<<endl;
    cout << "     Up yields: "<<uyi<<endl;
    cout << "     Do yields: "<<dyi<<endl;
    cout << "     Up error:  "<<tmp<<endl;*/
    if (tmp > 0) uerr += tmp*tmp;
    else derr += tmp*tmp;
    tmp = dyi-nom_yields;
    //cout << "     Do error:  "<<tmp<<endl;
    if (tmp > 0) uerr += tmp*tmp;
    else derr += tmp*tmp;
    /*cout << "     Final up error:   "<<sqrt(uerr)<<endl;
    cout << "     Final down error: "<<sqrt(derr)<<endl;
    cout << "     Final percentage up:   " << sqrt(uerr)/getYields()*100 << endl;
    cout << "     Final percentage down: " << sqrt(derr)/getYields()*100 << endl;*/
    vector<double> v = {sqrt(uerr),sqrt(derr)};
    return v;
}

/**
 * brief: Returns the histogram corresponding to the given down systematic variation specified by the syst parameters.
 * param: hname Name of the histogram used to get the yields.
 * param: syst Systematic ID of the systematic wanted.
 * param: nominal If set to true and the systematic is not found in the input the number of events
 *                  in the nominal case will be returned. Otherwise 0 will be returned.
 */
TH1D* NPAPlotInput::getSystDownHistogram(string hname, string syst, bool nominal){
    map<string,const void*>::iterator it = m_syst_down_list.find(syst);
    if (it == m_syst_down_list.end()){
        if(nominal){
            TH1D* h;
            m_file->GetObject(hname.c_str(),h);
            return h;
        }else return nullptr;
    }
    if(m_syst_down_type[syst] == FILE){
        TH1D* h;
        ((TFile*)m_syst_down_list[syst])->GetObject(hname.c_str(),h);
        return h;
    }
    if(m_syst_down_type[syst] == SCALE){
        double *d = (double*)m_syst_down_list[syst];
        double scale = 1 + *d/100;
        TH1D* h;
        m_file->GetObject(hname.c_str(),h);
        h->Scale(scale);
        return h;
    }
}

/**
 * brief: Returns the histogram corresponding to the given up systematic variation specified by the syst parameters.
 * param: hname Name of the histogram used to get the yields.
 * param: syst Systematic ID of the systematic wanted.
 * param: nominal If set to true and the systematic is not found in the input the number of events
 *                  in the nominal case will be returned. Otherwise 0 will be returned.
 */
TH1D* NPAPlotInput::getSystUpHistogram(string hname, string syst, bool nominal){
    map<string,const void*>::iterator it = m_syst_up_list.find(syst);
    if (it == m_syst_up_list.end()){
        if(nominal){
            TH1D* h;
            m_file->GetObject(hname.c_str(),h);
        }else return nullptr;
    }
    if(m_syst_up_type[syst] == FILE){
        TH1D* h;
        ((TFile*)m_syst_up_list[syst])->GetObject(hname.c_str(),h);
        return h;
    }
    if(m_syst_up_type[syst] == SCALE){
        double *d = (double*)m_syst_up_list[syst];
        double scale = 1 + *d/100;
        TH1D* h;
        m_file->GetObject(hname.c_str(),h);
        h->Scale(scale);
        return h;
    }
}
