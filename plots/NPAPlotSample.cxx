#include "NPAPlotSample.h"
#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <sstream>
#include <algorithm>
#include "TH1D.h"
#include "TGraphAsymmErrors.h"
#include "NPAPlotInput.h"
#include "NPASampCol.h"
#include "NPAXML.h"
#include "NPAColor.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"

using namespace std;
using namespace pugi;


NPAPlotSample::NPAPlotSample(string ID,string name) : NPAYields() , m_ID(ID), m_name(name){
    m_sys_names_cleaned = false;
    m_collection = NULL;
}

NPAPlotSample::NPAPlotSample(const NPAPlotSample& other): NPAYields(other){
    // Make a copy of the content of the pointers
    vector<NPAPlotInput*> inputs = other.getInputs();
    for(int i = 0; i < inputs.size(); i++){
        NPAPlotInput *in;
        *in = *inputs[i];
        m_inputs.push_back(in);
    }
    m_ID = other.getID();
    m_name = other.getName();
    m_type = other.getType();
    m_sys_names = getSystNames();
}

NPAPlotSample::~NPAPlotSample(){

}

TH1D* NPAPlotSample::getHistogram(string name, TGraphAsymmErrors &graph){
    // Check the size of the input vector
    if(m_inputs.size() == 0){
        cerr<<"WARNING: You have defined a sample ("<<m_name<<") but you have not specified any valid input."<<endl;
        cerr<<"Check the configuration file and run again"<<endl;
        exit(1);
    }

    TH1D* h = NULL;
    for (int i = 0; i < m_inputs.size(); ++i)
    {   
        TGraphAsymmErrors err;
        if (i == 0) {
            h = m_inputs[i]->getHistogram(name.c_str(),err);
            graph = err;
        }
        else{
            TH1D* hh = m_inputs[i]->getHistogram(name.c_str(),err);
            h->Add(hh);
            addErrors(graph,err);
            delete hh;
        }
    }
    if (h == NULL){
        if (m_collection == NULL){
            cerr<<"WARNING: The histogram requested is not present in any input and the sample does not belong to a collection."<<endl;
            cerr<<"       NULL histogram will be returned."<<endl;
            return h;
        } else return m_collection->getDummyHistogram(name);
    }
    applyHistoProperties(h);
    return h;
}

TH1D* NPAPlotSample::getHistogram(string name){
    // Check the size of the input vector
    if(m_inputs.size() == 0){
        cerr<<"ERROR: You have defined a sample but you have not specified any valid input."<<endl;
        cerr<<"Check the configuration file and run again"<<endl;
        exit(1);
    }

    TH1D* h = NULL;
    for (int i = 0; i < m_inputs.size(); ++i)
    {   
        if (i == 0) {
            h = m_inputs[i]->getHistogram(name.c_str());
        }
        else{
            TH1D* hh = m_inputs[i]->getHistogram(name.c_str());
            h->Add(hh);
            delete hh;
        }
    }
    if (h == NULL){
        if (m_collection == NULL){
            cerr<<"ERROR: The histogram requested is not present in any input and the sample does not belong to a collection."<<endl;
            cerr<<"       NULL histogram will be returned."<<endl;
            return h;
        } else return m_collection->getDummyHistogram(name);
    }
    applyHistoProperties(h);
    return h;
}

void NPAPlotSample::applyHistoProperties(TH1* h){
    // Apply fill color
    map<string,string>::iterator it = m_histoprop.find("FillColor");
    if (it != m_histoprop.end()) h->SetFillColor(NPAColor::parseColor(it->second));
    // Apply Line color
    it = m_histoprop.find("LineColor");
    if (it != m_histoprop.end()) h->SetLineColor(NPAColor::parseColor(it->second));
    // Apply line style
    it = m_histoprop.find("LineStyle");
    if (it != m_histoprop.end()) h->SetLineStyle(atoi(it->second.c_str()));
    // Apply Line width
    it = m_histoprop.find("LineWidth");
    if (it != m_histoprop.end()) h->SetLineWidth(atoi(it->second.c_str()));
    // Apply Fill Style
    it = m_histoprop.find("FillStyle");
    if (it != m_histoprop.end()) h->SetFillStyle(atoi(it->second.c_str()));
    // Set marker size
    it = m_histoprop.find("MarkerSize");
    if (it != m_histoprop.end()) h->SetMarkerStyle(atoi(it->second.c_str()));
    // Set marker color
    it = m_histoprop.find("MarkerColor");
    if (it != m_histoprop.end()) h->SetMarkerColor(NPAColor::parseColor(it->second));
    // Set marker style
    it = m_histoprop.find("MarkerStyle");
    if (it != m_histoprop.end()) h->SetMarkerStyle(atoi(it->second.c_str()));
}

void NPAPlotSample::addInput(NPAPlotInput *input){
    string id = input->getID();
    for (int i = 0; i < m_inputs.size(); ++i){
        if(m_inputs[i]->getID() == id){
            cerr<<"ERROR: The input "<<id<<" has already be added to the sample "<<m_ID<<endl;
            exit(1);
        }
    }
    m_inputs.push_back(input);
    /// Add all systematics names for the input
    vector<string> syst_names = input->getSystNames();
    m_sys_names.insert(m_sys_names.end(),
            syst_names.begin(),
            syst_names.end());
}

void NPAPlotSample::addErrors(TGraphAsymmErrors &orig, TGraphAsymmErrors toadd){
    int nbins = orig.GetN();
    for (int i = 0; i < nbins; ++i)
    {
        orig.SetPoint(i,orig.GetX()[i],orig.GetY()[i]+toadd.GetY()[i]);
        orig.SetPointEYhigh(i,
                orig.GetErrorYhigh(i)*orig.GetErrorYhigh(i)+
                toadd.GetErrorYhigh(i)*toadd.GetErrorYhigh(i));
        orig.SetPointEYlow(i,
                orig.GetErrorYlow(i)*orig.GetErrorYlow(i)+
                toadd.GetErrorYlow(i)*toadd.GetErrorYlow(i));
    }

    for (int i = 0; i < nbins; ++i)
    {
        orig.SetPointEYlow(i,sqrt(orig.GetErrorYlow(i)));
        orig.SetPointEYhigh(i,sqrt(orig.GetErrorYhigh(i)));
    }
}

int NPAPlotSample::getSystHistogram(string syst,const char* hname, TH1D* up, TH1D* down){
    // Check the size of the input vector
    if(m_inputs.size() == 0){
        cerr<<"ERROR: You have defined a sample ("<<m_name<<") but you have not specified any valid input."<<endl;
        cerr<<"Check the configuration file and run again"<<endl;
        exit(1);
    }

    int vars;
    for (const auto &in : m_inputs)    vars = in->addSyst(syst,hname,up,down);
    return vars;
}

void NPAPlotSample::plotSystVariations(const char* hname,string output,string col,xml_node plots){

    /// Read node for configuration
    if(!plots)
        cerr<<"WARNING: No ShapeSyst child for Plots node has been found. Default configuration will be applied"<<endl;

    /// Read options
    bool norm = plots.attribute("Normalise").as_bool(true);
    bool bbbin = plots.attribute("BinByBin").as_bool(true);
    bool nomstat = plots.attribute("ShowStatNom").as_bool(true);
    double ymin = plots.attribute("YAxisMin").as_double(-99);
    double ymax = plots.attribute("YAxisMax").as_double(-99);

    ///Remove duplicates in the syst names
    if(!m_sys_names_cleaned){
        sort(m_sys_names.begin(),m_sys_names.end());
        m_sys_names.erase(unique(m_sys_names.begin(),
                    m_sys_names.end()),
                m_sys_names.end());
        m_sys_names_cleaned = true;
    }

    // Define the base histogram and normalize it
    TH1D* base = getHistogram(hname);
    double base_integral = base->Integral();
    double low = base->GetBinLowEdge(1);
    double high = base->GetBinLowEdge(base->GetNbinsX()+1);
    int nbins = base->GetNbinsX();
    TH1D* base_norm = (TH1D*)base->Clone("base_cloned");
    //base_norm->Add(base_norm,-1);
    for (int i = 0; i <= base_norm->GetNbinsX()+1; i++) {
        if(!nomstat){
            base_norm->SetBinContent(i,0);
            base_norm->SetBinError(i,0);
        } else {
            if (norm){
                if(!bbbin){
                    if(i==0) base_norm->Scale(100./base_integral);
                } else{
                    double content = base->GetBinContent(i);
                    double error = base->GetBinError(i);
                    if(content != 0)
                        base_norm->SetBinError(i,100*error/content);
                    else base_norm->SetBinError(i,0);
                }
            }
            base_norm->SetBinContent(i,0);
        }
    }

    // Loop over all systematics
    for (int i = 0; i < m_sys_names.size(); i++)
    {
        // cout << "Plotting variation for systematic " << m_sys_names[i] << endl;
        double up_rel_var = 0;
        double down_rel_var = 0;
        int nsvar;
        stringstream nup, ndo;
        nup<<"up_var_"<<m_sys_names[i];
        ndo<<"down_var_"<<m_sys_names[i];
        TH1D *up = (TH1D*)base->Clone();//(nup.str().c_str(),nup.str().c_str(),nbins,low,high);
        TH1D *down = (TH1D*)base->Clone();//(ndo.str().c_str(),ndo.str().c_str(),nbins,low,high);
        up->SetDirectory(0);
        down->SetDirectory(0);
        up->Reset();
        down->Reset();
        for (int in = 0; in < m_inputs.size(); ++in)
            nsvar = m_inputs[in]->addSystVariations(m_sys_names[i],hname,up,down);

        up_rel_var = up->Integral();
        down_rel_var = down->Integral();
        /// Normalise variations
        if(norm){
            if(!bbbin){
                up->Scale(100./base_integral);
                down->Scale(100./base_integral);
                up_rel_var = up->Integral();
                down_rel_var = down->Integral();
            } else {
                up_rel_var = up->Integral()*100./base_integral;
                down_rel_var = down->Integral()*100./base_integral;
                for (int i = 0; i <= up->GetNbinsX()+1; i++) {
                    double ucont = up->GetBinContent(i);
                    double dcont = down->GetBinContent(i);
                    double bcont = base->GetBinContent(i);
                    if (bcont == 0){
                        up->SetBinContent(i,0);
                        down->SetBinContent(i,0);
                    } else {
                        up->SetBinContent(i,100*ucont/bcont);
                        down->SetBinContent(i,100*dcont/bcont);
                    }
                }

            }
        }

        // Define canvas and aspect
        TCanvas *c = new TCanvas("sys_var","sys_var",800,600);
        vector<double> ext;
        if (ymax != -99 && ymin != -99){
            ext.push_back(ymin);
            ext.push_back(ymax);
        } else ext = (nomstat) ? getExtremes(*up, *down, base_norm, 0.5,0.1) :getExtremes(*up, *down, NULL, 0.5,0.1) ;

        TH1F* h = c->DrawFrame(low,ext[0],high,ext[1]);
        h->SetXTitle(base->GetTitle());
        if (norm) h->SetYTitle("Relative variation (\%)");
        else h->SetYTitle("Absolute variation");
        h->SetStats(0);
        up->SetLineColor(kBlue);
        up->SetFillColor(0);
        down->SetLineColor(kRed);
        down->SetFillColor(0);
        base_norm->SetLineColor(kBlack);
        base_norm->SetFillStyle(3354);
        base_norm->SetFillColor(kBlack);

        //Draw histograms and text
        up->Draw("histosame");
        down->Draw("histosame");
        base_norm->Draw("histosamee2");
        stringstream upname; upname.precision(2);
        stringstream downame; downame.precision(2);
        if(nsvar == 2){
            upname<<"Total up variation "<<up_rel_var;
            downame<<"Total down variation: "<<down_rel_var;
        } else upname<<"Total variation "<<up_rel_var;
        if(norm)upname<<"\%";
        if(norm)downame<<"\%";
        TLatex tup(0.20,0.75,upname.str().c_str());
        tup.SetNDC();
        tup.SetTextSize(0.04);
        //tup.SetTextColor(kBlue);
        TLatex tdo(0.20,0.7,downame.str().c_str());
        tdo.SetNDC();
        tdo.SetTextSize(0.04);
        //tdo.SetTextColor(kRed);
        tup.Draw("same");
        if(nsvar == 2) tdo.Draw("same");
        TLatex sys(0.15,0.8,(m_name+" ("+m_sys_names[i]+")").c_str());
        sys.SetNDC();
        sys.SetTextSize(0.05);
        sys.SetTextColor(kBlack);
        sys.Draw("same");

        TLegend leg(0.63,0.68,0.85,0.85);
        leg.SetFillColor(0);
        leg.SetFillStyle(0);
        leg.SetBorderSize(0);
        leg.SetTextFont(72);
        leg.SetTextSize(0.04);
        if(nsvar == 2){
            leg.AddEntry(up,"Up var.","l");
            leg.AddEntry(down,"Down var.","l");
        } else leg.AddEntry(up,"Variation","l");
        if(nomstat) leg.AddEntry(base_norm,"#splitline{Nominal stat.}{uncertainty}","f");
        leg.Draw("same");


        stringstream outputdir;
        stringstream outputfile;
        outputdir<<output<<"/SystVars/"<<m_sys_names[i]<<"/"<<m_ID<<"/";
        system(("mkdir -p "+outputdir.str()).c_str());
        string extension = "pdf";
        if (plots.attribute("Extension")) extension = plots.attribute("Extension").value();
        outputfile<<outputdir.str()<<col<<"_"<<hname<<"."<<extension;
        c->Print(outputfile.str().c_str());
        delete c;
    }

}

vector<double> NPAPlotSample::getExtremes(TH1D up, TH1D down, TH1D* base, double plus, double minus){

    vector<double> extremes;
    double min = 99999999;
    double max = -99999999;
    // Check maximum and minimum of variations
    if(min > up.GetMinimum()) min = up.GetMinimum();
    if(min > down.GetMinimum()) min = down.GetMinimum();
    if(max < up.GetMaximum()) max = up.GetMaximum();
    if(max < down.GetMaximum()) max = down.GetMaximum();
    if (base != NULL){
        for (int b = 1; b <= base->GetNbinsX(); b++)
        {
            if(min > -base->GetBinError(b)) min = -base->GetBinError(b);
            if(max < base->GetBinError(b)) max = base->GetBinError(b);
        }
    }

    if(min > 0 ) minus *= -1;
    if(max < 0) plus *= -1;
    double h = max-min;
    extremes.push_back(min-h*1/4);
    extremes.push_back(max+h*3/4);

    return extremes;
}

vector<string> NPAPlotSample::getListOfHistos(){
    // Check the size of the input vector
    if(m_inputs.size() == 0){
        cerr<<"ERROR: You have defined a sample but you have not specified any valid input."<<endl;
        cerr<<"Check the configuration file and run again"<<endl;
        exit(1);
    }

    return m_inputs[0]->getListOfHistos();
}

void NPAPlotSample::defineYields(){
    /// Loop over all inputs and add the yields to the own variable
    // and insert all vectors with errors to be derived lately.
    TH1D* h = getHistogram(m_yields_source);
    double err;
    m_yields = h->IntegralAndError(0,h->GetNbinsX(),err);
    m_stat_errors.push_back(err);
    double up = 0;
    double down = 0;
    vector<string> names = getSystNames();
    vector<double> upsyst(names.size(),0.0);
    vector<double> dosyst(names.size(),0.0);
    for (auto &in : m_inputs) {
        in->setYieldsSource(m_yields_source);
        for (int s = 0; s < names.size(); s++){
            upsyst[s] += in->getSystUpYields(m_yields_source,names[s],true);
            dosyst[s] += in->getSystDownYields(m_yields_source,names[s],true);
        }  
    }
    for (int i = 0; i < upsyst.size(); i++){
        double uu = upsyst[i]-m_yields; 
        double dd = dosyst[i]-m_yields; 
        if(uu > 0) m_up_syst_errors.push_back(uu);
        else m_down_syst_errors.push_back(uu);
        if(dd > 0) m_up_syst_errors.push_back(dd);
        else m_down_syst_errors.push_back(dd);
    }
    delete h;
}

NPAPlotSample NPAPlotSample::operator+(const NPAPlotSample& other) const {
    /// Two samples of different nature cannot be added becase some
    //of the methods will loose meaning, besides it has no physical
    //meaning
    if(m_type != other.getType()){
        cerr<<"ERROR: You are trying to add two samples of different nature: "<<m_type<<" and "<<other.getType()<<"."<<endl;
        exit(1);
    }
    /// Dfine the ID and name of the new sample as the 
    //merge of the two.
    string newID = m_ID+"+"+other.getID();
    string newname = m_name+"+"+other.getName();
    //Create a temporary sample
    NPAPlotSample tmp(newID,newname);
    /// Add inputs
    for(int i = 0; i < m_inputs.size();i++) tmp.addInput(m_inputs[i]);
    for(int i = 0; i < other.getInputs().size(); i++) tmp.addInput(other.getInputs()[i]);
    tmp.setType(m_type);
    tmp.m_defined = false;
    return tmp;
}

NPAPlotSample& NPAPlotSample::operator+=(const NPAPlotSample& other){
    if(m_type != other.m_type){
        cerr<<"ERROR: You are trying to add two samples of different nature: "<<m_type<<" and "<<other.getType()<<"."<<endl;
        exit(1);
    }
    // Add this and the other sample
    //m_ID = m_ID+"+"+other.getID();
    //m_name = m_name+"+"+other.getName();
    for (auto &in : other.getInputs()) addInput(in);
    m_defined = false;
    return *this;
}

vector<string> NPAPlotSample::getSystNames(){
    ///Remove duplicates in the syst names
    if(!m_sys_names_cleaned){
        sort(m_sys_names.begin(),m_sys_names.end());
        m_sys_names.erase(unique(m_sys_names.begin(),
                    m_sys_names.end()),
                m_sys_names.end());
        m_sys_names_cleaned = true;
    }
    return m_sys_names;
}

double NPAPlotSample::getSystUpYields(string hname, string syst, bool nominal){
    // Loop over all inputs and get the up yileds
    double yields = 0;
    for (int i = 0; i < m_inputs.size(); i++){ 
        /*cout << "Syst yields for input " << m_inputs[i]->getID() <<endl;
        cout << "  -> Yields: " << m_inputs[i]->getYields()<<endl;
        cout << "  -> Up yields: " << m_inputs[i]->getSystUpYields(hname,syst,nominal)<<endl;*/
        yields += m_inputs[i]->getSystUpYields(hname,syst,nominal);
    }
    return yields;
}

double NPAPlotSample::getSystDownYields(string hname, string syst, bool nominal){
    // Loop over all inputs and get the up yileds
    double yields = 0;
    for (int i = 0; i < m_inputs.size(); i++) {
        /*cout << "Syst yields for input " << m_inputs[i]->getID() <<endl;
        cout << "  -> Yields: " << m_inputs[i]->getYields()<<endl;
        cout << "  -> Up yields: " << m_inputs[i]->getSystDownYields(hname,syst,nominal)<<endl;*/
        yields += m_inputs[i]->getSystDownYields(hname,syst,nominal);
    }
    return yields;
}

vector<double> NPAPlotSample::getSystError(string hname, string syst){
    // Loop over all inputs and get the up and down error
    //cout << "Errors systematic " << syst << " and sample: "<< m_name << endl;
    double uerr = 0;
    double derr = 0;
    for (int i = 0; i < m_inputs.size(); i++) {
        vector<double> err = m_inputs[i]->getSystError(hname,syst);
        uerr += err[0]*err[0];
        derr += err[1]*err[1];
    }
    vector<double> v = {sqrt(uerr),sqrt(derr)};
    return v;
}
