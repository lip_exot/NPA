#include "NPALegend.h"
#include <iostream>

NPALegend::NPALegend(const char* id, double x1, double y1, double x2, double y2):
NPADrawable(id, x1,x2,y1,y2),
TLegend(x1,y1,x2,y2){

	put(x1,y1,x2,y2);
}

NPALegend::~NPALegend(){}

void NPALegend::draw(const char* opt){
	Draw(opt);// Call the Draw function of TLegend
}

void NPALegend::update(Mods m){
	std::cout<<"Mod: "<<m<<std::endl;
	switch(m){
		case CHANGE_POS:
		std::cout<<"Entra"<<std::endl;
		Coord c = getCoord();
		std::cout<<"Coords: "<<
		"x1 "<<c.x1<<
		"x2 "<<c.x2<<
		"y1 "<<c.y1<<
		"y2 "<<c.y2<<std::endl;
		SetX1(c.x1);
		SetX2(c.x2);
		SetY1(c.y1);
		SetY2(c.y2);
		break;

	}
}
