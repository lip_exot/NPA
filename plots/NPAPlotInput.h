/**
NPAPlotInput is the object which references a sample that will be plotted.
It takes care of all inputs rootfiles, systematics for each sample
and make calculations needed to get variation regions.
*/
#ifndef NPA_PLOT_INPUT_H
#define NPA_PLOT_INPUT_H
#include "TFile.h"
#include "NPAYields.h"
#include <vector>
#include <string>
#include <map>
#include <string>
#include <algorithm>
#include <memory>

class TH1D;
class TGraphAsymmErrors;

class NPAPlotInput: public NPAYields
{
public:
	NPAPlotInput(std::string ID, std::string rootfile);
	/*NPAPlotInput(const NPAPlotInput& i);*/
	~NPAPlotInput();

	/// Type of input: data, signal or backtround
	enum inputType{DATA,SIGNAL,BCK,NOINPUT};
	enum sys_type{FILE, HISTO, SCALE,VAR,NONE};

	/// Add shape systematic to this sample
	void addUpSyst(const std::string ID, std::string name, std::string input, const std::string type, const std::string ud="up");
	void addDownSyst(const std::string ID, std::string name, std::string input, const std::string type){addUpSyst(ID,name,input,type,"down");};
	void addOneSideSyst(const std::string ID, std::string name, std::string input, const std::string type){m_onesided_syst.push_back(ID);addUpSyst(ID,name,input,type,"none");};
    /// Tell if a given systematic is one sided
    bool isOneSided(const std::string ID){return find(m_onesided_syst.begin(),m_onesided_syst.end(),ID) != m_onesided_syst.end();};

	/// Load a given histogram and the error of that histogram is
	/// derived as the statistical plus all the systematics.
	TH1D* getHistogram(std::string hname, TGraphAsymmErrors &graph);
	TH1D* getHistogram(std::string hname);

	/// Add the variation of a given systematic bin by bin for up and down. 
	int addSystVariations(std::string syst, const char* hname, TH1D *up, TH1D *down);

	/// Add the given systematic. 
	int addSyst(std::string syst, const char* hname, TH1D* up, TH1D* down);
	
    /// Get the id of the input
	std::string getID() const {return m_ID;};

	/// Avoid to include overflow and upperflow in histograms
	void noFlows(){p_include_flows=false;};

	/// Set type of the input
	void setType(inputType t){m_type = t;};

	/// Get the list with the names of the systematics
	std::vector<std::string> getSystNames(){if(!m_syst_ids_cleaned) 
                                            removeDuplicatedSysNames();
                                            return m_syst_ids;};

    /// Get list of all histograms stored in the files
    std::vector<std::string> getListOfHistos();

    /// Get the total number of systematics
    int getNSyst(){return m_syst_up_list.size();};

    /// Get name of the tfile
    std::string getSource(){return m_file->GetName();};

    /// Define the virtual function from NPAYields to get yields and errors
    virtual void defineYields();

    // Get the events of a given systematic.
    double getSystUpYields(std::string hname, std::string syst, bool nominal = false);
    double getSystDownYields(std::string hname, std::string syst, bool nominal = false);
    
    // Get the up and down error of a given systematic
    std::vector<double> getSystError(std::string, std::string);

    // Get the histogram of a given systematic.
    TH1D* getSystUpHistogram(std::string hname, std::string syst, bool nominal = false);
    TH1D* getSystDownHistogram(std::string hname, std::string syst, bool nominal = false);

private:
	// Private methods
	void strToType();
	bool checkSysType(std::string type);
    void removeDuplicatedSysNames();

	void includeFlows(TH1D& h);//TODO: Include underflow and upperflow

	// Private variables
	std::map<std::string, sys_type> m_strtotype;
	TFile* m_file;
	std::string m_ID;
	std::map<std::string, const void*> m_syst_up_list;//!< Lis of all systematics by ID
	std::map<std::string, sys_type> m_syst_up_type;//!< List with all the type of systematics.
	std::map<std::string, const void*> m_syst_down_list;//!< Lis of all systematics by ID
	std::map<std::string, sys_type> m_syst_down_type;//!< List with all the type of systematics.

	std::vector<std::string> m_syst_ids;
	std::vector<std::string> m_syst_names;
	std::vector<std::string> m_histos_names;
    std::vector<std::string> m_onesided_syst;
	std::vector<double> m_scale_vars;//!< Just to keep track of possible scal variations.
	bool p_include_flows;
    bool m_histos_names_filled;
    bool m_syst_ids_cleaned;
	inputType m_type;

    // Counter to keep track of how many times the base histogram has been retrieved
    // so we can avoid to overwrite it by loading the same one
    int m_base_histo_counter;
};

#endif
