#ifndef NPASAMPCOL_H
#define NPASAMPCOL_H

#include "NPAXML.h"
#include "NPAPlotSample.h"
#include "NPAInputCol.h"
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
using namespace std::placeholders;

class TGraphAsymmErrors;

class NPASampCol:public NPAXML
{
    public:
        NPASampCol(const char* xml,std::string root,std::string samples_name);
        ~NPASampCol();

        // getters
        std::vector<NPAPlotSample*> getSamples(){return m_sample_col;};
        int getNSamples(){return m_sample_col.size();};
        std::vector<NPAPlotSample*> getDataSamples();
        int getNData(){return m_data_index.size();};
        std::vector<NPAPlotSample*> getBckSamples();
        int getNBackground(){return m_bck_index.size();};
        std::vector<NPAPlotSample*> getSignalSamples();
        int getNSignal(){return m_signal_index.size();};
        std::string getName(){return m_sample_col_name;};
        NPAPlotSample* getSample(std::string ID);
        NPAPlotSample* nextSample();
        std::vector<std::string> getListOfHistos();
        NPAPlotSample* getTotalBackground(){return m_total_background;};
        std::vector<double> getBackgroundSystError();

        // Get a vector with a complete set of systeamtics in the samples
        std::vector<std::string> getSystNames(std::vector<std::string> base = std::vector<std::string>());


        void resetSampleCounter(){m_actual_sample = m_sample_col.begin();};
        void setInputsFile(std::string file);
        // Plot systematic variations for all samples in the collection
        // or just for one of them
        void plotSystVariations(const char* hname);
        void plotSampleSystVariations(const char* hname, std::string ID);
        void plotSampleSystVariations(const char* hname, NPAPlotSample* s);
        // Get systematic error band
        TGraphAsymmErrors getErrorBand(std::string hname);

        // Get the histogram of all MC samples.
        TH1D* getHistogram(std::string hname,TGraphAsymmErrors &graph); 
        TH1D* getHistogram(std::string hname); 
        TH1D* getDummyHistogram(std::string hname); 
        void parse();

        // Get the inputs collection in case it needs to be accessed
        NPAInputCol* getInputCol(){if(!m_checks.parse)parse();return m_input_col;};

        // Get a list with all the inputs in a given collection
        std::vector<std::string> getListOfInputs(std::vector<PlotProperties::sample_t> s);

        // Set string to find in histograms to accept
        void setHistosToAccept(std::vector<std::string> acc){ m_h_to_accept = acc;};
        void setHistosToAvoid(std::vector<std::string> avo){ m_h_to_avoid = avo;};




    private:

        // Auxiliary functions
        static bool sameSampleID(NPAPlotSample* s, std::string ss){return s->getID()==ss;};
        static bool hasSubstr(std::string test, std::string base){return test.find(base) != std::string::npos;};
        static bool cleanHistoVector(std::string string,std::vector<std::string> avoid, std::vector<std::string> accept){
            return (std::find_if(avoid.begin(),avoid.end(),std::bind(hasSubstr,string,_1)) != avoid.end() ||
                    std::find_if(accept.begin(),accept.end(),std::bind(hasSubstr,string,_1)) == accept.end());
        }
        static bool sortSystNames(std::string s1, std::string s2, std::vector<std::string> base){
            std::vector<std::string>::iterator it1 = std::find(base.begin(), base.end(),s1);
            std::vector<std::string>::iterator it2 = std::find(base.begin(), base.end(),s2);
            return it1 < it2;
        }

        std::string getPlotOptions(std::string node);

        std::vector<NPAPlotSample*> m_sample_col;
        std::vector<int> m_data_index;
        std::vector<int> m_bck_index;
        std::vector<int> m_signal_index;
        std::vector<std::string> m_h_to_avoid;
        std::vector<std::string> m_h_to_accept;
        std::vector<std::string> m_syst_names;;
        std::vector<NPAPlotSample*>::iterator m_actual_sample;
        std::string m_inputs_file;
        std::string m_sample_col_name;
        NPAInputCol* m_input_col;
        // Total background sample
        NPAPlotSample *m_total_background;

};


#endif
