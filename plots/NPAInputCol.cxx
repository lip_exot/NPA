#include "NPAInputCol.h"
#include "NPAXML.h"
#include "NPAPlotInput.h"
#include <vector>
#include <iostream>
#include <functional>
using namespace std::placeholders;
using namespace std;

NPAInputCol::NPAInputCol(const char* xml,string root):NPAXML(xml,root){
}

NPAInputCol::~NPAInputCol(){
    for(auto &x : m_input_col) delete x; 
}

void NPAInputCol::parse(){
    cout<<endl<<"---------------------------"<<endl;
    cout<<endl<<" Loading inputs to be used "<<endl;
    cout<<endl<<"---------------------------"<<endl;
    vector<PlotProperties::input_t> inputs;
    inputs = getXmlInputs();
    if(m_to_filter.size() > 0)
        inputs.erase(remove_if(inputs.begin(),
                    inputs.end(),
                    bind(&NPAInputCol::toFilter,_1,m_to_filter)),inputs.end());
    // Create all inputs objects in the collection
    for (int i = 0; i < inputs.size(); ++i)
    {
        PlotProperties::input_t input = inputs[i];
        NPAPlotInput *in = new NPAPlotInput(input.id, input.source);
        cout<<"\tInput: "<<input.id<<endl;

        // Loop over all systematics
        if(m_do_syst){
            for (int sy = 0; sy < input.syst.size(); ++sy)
            {
                PlotProperties::syst_t syst = input.syst[sy];
                if(syst.onlysource.length() > 1) in->addOneSideSyst(syst.id,syst.name, syst.onlysource,syst.type);
                else{
                    in->addUpSyst(syst.id,syst.name, syst.upsource,syst.type);
                    in->addDownSyst(syst.id, syst.name, syst.dosource,syst.type);
                }
            }
        }
        m_input_col.push_back(in);
    }
    m_actual_input = m_input_col.begin();
    m_checks.parse=1;
    cout<<"Done!"<<endl;
}

NPAPlotInput* NPAInputCol::getInput(string id){
    if(!m_checks.parse)parse();
    vector<NPAPlotInput*>::iterator it = find_if(m_input_col.begin(),m_input_col.end(),bind(&NPAInputCol::sameInputID,_1,id));
    if (it == m_input_col.end()){
        cerr<<"ERROR: Input "<<id<<" does not exists. Request a existing one, nullputr is being returned, be sure to take care of it."<<endl;
        return nullptr;
    }
    return *it;
}

NPAPlotInput* NPAInputCol::nextInput(){
    if(!m_checks.parse)parse();
    if(m_actual_input == m_input_col.end()) return NULL;
    return *m_actual_input++;
}

