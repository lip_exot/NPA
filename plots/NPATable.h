#ifndef NPATABLE_H
#define NPATABLE_H
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include "NPATableCell.h"


class NPATable {
public:
    NPATable (int rows, int columns);
    virtual ~NPATable ();

    void addCell(int x, int y, std::string content);
    void write(std::ofstream &file);
    void setStretch(std::string s){m_stretch = s;};
private:
    void updateWidths();    
    std::vector<std::vector<NPATableCell> > m_cells;
    std::vector<int> m_columns_width;
    std::string m_stretch;
    int m_rows;
    int m_columns;

};

#endif
