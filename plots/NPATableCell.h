#ifndef NPATABLECELL_H
#define NPATABLECELL_H
#include <string>

class NPATableCell {
public:
    NPATableCell (int x, int y, std::string content);
    NPATableCell (): m_X(0), m_Y(0),m_content(""){};
    /*NPATableCell (std::string content);
    NPATableCell (std::string content,std::string format);*/
    // TODO: Needs to add format to the cells and add for that new constructros.
    virtual ~NPATableCell ();

    std::string write();
    void updateWidth(int w){m_width = w;};

private:
    std::string m_content;
    int m_X, m_Y;
    int m_width;

};

#endif
