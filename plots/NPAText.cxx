#include "NPAText.h"
#include "TMarker.h"
#include <iostream>

NPAText::NPAText(const char* id): NPADrawable(id), TLatex(){
	m_marker = NULL;
	m_text = "";
}

NPAText::NPAText(const char* id, double x1, double y1, const char* text): NPADrawable(id, x1, y1),
TLatex(x1, y1, text){
	put(x1,y1,x1+getLength(),y1+getHight());
	m_marker = NULL;
	m_text = text;
}

NPAText::NPAText(NPAText &t): NPADrawable(t), TLatex(t){
	t.getMarker()->Copy(*this);
}

NPAText::~NPAText(){
	if(m_marker != NULL) delete m_marker;
}

double NPAText::getLength(){
	return GetXsize();
}

double NPAText::getHight(){
	return GetYsize();
}

void NPAText::draw(const char* opt){
	if(m_marker != NULL){
		m_marker->Draw();
	}
	Paint(opt);
}

void NPAText::addMarker(const char* pos, double d, int marker){
	if(pos == "left")
	m_marker = new TMarker(m_coord.x1-d,m_coord.y1,marker);
	else if (pos == "right")
	m_marker = new TMarker(m_coord.x1+getLength()+d,m_coord.y1,marker);
	else std::cerr<<"Error: the marker position "<<pos<<" is not valid."<<std::endl;
}

