#include "NPADrawable.h"
#include "NPAXML.h"
#include <iostream>

/**
Create a Drwable object without espcifying the
coordenates.
@param id ID of the Drawable
*/
NPADrawable::NPADrawable(const char* id){
	m_id = id;
	m_coord.x1 = 0;
	m_coord.y1 = 0;
	m_coord.x2 = -1;
	m_coord.y2 = -1;
}

NPADrawable::~NPADrawable(){}

/**
Create a Drwable object espcifying the
coordenates.
@param id ID of the Drawable
@param x X position of the Drawable.
@param y Y position of the Drawable.
*/
NPADrawable::NPADrawable(const char* id, double x, double y){
	m_id = id;
	m_coord.x1 = x;
	m_coord.y1 = y;
	m_coord.x2 = -1;
	m_coord.y2 = -1;
}

/**
Create a Drwable object with specific lenght and hight
@param id ID of the Drawable
@param x1 X position of the bottom-left corner.
@param y1 Y position of the bottom-left corner.
@param x2 X position of the top-right corner.
@param y2 Y position of the top-right corner.
*/
NPADrawable::NPADrawable(const char* id, double x1, double y1, double x2, double y2){
	m_id = id;
	m_coord.x1 = x1;
	m_coord.y1 = y1;
	m_coord.x2 = x2;
	m_coord.y2 = y2;
}

void NPADrawable::alignLeft(const NPADrawable *d,double dist){
		double l = getLength();
	double h = getHight();
	double ld = d->getLength();
	double hd = d->getHight();
	m_coord.x1 = d->getXPos()-l+dist;
	double yoff = (hd == 0) ? 0 : (hd-h)/2;
	m_coord.y1 = d->getYPos()+yoff;
	if(m_coord.x2 != -1) m_coord.x2 = m_coord.x1+l;
	if(m_coord.y2 != -1) m_coord.y2 = m_coord.y1+h;
	update(CHANGE_POS);
}

void NPADrawable::alignRight(const NPADrawable *d,double dist){
	//Save lenght and hight
	double l = getLength();
	double h = getHight();
	double ld = d->getLength();
	double hd = d->getHight();
	m_coord.x1 = d->getXPos()+dist+ld;
	double yoff = (hd == 0) ? 0 : (hd-h)/2;
	m_coord.y1 = d->getYPos()+yoff;
	if(m_coord.x2 != -1) m_coord.x2 = m_coord.x1+l;
	if(m_coord.y2 != -1) m_coord.y2 = m_coord.y1+h;
		update(CHANGE_POS);
}

void NPADrawable::alignTop(const NPADrawable *d,double dist){
	double l = getLength();
	double h = getHight();
	double ld = d->getLength();
	double hd = d->getHight();
	double xoff = (ld == 0) ? 0 : (ld-l)/2;
	m_coord.x1 = d->getXPos()+xoff;
	m_coord.y1 = d->getYPos()+hd+dist;
	if(m_coord.x2 != -1) m_coord.x2 = m_coord.x1+l;
	if(m_coord.y2 != -1) m_coord.y2 = m_coord.y1+h;
		update(CHANGE_POS);
}

void NPADrawable::alignBottom(const NPADrawable *d,double dist){
	double l = getLength();
	double h = getHight();
	double ld = d->getLength();
	double hd = d->getHight();
	double xoff = (ld == 0) ? 0 : (ld-l)/2;
	m_coord.x1 = d->getXPos()+xoff;
	m_coord.y1 = d->getYPos()-h-dist;
	if(m_coord.x2 != -1) m_coord.x2 = m_coord.x1+l;
	if(m_coord.y2 != -1) m_coord.y2 = m_coord.y1+h;
		update(CHANGE_POS);
}

double NPADrawable::getLength() const{
	return (m_coord.x2 == -1) ? 0 : m_coord.x2-m_coord.x1;};

double NPADrawable::getHight() const {
	return (m_coord.y2 == -1) ? 0 : m_coord.y2-m_coord.y1;
}
