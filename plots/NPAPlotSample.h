/**
  NPAPlotSample is the object which references a sample that will be plotted.
  It takes care of format and add histograms if needed. 
  */
#ifndef NPA_PLOT_SAMPLE_H
#define NPA_PLOT_SAMPLE_H
#include <vector>
#include <string>
#include <map>
#include "NPAYields.h"
#include "pugixml.hpp"

class TH1D;
class TH1;
class NPAPlotInput;
class NPASampCol;
class TGraphAsymmErrors;

class NPAPlotSample:public NPAYields
{
    public:
        NPAPlotSample(std::string ID,std::string name);
        NPAPlotSample(const NPAPlotSample& s);
        ~NPAPlotSample();

        /// Load a given histogram and the error of that histogram is
        /// derived as the statistical plus all the systematics.
        TH1D* getHistogram(std::string hname, TGraphAsymmErrors &graph);
        TH1D* getHistogram(std::string hname);
        /// Apply properties to a given histogram
        void applyHistoProperties(TH1* h);

        /// Get the ID of the sample
        std::string getID() const {return m_ID;};
        std::string getName() const {return m_name;};
        //SEt the ID and name of the sample if needed
        void setID(std::string id){m_ID = id;};
        void setName(std::string name){m_name = name;};

        /// Add an input to the Sample
        void addInput(NPAPlotInput *input);
        /// Get the set of inputs of the sample
        std::vector<NPAPlotInput*> getInputs() const {return m_inputs;};

        /// Plot the variation per systematic in the sample
        void plotSystVariations(const char* hname,std::string output, std::string col,pugi::xml_node plots);
        /// Get the systematic variations up and down for a given systematic
        int getSystVariation(std::string syst,const char* hname, TH1D &up, TH1D &down);

        std::vector<std::string> getListOfHistos();

        /// Set type of sample (data, bck or signal)
        void setType(std::string t){m_type=t;};
        /// Get the type of the sample
        std::string getType() const {return m_type;};

        /// Define yields for NPAYields
        virtual void defineYields();

        /// Get list with all systematic names
        std::vector<std::string> getSystNames();

        /// Get the histogram of a given systematic
        int getSystHistogram(std::string syst,const char* hname, TH1D* up, TH1D* down);

        // Get the yields of a given systematic up or down
        double getSystUpYields(std::string hname, std::string syst,bool nominal = false);
        double getSystDownYields(std::string hname, std::string syst, bool nominal = false);

        // Get systematic up and down error from all inputs in the sample
        std::vector<double> getSystError(std::string, std::string);

        // Set the associated sample collection in case there is one
        void setCollection(NPASampCol *col){ m_collection = col;};
        // Set the plot properties for the sample
        void setHistoProperties(std::map<std::string, std::string> s){m_histoprop = s;};


        NPAPlotSample operator+(const NPAPlotSample& other) const;
        NPAPlotSample& operator+=(const NPAPlotSample& other);

    private:

        /// Get the maximum and minimum of the up and down syst variations
        std::vector<double> getExtremes(TH1D up, TH1D down, TH1D* base, double plus=1, double minus=1);
        /// Add the TGraphAsymmErrors to get the global one
        static void addErrors(TGraphAsymmErrors &orig, TGraphAsymmErrors err);

        std::vector<NPAPlotInput*> m_inputs;
        std::vector<std::string> m_sys_names;
        bool m_sys_names_cleaned;
        std::string m_ID, m_name,m_type;
        /// Porperties of the histogram once it is obtained by getHistogram
        std::map<std::string,std::string> m_histoprop;

        /// Sample collection associated in case there is a collection where this sample belongs
        NPASampCol *m_collection;
};

#endif
