#ifndef NPATABLEWRITER_H
#define NPATABLEWRITER_H
#include <fstream>
#include <vector>
#include "NPASampCol.h"
#include "NPAXML.h"
#include "NPATable.h"

class NPATableWriter : public NPAXML {
public:
    NPATableWriter (const char* xml);
    virtual ~NPATableWriter ();

    void writeYieldsTable(std::string hname,std::string id);
    void writeSystTable(std::string hname,std::string id);
    std::string getYields(NPAPlotSample* s, std::string hname,bool isdata=false);
    std::string getSystVar(NPAPlotSample* s, std::string hname, std::string syst);
    std::string getSensitivity(NPAPlotSample* signal, NPAPlotSample* bck,std::string hname);
    std::string getAgreement(NPAPlotSample* data, NPAPlotSample* bck,std::string hname);
    
    // Methods to call to configure everything needed
    void configureYields(pugi::xml_node yields);
    void configureSyst(pugi::xml_node syst);

private:
    std::vector<std::string> getTotalSamples(std::vector<NPASampCol>);
    std::ofstream m_yields_file;
    std::ofstream m_syst_file;

    // Sample collections used
    std::vector<NPASampCol*> m_samples;
    std::string m_output_dir;
    struct bits{
        int yfile_created;
        int sfile_created;
        int significance;
        int agreement;
        int signal;
        int data;
        int dataerror;
        int total_bck;
        int total_signal;
        int samples_defined;
        int sym_syst;
        int merge_errors;
        int show_syst;
        int show_stat;
    } m_bits;

    struct format{
        int precision;
        std::string stretch;
    } m_format;
};

#endif
