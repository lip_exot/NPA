/**
NPALegend is used to put a legend in the plot generated
with NPAPlots libraries. This legend inherits from TLegend
but is able to comunicate with the rest of the objetc in 
the plot.
*/
#ifndef NPA_LEGEND_H
#define NPA_LEGEND_H
#include "NPAConfig.h"
#include "NPADrawable.h"
#include "TLegend.h"

class NPALegend:public NPADrawable, public TLegend
{
public:
	NPALegend(const char* id, double x1, double y1, double x2, double y2);
	virtual ~NPALegend();

	virtual void draw(const char* opt); //!< Definition of the draw function.
	virtual void update(Mods m);//!< Define what updates can be done in this objetc.


private:
	void updateLegend();
	
};
#endif