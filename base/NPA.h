// #################################################################################
//
//                                         
//          888b      88  88888888ba      db         
//          8888b     88  88      "8b    d88b        
//          88 `8b    88  88      ,8P   d8'`8b       
//          88  `8b   88  88aaaaaa8P'  d8'  `8b      
//          88   `8b  88  88""""""'   d8YaaaaY8b     
//          88    `8b 88  88         d8""""""""8b    
//          88     `8888  88        d8'        `8b   
//          88      `888  88       d8'          `8b  
//                                         
//                                         
//                                                                              
// #################################################################################
//                                                                              
//   NPA                                         \      | | / |  /    |   |  ||  
//                                                '-___-+-+'  |_/     |   |  |   
//   by J.P. Araque, N. Castro, V. Oliveira      `--___-++--^'|       |   | /|   
//                                                      ||    |       |   |' |   
//   date: 19-12-2013                            --___  ||    |       | _/|  |   
//                                               ==---:-++___ |  ___--+'  |  |   
//                                                 '--_'|'---'+--___  |   |  |   
//                                                     '+-_   |     '-+__ |  |   
//                                              ._.          ._.       ._____   
//                                              | |          | |       | ___ \  
//                                              | |_.        | |       | .___/  
//                                              |___|        |_|       |_|
//      
//   Note: 	this code was built from LipCbrAnalysis developed by 
//     	   	Nuno Castro   (nfcastro@lipc.fis.uc.pt)  and 
//         	Filipe Veloso (fveloso@lipc.fis.uc.pt)             
//   Purpose: NPA is intended to be a complete API to develop new physics analyses
//            using NPA as an interface class.
//   Version: 1.0 (This is a quick adaptation of LipCbrAnalysis and several changes
//                are foreseen).
//
//
// #################################################################################

#ifndef NPA_h
#define NPA_h

#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TH1.h>
#include <TNtuple.h>
#include <TRandom3.h>

#include <vector>

#include <getopt.h>
#include <time.h>
#include <fstream>
#include <sys/statvfs.h>
#include <stdlib.h>

#include "TLorentzVectorWFlags.h"
#include "Ntu.h"
#include "NPAConfig.h"


// Compare by pt
bool LorentzVecComp(TLorentzVectorWFlags a, TLorentzVectorWFlags b);

// #############################################################################
class TInput {
// #############################################################################
public:
  TInput();
  ~TInput();
  inline void File(std::string Type, std::string Name) {p_Type.push_back(Type); p_Name.push_back(Name);}
  inline std::string Type(int j) {return p_Type[j];}
  inline std::string Name(int j) {return p_Name[j];}
  inline int size() {return p_Type.size();}

private:
  std::vector<std::string> p_Type;
  std::vector<std::string> p_Name;
};

// #############################################################################
class TMonteCarlo {
// #############################################################################
public:
  TMonteCarlo(int type, double run, double lum, int nGenEvt, std::string title, int MaxCuts);
  ~TMonteCarlo();
  inline void AddSelEvt(Int_t i_syst, Int_t level) {p_nSelEvt[i_syst][level]++;}
  inline void AddSelWeightedEvt(Int_t i_syst, Int_t level, Double_t weight) {p_nSelWeightedEvt[i_syst][level]+=weight;}
  inline int type() {return p_type;}
  inline double run() {return p_run;}
  inline double CrossSection() {return p_CrossSection;}
  inline int nGenEvt() {return p_nGenEvt;}
  void SetnGenEvt(int nGenEvt);
  inline std::string title() {return p_title;}
  inline double lum() {return p_lum;}
  inline double nSelEvt(Int_t i_syst, Int_t level) {return p_nSelEvt[i_syst][level];}
  inline double nSelWeightedEvt(Int_t i_syst, Int_t level) {return p_nSelWeightedEvt[i_syst][level];}
//  inline double nSelWeightedEvtErr(int level) {if (p_type != 0 ) {if (nSelEvt(level) != 0) { return nSelWeightedEvt(level)/sqrt(nSelEvt(level));} else {return p_nSelWeightedEvtErr[level];}} // nSelWeightedEvt(level)/nSelEvt(level)*sqrt(nSelEvt(level))
  double nSelWeightedEvtErr(Int_t i_syst, Int_t level);
  void SetnSelWeightedEvtErr(Int_t i_syst, Int_t level, double err);
  inline double nSelWeightedEvtEff(Int_t i_syst, Int_t level) {return (p_nGenEvt > 0.) ? 100.*(nSelEvt(i_syst, level)/p_nGenEvt) : 0.;}
  inline double nSelWeightedEvtEffErr(Int_t i_syst, Int_t level) {return (nSelWeightedEvt(i_syst, 0) > 0.) ? 100.*(sqrt(nSelWeightedEvt(i_syst, level))/nSelWeightedEvt(i_syst, 0)) : 0.;}
  inline TDirectory* Dir() {return dir;}

  std::vector< std::vector<Double_t> > p_nSelEvt;
  std::vector< std::vector<Double_t> > p_nSelWeightedEvt;
  std::vector< std::vector<Double_t> > p_nSelWeightedEvtErr;

private:
  int p_type;
  double p_run;
  double p_lum;
  int p_nGenEvt;
  std::string p_title;
  double p_CrossSection;
  TDirectory *dir;
};

// #############################################################################
class NPA {
// #############################################################################
public :

  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  Ntu *nTuple;

//  ostream cout("out");

  // The version of NPA
  float NPAVersion;

  // files to read (ntuples)
  TInput Input;
  std::string TreeName;

  // The constructor and destructor
  NPA();
  virtual ~NPA();

  // To set default and user's values
  void DefaultValues();
  //void CommandLineOptions(int print);
//  virtual void UserCommandLineOptions(char * const *options);
  virtual void setOptions();
  void SetSystematics();

  // time
  time_t rawtimeI;
  time_t rawtimeF;

  // general stuff
  std::string SetSystematicsFileName;
  std::string OutputFileName;
  std::string BackgroundPdfFile;
  std::string GRLXMLFile;
  std::vector<const char*> SignalPdfFiles;
  double Luminosity;
  int DoLike;
  int Sample;
  int subperiod;

  // units
  double keV;
  double MeV;
  double GeV;
  double TeV;

  // masses
  double mZ;
  double mW;
  double mt;
  double mT;


  // jet energy calibration? and btag cut; both for fullsim topview >= 12-13
  double LightJetCalib;
  double BJetCalib;
  double BTagCut;

  double PtCutJet;
  double PtCutEle; 
  double PtCutMuo; 
  
  // flag for data events
  int isData;
 
  // flag to do good objects selection
  int doGoodObjSelection;

  // Pdf Smooth
  int PdfSmooth[50];

  // overlap of jets with photons and taus
  int JetOverlap;

  // Options for different reconstruction of objets within the same ntuples
  Int_t RecoType;

  // The main loop
  virtual void Loop();

  // Variables which are filled for each event
  int Isub;
  int LumiBlock; 
  int RunNumber; 
  int EveNumber; 
  int TruthEleNumber; 
  int TruthMuonNumber; 
  int ElectronTrigger;
  int MuonTrigger;
  int BadJet;
  int Reject_LAr;
  int Cosmic;
  int EleMuoOverlap;
  int GoodRL;
  int LArErroR;
  double MissPx;
  double MissPy;
  double MissPt;
  double Weight;
  double Sphericity;
  double Aplanarity;
  double Planarity;

  std::vector<TLorentzVectorWFlags> TruthVec;
  std::vector<TLorentzVectorWFlags> TauVec;
  std::vector<TLorentzVectorWFlags> MuonVec;
  std::vector<TLorentzVectorWFlags> ElectronVec;
  std::vector<TLorentzVectorWFlags> LeptonVec;
  std::vector<TLorentzVectorWFlags> PhotonVec;
  std::vector<TLorentzVectorWFlags> BTaggedJetVec;
  std::vector<TLorentzVectorWFlags> NonTaggedJetVec;
  std::vector<TLorentzVectorWFlags> JetVec;

  //std::vector<TVertex> Vtx;
  std::vector<TVector3> Vtx;

  // To fill vectors with subtypes of particles
  void FillMuonVec();
  void FillElectronVec();
  void FillBTaggedJetVec();
  void FillNonTaggedJetVec();

  // Fill all vectors
  void FillAllVectors();

  // to compute isolation angles
  void LeptonIsolationDeltaR();
  void PhotonIsolationDeltaR();
  void JetIsolationDeltaR();
  void TauIsolationDeltaR();


  // Main function
  void Start(int i_argc, const char *i_argv[]);

///////////////////////////////

  // amostras para ler
  std::vector<TMonteCarlo> MonteCarlo;
  virtual void DefineSamples();
  void ReadFiles(const char * ntu, const char * file, int type, double run, double lum, int nGenEvt, std::string title, int MaxCuts);

  // stuff for selection levels
  // number of cuts: do not forget to change when a new cut is added
  //const static int MaxCuts=10;
  int MaxCuts;
  int LastCut;
  virtual void DoCuts();
  
  // Setup over and underflow in histograms
  bool m_histos_underflow;
  bool m_histos_overflow;
  enum flowopt {
    OVER,
    UNDER,
    OVERUNDER,
    NONE
  };


  // stuff for histograms
  //TH1D* histo[MaxCuts+1][100];
  //Isto vai estoirar se houver mais de 20 niveis de selecao ou mais de 200 
  //histogramas por nivel...
  //TH1D* histo[20][200];
  inline char* c(char *s, int i, const char *t) { sprintf(s, "sel%02i%s", i, t); return s;}
  virtual void BookHistograms();
  virtual void FillHistograms();

  // stuff for making pdf
  // TFile* SignalPdf;
  // TFile* BackgroundPdf;
  std::vector< std::vector<TH1D*> > PdfVec;
  std::vector<double> ThisEventPdfValues;
  inline char* pdf(char *s, int i) { sprintf(s, "pdf%02i", i); return s;}
  virtual void BookPdfHistos(Int_t);
  virtual void PdfValues();
  void FillPdf(Int_t);
  void ProcessPdf();

  // Virtual function to configure things after the ntuple has been created
  virtual void configuration(){};
  // Add histogram, make histogram creation easier
  void addHisto(std::string name, std::string title, int nbins, double down, double up);
  void addHisto(std::string name, std::string title, int nbins, double *bins);
  void addHisto2D(std::string name, std::string title, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup);
  void addHisto2D(std::string name, std::string title, int nbinsx, double *xbins, int nbinsy, double ylow, double yup);
  void addHisto2D(std::string name, std::string title, int nbinsx, double xlow, double xup, int nbinsy, double *ybins);
  void addHisto2D(std::string name, std::string title, int nbinsx, double *xbins, int nbinsy, double *ybins);
  void setHistoErrors();
  void setHistoFlowOptions(flowopt);
  void fillHisto(std::string name, double val, double weight = 1);
  void fillHisto2D(std::string name, double x, double y, double weight = 1);
  // Define histograms
  std::map<std::string,TH1*> m_histos;

  // Function to define the reader to be used
  void defineReader(Ntu *ntu);

  // stuff for reading pdf and making likelihoods
  virtual void DefineSignalPdfSamples();
  TH1D* LogELikeSHist;
  TH1D* LogELikeBHist;
  TH1D* LogELikelihoodHist;
  TH1D* Log10LikeSHist;
  TH1D* Log10LikeBHist;
  TH1D* Log10LikelihoodHist;
  TH1D* LogELikeSHistVec[100];
  TH1D* LogELikeBHistVec[100];
  TH1D* LogELikelihoodHistVec[100];
  TH1D* Log10LikeSHistVec[100];
  TH1D* Log10LikeBHistVec[100];
  TH1D* Log10LikelihoodHistVec[100];
  std::vector<TH1D*> SignalPdfVec[100];
  std::vector<TH1D*> BackgroundPdfVec;
  double LogELikeSValue;
  double LogELikeBValue;
  double LogELikeLOverLikeBValue;
  double LogELikeMinCut;
  double LogELikeMaxCut;
  double Log10LikeSValue;
  double Log10LikeBValue;
  double Log10LikeLOverLikeBValue;
  double Log10LikeMinCut;
  double Log10LikeMaxCut;
  bool m_lastEvent;//!< True when we reach the last event in the loop
  void ReadPdf();
  virtual void BookLikeHistos();
  void ProcessBookLikeHistos();
  void ComputeAndFillLikelihood();

  // nTuple with surviving events
  std::vector<TNtuple*> OutputNtuple;
  void CreateOutputNtuple();
  void FillOutputNtuple(Int_t);

  // stuff for calculations after events loop
  virtual void PostLoopCalculations();

  // stuff for print stuff
  void PrintSummary(int OutputLevel, Int_t i_syst);

  // systematics
  std::vector<Int_t> Syst;
  std::vector<std::string> SystName;

  // stuff for write histograms to file
  std::vector<TFile*> OutputFile;
  void CreateOutputFile();
  void WriteHistograms();

  
};

#endif
