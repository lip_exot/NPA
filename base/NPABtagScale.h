#ifndef NPABTAGSCALE_H
#define NPABTAGSCALE_H
#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"
#include "TLorentzVectorWFlags.h"
#include <string>
#include <vector>


namespace Systematics{
    enum btagSyst {BTAGSFUP,BTAGSFDOWN,
                   CTAGSFUP,CTAGSFDOWN,
                   MTAGSFUP,MTAGSFDOWN,
                   HPTBTAGSFUP,HPTBTAGSFDOWN,
                   HPTCTAGSFUP,HPTCTAGSFDOWN,
                   HPTMTAGSFUP,HPTMTAGSFDOWN,
                   NOMINAL};
}

class NPABtagScale {
public:
    NPABtagScale (std::string workingpoint, std::string calivfile,std::string syst);
    virtual ~NPABtagScale ();

    double getJetSF(TLorentzVectorWFlags jet);
    double getTotalSF(std::vector<TLorentzVectorWFlags> *jets);

    static Systematics::btagSyst getBtagSyst(std::string syst);
private:
    
    double hptExtSF(double pt,int tfl);
    Analysis::CalibrationDataInterfaceROOT m_cdir;
    Analysis::CalibrationDataVariables m_cdv;
    Analysis::Uncertainty m_unc;

    unsigned int sfIndex[4];
    unsigned int effIndex[4];
    Systematics::btagSyst m_syst;
    double m_hptsf[9];
    enum HihgPtBin {B300,B500,B800,C300,C500,C800,L300,L500,L800};
};



#endif
