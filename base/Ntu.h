// #################################################################################
//
//                                         
//          888b      88  88888888ba      db         
//          8888b     88  88      "8b    d88b        
//          88 `8b    88  88      ,8P   d8'`8b       
//          88  `8b   88  88aaaaaa8P'  d8'  `8b      
//          88   `8b  88  88""""""'   d8YaaaaY8b     
//          88    `8b 88  88         d8""""""""8b    
//          88     `8888  88        d8'        `8b   
//          88      `888  88       d8'          `8b  
//                                         
//                                         
//                                                                              
// #################################################################################
//                                                                              
//   NPA                                         \      | | / |  /    |   |  ||  
//                                                '-___-+-+'  |_/     |   |  |   
//   by J.P. Araque, N. Castro, V. Oliveira      `--___-++--^'|       |   | /|   
//                                                      ||    |       |   |' |   
//   date: 19-12-2013                            --___  ||    |       | _/|  |   
//                                               ==---:-++___ |  ___--+'  |  |   
//                                                 '--_'|'---'+--___  |   |  |   
//                                                     '+-_   |     '-+__ |  |   
//                                              ._.          ._.       ._____   
//                                              | |          | |       | ___ \  
//                                              | |_.        | |       | .___/  
//                                              |___|        |_|       |_|
//      
//   Note: 	this code was built from LipCbrAnalysis developed by 
//     	   	Nuno Castro   (nfcastro@lipc.fis.uc.pt)  and 
//         	Filipe Veloso (fveloso@lipc.fis.uc.pt)             
//   Purpose: NPA is intended to be a complete API to develop new physics analyses
//            using NPA as an interface class.
//   Version: 1.0 (This is a quick adaptation of LipCbrAnalysis and several changes
//                are foreseen).
//
//
// #################################################################################


#ifndef Ntu_h
#define Ntu_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <vector>
#include "TLorentzVectorWFlags.h"
#include "TVector3.h"

class Ntu {
public :
  TChain *tree;
  /*TChain *tree2;
  TChain *tree3;
  TChain *tree4;*/
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
  int m_currentEntry;//!< Current entry that has been read

  Ntu();
  virtual ~Ntu();
  virtual Int_t GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void Init(int _isdata,TFile *_f, const char* _name);

  std::vector<TLorentzVectorWFlags> * TruthVec;
  std::vector<TLorentzVectorWFlags> * TauVec;
  std::vector<TLorentzVectorWFlags> * LeptonVec;
  std::vector<TLorentzVectorWFlags> * PhotonVec;
  std::vector<TLorentzVectorWFlags> * JetVec;
  std::vector<TVector3> * Vtx;
  double LightJetCalib;
  double BJetCalib;
  double BTagCut;
  double PtCutJet, PtCutEle, PtCutMuo;
  int JetOverlap;
  Int_t RecoType;
  int isData;
  void FillVectors(
    	std::vector<TLorentzVectorWFlags>&, 
	std::vector<TLorentzVectorWFlags>&,
    	std::vector<TLorentzVectorWFlags>&,
 	std::vector<TLorentzVectorWFlags>&,
    	std::vector<TLorentzVectorWFlags>&, 
	std::vector<TVector3>&, 
	double, 
	double, 
	double, 
	int, 
	double, 
	double, 
	double, 
	Int_t);

  virtual void Input(char*);

  virtual int Isub();
  virtual int LumiBlock();
  virtual int RunNumber();
  virtual int EveNumber();
  virtual int TruthEleNumber();
  virtual int TruthMuonNumber();
  virtual bool ElectronTrigger();
  virtual bool MuonTrigger();
  virtual int BadJet();
  virtual int Reject_LAr();
  virtual bool Cosmic();
  virtual int EleMuoOverlap();
  virtual bool GoodRL();
  virtual int LArErroR();
  virtual double MissPx();
  virtual double MissPy();
  virtual double MissPt();
  virtual double Weight();
  virtual double Sphericity();
  virtual double Aplanarity();
  virtual double Planarity();
  virtual double GeV();

  virtual void FillTruthVec();
  virtual void FillTauVec();
  virtual void FillLeptonVec();
  virtual void FillPhotonVec();
  virtual void FillJetVec();


};

#endif
