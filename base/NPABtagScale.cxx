#include "NPABtagScale.h"
#include "TopD3PDSelection/BTagSettings.h"
#include <iostream>

using namespace std;

NPABtagScale::NPABtagScale(string workingpoint, string calivfile,string syst) : 
    m_cdir("MV1",calivfile),
    m_unc(Analysis::None),
    m_syst(getBtagSyst(syst))
{
    BTagSettings::setBTagType(workingpoint);
    string wp = BTagSettings::bTagWorkPoint(BTagSettings::btagType());

    m_cdv.jetAuthor = "AntiKt4TopoLCJVF0_5";
    for (int i = 0; i < 4; i++) {
        sfIndex[i] = 999;
        effIndex[i] = 999;
    }
    // Get calibration indices
    m_cdir.retrieveCalibrationIndex("C"    , wp.c_str(), m_cdv.jetAuthor, true, sfIndex[0]);
    m_cdir.retrieveCalibrationIndex("B"    , wp.c_str(), m_cdv.jetAuthor, true, sfIndex[1]);
    m_cdir.retrieveCalibrationIndex("T"    , wp.c_str(), m_cdv.jetAuthor, true, sfIndex[2]);
    m_cdir.retrieveCalibrationIndex("Light"    , wp.c_str(), m_cdv.jetAuthor, true, sfIndex[3]);
    m_cdir.retrieveCalibrationIndex("C"    , wp.c_str(), m_cdv.jetAuthor, false, effIndex[0]);
    m_cdir.retrieveCalibrationIndex("B"    , wp.c_str(), m_cdv.jetAuthor, false, effIndex[1]);
    m_cdir.retrieveCalibrationIndex("T"    , wp.c_str(), m_cdv.jetAuthor, false, effIndex[2]);
    m_cdir.retrieveCalibrationIndex("Light"    , wp.c_str(), m_cdv.jetAuthor, false, effIndex[3]);

    m_hptsf[B300] = 0.12; m_hptsf[B500] = 0.33; m_hptsf[B800] = 0.27;
    m_hptsf[C300] = 0.17; m_hptsf[C500] = 0.27; m_hptsf[C800] = 0.30;
    m_hptsf[L300] = 0; m_hptsf[L500] = 0; m_hptsf[L800] = 0.58;
}

NPABtagScale::~NPABtagScale(){
}

double NPABtagScale::getJetSF(TLorentzVectorWFlags jet){
    using namespace Systematics;
    m_cdv.jetPt = jet.Pt();
    m_cdv.jetEta = jet.Eta();
    if(m_cdv.jetEta > 2.5) return 1.;
    int index = -1;
    int tfl = jet.truthFlavor();
    switch(tfl){
        case 4:
            index = 0;break;
        case 5:
            index = 1;break;
        case 15:
            index = 2;break;
        default:
            index = 3;break;
    }
    if(m_syst == HPTBTAGSFUP || m_syst == HPTBTAGSFDOWN ||
            m_syst == HPTCTAGSFUP || m_syst == HPTCTAGSFDOWN ||
            m_syst == HPTMTAGSFUP || m_syst == HPTMTAGSFDOWN){
        //m_unc = Analysis::Extrapolation;
        m_unc = Analysis::None;
        //cout<<"----->Extrapolation"<<endl;
    }
    if(m_syst == BTAGSFUP || m_syst == BTAGSFDOWN ||
            m_syst == CTAGSFUP || m_syst == CTAGSFDOWN ||
            m_syst == MTAGSFUP || m_syst == MTAGSFDOWN){
        m_unc = Analysis::Total;
        //cout<<"----->Total"<<endl;
    }
    bool tagged = jet.isb == 5;
    Analysis::CalibResult res;
    if (tagged) res = m_cdir.getScaleFactor(m_cdv, sfIndex[index],m_unc);
    else res = m_cdir.getInefficiencyScaleFactor(m_cdv, sfIndex[index],effIndex[index],m_unc);
    double weight = res.first;
    if(((m_syst == BTAGSFUP || m_syst == HPTBTAGSFUP) && tfl == 5) ||
            ((m_syst == CTAGSFUP || m_syst == HPTCTAGSFUP) && tfl == 4) ||
            ((m_syst == CTAGSFUP || m_syst == HPTCTAGSFUP) && tfl == 15) ||
            ((m_syst == MTAGSFUP || m_syst == HPTMTAGSFUP) && tfl == 0)){
        if(tagged){
            weight = res.first+res.second;
            //cout<<"Up variation tagged: "<<weight<<endl;
        }
        else{
            weight = res.first-res.second;
            //cout<<"Up variation non-tagged: "<<weight<<endl;
        }
    } 
    // Do it with Jiahang table
    /*if((m_syst == HPTBTAGSFUP && tfl == 5) ||
            (m_syst == HPTCTAGSFUP && tfl == 4) ||
            (m_syst == HPTCTAGSFUP && tfl == 15) ||
            (m_syst == HPTMTAGSFUP && tfl == 0)){
        if(tagged){
            weight = res.first+hptExtSF(m_cdv.jetPt,tfl);
            //cout<<"Up variation tagged: "<<weight<<endl;
        }
        else{
            weight = res.first-hptExtSF(m_cdv.jetPt,tfl);
            //cout<<"Up variation non-tagged: "<<weight<<endl;
        }
    } */
    if((m_syst == BTAGSFDOWN || m_syst == HPTBTAGSFDOWN) && tfl == 5 ||
            (m_syst == CTAGSFDOWN || m_syst == HPTCTAGSFDOWN) && tfl == 4 ||
            (m_syst == CTAGSFDOWN || m_syst == HPTCTAGSFDOWN) && tfl == 15 ||
            (m_syst == MTAGSFDOWN || m_syst == HPTMTAGSFDOWN) && tfl == 0){
        if(tagged){
            weight = res.first-res.second;
            //cout<<"Up variation tagged: "<<weight<<endl;
        }
        else{
            weight = res.first+res.second;
            //cout<<"Up variation non-tagged: "<<weight<<endl;
        }
    } 
    // Do it with Jiahang table
    /*if((m_syst == HPTBTAGSFDOWN && tfl == 5) ||
            (m_syst == HPTCTAGSFDOWN && tfl == 4) ||
            (m_syst == HPTCTAGSFDOWN && tfl == 15) ||
            (m_syst == HPTMTAGSFDOWN && tfl == 0)){
        if(tagged){
            weight = res.first-hptExtSF(m_cdv.jetPt,tfl);
            //cout<<"Up variation tagged: "<<weight<<endl;
        }
        else{
            weight = res.first+hptExtSF(m_cdv.jetPt,tfl);
            //cout<<"Up variation non-tagged: "<<weight<<endl;
        }
    } */

    // Protect the sf variations
    Analysis::CalibResult tmpres;
    if(tagged) tmpres = m_cdir.getMCEfficiency(m_cdv, effIndex[index],m_unc);
    else tmpres = m_cdir.getMCInefficiency(m_cdv, effIndex[index],m_unc);
    double mceff = tmpres.first;
    //cout<<"Protection: "<<weight<<"*"<<mceff<<"="<<weight*mceff<<endl;
    if(weight*mceff > 1) weight = 1./mceff;
    if(weight*mceff < 0) weight = 0;
    //cout<<"After: "<<weight*mceff<<endl;

    // Don't know why but TRC does it like this for non tagged tau jets
    if (tfl==15 && !tagged) { // for tau inefficiency, this needs to be calculated this ugly way
        Analysis::CalibResult tempRes;
        tempRes = m_cdir.getScaleFactor(m_cdv, sfIndex[index], m_unc);
        float tausf = tempRes.first;
        if (m_syst==CTAGSFUP || m_syst == HPTCTAGSFUP) tausf = tempRes.first+tempRes.second;
        else if (m_syst==CTAGSFDOWN || m_syst == HPTCTAGSFDOWN) tausf = tempRes.first-tempRes.second;
        tempRes = m_cdir.getMCEfficiency(m_cdv, effIndex[index], m_unc);
        float mceff = tempRes.first;
        weight=(1-tausf*mceff)/(1-mceff);
    }

    //if(jet.Pt()/1000 > 500) cout<<"Para este jet el peso es de: "<<weight<<endl;
    return weight;
}   

double NPABtagScale::getTotalSF(vector<TLorentzVectorWFlags> *jets){
    double sf = 1;
    for (int i = 0; i < jets->size(); i++)
        sf *=  getJetSF(jets->at(i));
    return sf;
}

Systematics::btagSyst NPABtagScale::getBtagSyst(string syst){
    using namespace Systematics;
    if(syst == "btag_up") return BTAGSFUP;
    if(syst == "btag_down") return BTAGSFDOWN;
    if(syst == "ctautag_up") return CTAGSFUP;
    if(syst == "ctautag_down") return CTAGSFDOWN;
    if(syst == "mistag_up") return MTAGSFUP;
    if(syst == "mistag_down") return MTAGSFDOWN;
    if(syst == "hpt_btag_up") return HPTBTAGSFUP;
    if(syst == "hpt_btag_down") return HPTBTAGSFDOWN;
    if(syst == "hpt_ctautag_up") return HPTCTAGSFUP;
    if(syst == "hpt_ctautag_down") return HPTCTAGSFDOWN;
    if(syst == "hpt_mistag_up") return HPTMTAGSFUP;
    if(syst == "hpt_mistag_down") return HPTMTAGSFDOWN;
    return NOMINAL;
}

double NPABtagScale::hptExtSF(double hpt,int tf){
    int pos = -1;;
    hpt = hpt/1000;
    if(tf == 5){
        if (hpt >= 300 && hpt < 500) pos = B300;
        else if (hpt >= 500 && hpt < 800) pos = B500;
        else if (hpt >= 800 && hpt <  1200) pos = B800;
        else pos = -1;
    } else if (tf == 4 || tf == 15){
        if (hpt >= 300 && hpt < 500) pos = C300;
        else if (hpt >= 500 && hpt < 800) pos = C500;
        else if (hpt >= 800 && hpt <  1200) pos = C800;
        else pos = -1;
    } else if (tf == 0){
        if (hpt >= 300 && hpt < 500) pos = L300;
        else if (hpt >= 500 && hpt < 800) pos = L500;
        else if (hpt >= 800 && hpt <  1200) pos = L800;
        else pos = -1;
    }
    if (pos == -1) return 0;
    else return m_hptsf[pos];
    /*cout<<"Before weight: "<<Weight<<endl;
      cout<<"Jet pt: "<<hpt<<endl;
      cout<<"Pos: "<<pos<<endl;
      cout<<"Scale: "<<m_hptsf[pos]<<endl;*/
}
