/**
NPAXML is the class that manages read and write from 
xml configuration files.
It uses pugixml as the xml parser.
*/
#ifndef NPAXML_H
#define NPAXML_H
#include <vector>
#include <string>
#include <map>
#include "pugixml.hpp"

#if __GNUC__ >= 4 && __GNUC__MINOR >= 7
    #define __VALIDC11__
#endif


namespace PlotProperties{
	struct legend_entry_t{
		std::string source;
		std::string type;
		std::string title;
	};
	
	struct legend_t{
		std::string id;
		float x1,x2,y1,y2;
		std::vector<legend_entry_t> entries;
	};

	struct syst_t{
		std::string id;
        std::string name;
		std::string type;
		std::string upsource;
		std::string dosource;
		std::string onlysource;
		double scaleup;
		double scaledo;
		double scaleonly;
		bool oneside;
	};
	struct input_t{
		std::string id;
		std::string type;
		std::string source;
		std::vector<PlotProperties::syst_t> syst;
	};
	struct sample_t{
		std::string id;
        std::string name;
        std::string type;
		std::vector<std::string> inputs;
        std::map<std::string,std::string> histoprops;
	};

}

class NPAXML
{
public:
	NPAXML(const char* xml,std::string root);
	virtual ~NPAXML();

	void printout();

    // Configuration
    int readOptionStr(std::string option,std::string &tosave);
    int readOptionDouble(std::string option, double &tosave);
    int readOptionInt(std::string option, int &tosave);
    int readOptionBool(std::string option, bool &tosave);

    void setOption(const char* option, const char* value);

    void saveFile(const char *output){m_xml_file_name=output;m_doc.save_file(output);};
    void update(const char* newfile){saveFile(newfile);m_doc.load_file(newfile);};

protected:	
    // Functions that might be called from any of the derived classes
    // but not called by the user!!
    
    pugi::xml_node getNode(const char* node){return m_doc.child(node);};

	// Method to get information
	// Get objetcs
	std::vector<PlotProperties::sample_t> getXmlSamples(const char* name){readSamples(name); return m_samples;};
	std::vector<PlotProperties::legend_t> getXmlLegends(){readLegends(); return m_legends;};
	std::vector<PlotProperties::input_t>  getXmlInputs(){readInputs(); return m_inputs;};
    
	void readLegends();
	void readSamples(const char* name);
	void readInputs();
    void readConfig();
    void readTables();

    std::string getXMLName(std::string s);


    // Check if operations has been done
    struct check_t{
        char legends:1;
        char samples:1;
        char inputs:1;
        char config:1;
        char parse:1;
    } m_checks;
    pugi::xml_node m_root;
    pugi::xml_document m_doc;
    std::string m_xml_file_name;
private:
    // None touches it!
    
    pugi::xml_node m_configuration;
	std::vector<PlotProperties::legend_t> m_legends;
	std::vector<PlotProperties::input_t> m_inputs;
	std::vector<PlotProperties::sample_t> m_samples;


    // Used to sort inputs
    static bool sortInputs(PlotProperties::input_t i1, PlotProperties::input_t i2){return i1.id<i2.id;};
};

#endif
