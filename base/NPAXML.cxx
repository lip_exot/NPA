#include "NPAXML.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include <algorithm>

using namespace pugi;
using namespace std;

NPAXML::NPAXML(const char* xml,string root){
    xml_parse_result res = m_doc.load_file(xml);
    if (res) cout<<"The XML file "<<xml<<" was parsed sucessfully!"<<endl;
    else {
        cerr<<"Xml file "<<xml<<" parsed with error: "<<res.description()<<endl;
        exit(1);
    }
    m_root = m_doc.first_child();
    if(root != m_root.name()){
        cerr<<"ERROR: It seems like "<<root<<" is not the root node of "<<xml<<"."<<endl;
        cerr<<"Please define all information inside a node "<<root<<"."<<endl;
        exit(1);
    }
    m_xml_file_name = xml;
    m_checks = check_t();
}

NPAXML::~NPAXML(){
}

void NPAXML::readLegends(){
    if(m_checks.legends) return;

    m_legends.clear();
    for (xml_node legend = m_root.child("Legend"); legend; legend = legend.next_sibling("Legend"))
    {
        PlotProperties::legend_t l;
        //legend.attribute("ID").set_value("Prueba");
        l.id=legend.attribute("ID").value();
        l.x1=legend.attribute("x1").as_float();
        l.x2=legend.attribute("x1").as_float();
        l.y1=legend.attribute("y1").as_float();
        l.y2=legend.attribute("y2").as_float();
        vector<PlotProperties::legend_entry_t> entries_vec;
        for (xml_node entry = legend.child("Entry"); entry; entry = entry.next_sibling("Entry"))
        {
            PlotProperties::legend_entry_t tmp;
            tmp.source = entry.attribute("Source").value();
            tmp.type = entry.attribute("Type").value();
            tmp.title = entry.attribute("Title").value();
            entries_vec.push_back(tmp);

        }
        l.entries = entries_vec;
        m_legends.push_back(l);
    }
    m_checks.legends=1;
}

void NPAXML::readInputs(){
    if(m_checks.inputs) return;
    // Loop over all inputs
    for (xml_node input = m_root.child("Input"); input; input = input.next_sibling("Input")){
        PlotProperties::input_t i = {};
        i.id = input.attribute("ID").value();
        i.type = input.attribute("Type").value();
        string inputdir;
        readOptionStr("InputDir",inputdir);
        string source = inputdir + input.attribute("File").value();
        i.source = source.c_str();
        vector<PlotProperties::syst_t> sysvec;

        // Loop over all shape systematics
        for(xml_node sys = input.child("ShapeSyst");sys;sys = sys.next_sibling("ShapeSyst")){
            PlotProperties::syst_t sy = {};
            sy.id = sys.attribute("ID").value();
            sy.name = sys.attribute("Name").as_string(sy.id.c_str());
            bool welldone = false;
            // Check if there are file systematics for the shape
            if(sys.attribute("FileUp") && sys.attribute("FileDown")){
                sy.type = "file";
                sy.oneside = false;
                string sourceup = inputdir + sys.attribute("FileUp").value();
                string sourcedown = inputdir + sys.attribute("FileDown").value();
                sy.upsource = sourceup;
                sy.dosource = sourcedown;
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(sys.attribute("HistoUp") && sys.attribute("HistoDown")){
                sy.type = "histo";
                sy.oneside = false;
                sy.upsource = sys.attribute("HistoUp").value();
                sy.dosource = sys.attribute("HistoDown").value();
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(sys.attribute("File")){
                sy.type = "file";
                sy.oneside = true;
                string source = inputdir + sys.attribute("File").value();
                sy.onlysource = source;
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(sys.attribute("Histo")){
                sy.type = "histo";
                sy.oneside = true;
                sy.onlysource = sys.attribute("Histo").value();
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(!welldone){
                cerr<<"ERROR: You have defined "<<sy.id<<" for the input "<<i.id<<" as a ShapeSystematic but ";
                cerr<<"it has not be defined properly. Check the definition of the sources ";
                cerr<<"and try again.";
                exit(1);
            }
            sysvec.push_back(sy);
        }

        for(xml_node sys = input.child("ScaleSyst");sys;sys = sys.next_sibling("ScaleSyst")){
            PlotProperties::syst_t sy = {};
            sy.id = sys.attribute("ID").value();
            bool welldone = false;
            // Check if there are file systematics for the shape
            if(sys.attribute("ScaleUp") && sys.attribute("ScaleDown")){
                sy.type = "scale";
                sy.oneside = false;
                string sourceup = sys.attribute("ScaleUp").value();
                string sourcedown = sys.attribute("ScaleDown").value();
                sy.upsource = sourceup;
                sy.dosource = sourcedown;
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(sys.attribute("Scale")){
                sy.type = "scale";
                sy.oneside = true;
                string source =sys.attribute("Scale").value();
                sy.onlysource = source;
                welldone = true;
                //TODO: Don't forget to check that everything is consistent
            }
            if(!welldone){
                cerr<<"ERROR: You have defined "<<sy.id<<" for the input "<<i.id<<" as a ShapeSystematic but ";
                cerr<<"it has not be defined properly. Check the definition of the sources ";
                cerr<<"and try again.";
                exit(1);
            }
            sysvec.push_back(sy);
        }

        // Add systematic vector to the current input
        i.syst = sysvec;
        m_inputs.push_back(i);
    }
    /// Sort inputs by id so then it is more efficient to find them
    /// for samples
    sort(m_inputs.begin(),m_inputs.end(),&NPAXML::sortInputs);
    m_checks.inputs=1;
}

void NPAXML::readSamples(const char* name){
    if(m_checks.samples) return;
    xml_node samples = m_root.find_child_by_attribute("Samples","Name",name);
    if(!samples){
        cerr<<"ERROR: The sample collection "<<name<<" has not been defined."<<endl;
        exit(1);
    }
    for (xml_node sample = samples.child("Sample"); sample; sample = sample.next_sibling("Sample"))
    {
        PlotProperties::sample_t s;
        s.id = sample.attribute("ID").value();
        s.type = sample.attribute("Type").value();
        if(sample.attribute("Name")) s.name=sample.attribute("Name").value();
        else s.name=s.id;
        vector<string> inputvec;
        for(xml_node input = sample.child("Input");input; input = input.next_sibling("Input"))
            inputvec.push_back(input.attribute("ID").value());
        for(xml_node hprop = sample.child("HistoProperties");hprop; hprop = hprop.next_sibling("HistoProperties"))
            for(xml_attribute_iterator it = hprop.attributes_begin(); it != hprop.attributes_end(); it++)
                s.histoprops[it->name()] = it->value();
        // Set current sample and add it to the member variable.
        s.inputs = inputvec;
        m_samples.push_back(s);
    }
    m_checks.samples=1;
}

void NPAXML::readConfig(){
    m_configuration = m_root.child("Configuration");
    if(!m_configuration){
        cerr<<"ERROR: No configuration node have been found in "<<m_xml_file_name<<endl;
        cerr<<"Please, define a configuration node before triying to access configuration options"<<endl;
        exit(1);
    }
    m_checks.config=1;
}

void NPAXML::printout(){
    for (int i = 0; i <m_legends.size(); ++i)
    {
        PlotProperties::legend_t l = m_legends[i];
        cout<<"ID:  "<<l.id<<endl;
        cout<<i<<endl;
        cout<<"Pos: ("<<l.x1<<","<<l.y1<<","<<l.x2<<","<<l.y2<<endl;
        cout<<"Entries: "<<endl;
        cout<<"_____"<<endl;
        for (int e = 0; e < l.entries.size(); ++e)
        {
            cout<<"	Source: "<<l.entries[e].source<<endl;
            cout<<"	Name: "<<l.entries[e].title<<endl;
            cout<<"	Type: "<<l.entries[e].type<<endl;	
            cout<<"______"<<endl;
        }
    }
}

/// Read options from Configuration node
int NPAXML::readOptionStr(string option, string &tosave){
    int ret = 1;
    if(!m_checks.config) readConfig();
    if(m_configuration.attribute(option.c_str())){
        tosave = m_configuration.attribute(option.c_str()).value();
        ret = 0;
    } 
    return ret;
}

int NPAXML::readOptionInt(string option, int &tosave){
    int ret = 1;
    if(!m_checks.config) readConfig();
    if(m_configuration.attribute(option.c_str())){
        tosave = m_configuration.attribute(option.c_str()).as_int();
        ret = 0;
    }
    return ret;
}

int NPAXML::readOptionBool(string option, bool &tosave){
    int ret = 1;
    if(!m_checks.config) readConfig();
    if(m_configuration.attribute(option.c_str())){
        tosave = m_configuration.attribute(option.c_str()).as_bool();
        ret = 0;
    }
    return ret;
}

int NPAXML::readOptionDouble(string option, double &tosave){
    int ret = 1;
    if(!m_checks.config) readConfig();
    if(m_configuration.attribute(option.c_str())){
        tosave = m_configuration.attribute(option.c_str()).as_double();
        ret = 1;
    }
    return ret;
}

void NPAXML::setOption(const char* option, const char* value){
    if(!m_checks.config) readConfig();
    if(m_configuration.attribute(option)) m_configuration.set_value(value);
    else m_configuration.append_attribute(option).set_value(value);
}

string NPAXML::getXMLName(string s){
    size_t pos = s.rfind("/");
    return s.substr(pos+1,s.rfind(".xml")-pos);
}
